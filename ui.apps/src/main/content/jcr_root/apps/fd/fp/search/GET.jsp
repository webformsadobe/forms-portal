
<%--
ADOBE CONFIDENTIAL

Copyright 2014 Adobe Systems Incorporated All Rights Reserved.

NOTICE:  All information contained herein is, and remains
the property of Adobe Systems Incorporated and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Adobe Systems Incorporated and its
suppliers and may be covered by U.S. and Foreign Patents,
patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Adobe Systems Incorporated.
 --%><%
%><%@page session="false" contentType="application/json; charset=utf-8"%>
<%@include file="/libs/foundation/global.jsp"%><%
%><%@page import="java.util.Iterator,
        com.day.cq.i18n.I18n,
        org.apache.sling.api.resource.ResourceResolver,
        org.apache.sling.api.resource.ResourceResolverFactory,
        org.apache.sling.commons.json.JSONArray,
        org.apache.sling.commons.json.JSONException,
        org.apache.sling.commons.json.JSONObject,
        org.apache.sling.api.resource.ValueMap,
        org.apache.commons.lang3.StringUtils,
        javax.jcr.Session,
        java.util.*,
        java.text.SimpleDateFormat,
        com.day.cq.search.Query,
        com.day.cq.search.QueryBuilder,
        com.day.cq.search.PredicateGroup,
        com.day.cq.search.result.SearchResult,
        com.adobe.fd.fp.util.PortalUtils,
        com.adobe.fd.fp.util.FormsPortalConstants" %><%
        %><%@page import="java.io.*"%><cq:defineObjects /><%
ResourceResolver serviceResourceResolver = null;
try {
ResourceResolverFactory resolverFactory = sling.getService(ResourceResolverFactory.class);
serviceResourceResolver = PortalUtils.getFnDServiceResolver(resolverFactory);
QueryBuilder queryBuilder = sling.getService(QueryBuilder.class);
//Query Map, which will have queries that can be understood by query builder, for creation of predicateGroup
Map<String, String> queryMap = new HashMap<String, String>();

//Primary Assest Type
queryMap.put("type", "dam:Asset");
String metaDataNodePath   = "jcr:content/metadata";
String jcrContentNodePath = "jcr:content";
int predicateID = 0;
List<String> searchedPaths = new ArrayList<String>();
Map<String,JSONObject> objectMap = new HashMap<String,JSONObject>();
//Iterating request parameters
Map parameterMap           = request.getParameterMap();
Set parameterSet           = parameterMap.entrySet();
Iterator paramIterator     = parameterSet.iterator();
boolean isExternalCall = true;
String browserLocale = "en";
String submitUrl = "", htmlProfile = "",pdfProfile = "", renderType = "";
I18n i18n = new I18n(slingRequest.getResourceBundle(new Locale(browserLocale)));
int counter = 0;

while (paramIterator.hasNext()) {
    Map.Entry<String,String[]> param = (Map.Entry<String,String[]>)paramIterator.next();
    String paramName                 = param.getKey();
    String[] paramValue              = param.getValue();
    int length                       = paramValue.length;
    //several parameters won't be mapped against any predicate
    String[] excludedParams          = {"_dc","_charset_","resultCount","cutPoints","submitUrl","htmlProfile", "pdfProfile", ":aem_csrf_token", ":cq_csrf_token"};
    if (!paramName.toLowerCase().contains("group.0_type") && !Arrays.asList(excludedParams).contains(paramName)) {
        if (paramName.equals("path[]") || paramName.equals("path")) {
            if (paramValue != null) {
                int pathID = 0;
                while (length-- > 0) {
                    queryMap.put(predicateID+"_group." + pathID + "_path", paramValue[pathID]);
                    pathID++;
                }
                queryMap.put(predicateID+"_group.p.or", "true");
                predicateID++;
            }
        } else if(paramName.equals("assetsources[]") || paramName.equals("assetsources")){
            isExternalCall = false;
            for (String val: paramValue) {
                counter = 1;
                JSONObject obj = new JSONObject(val);
                String path = obj.has("path") ? obj.getString("path") : null;
                String renderFormat = obj.has(FormsPortalConstants.STR_RENDER_TYPE) ? obj.getString(FormsPortalConstants.STR_RENDER_TYPE) : "";
                if (path != null) {
                    // adding all path of assets which are selected by user from edit dialog
                    searchedPaths.add(path);
                    // adding renderformat of selected assets
                    objectMap.put(path, obj);
                    Resource res = serviceResourceResolver.getResource(path);
                    if(res != null) {
                        ValueMap resValueMap = res.adaptTo(ValueMap.class);
                        Resource searchResource = res.getChild("searchcriteria");
                        if(searchResource != null) {
                            Iterable<Resource> resChilds = searchResource.getChildren();
                            Iterator<Resource> iter = resChilds.iterator();
                            while(iter.hasNext()){
                                Resource re =iter.next();
                                ValueMap map = re.adaptTo(ValueMap.class);
                                String searchcriteria = map.get("name",String.class);
                                String searchCriteriaValue = map.get("value",String.class);
                                queryMap.put("group."+predicateID + "_group." + counter + "_property", searchcriteria);
                                queryMap.put("group."+predicateID + "_group." + counter + "_property.value", searchCriteriaValue);
                                queryMap.put("group."+predicateID + "_group." + counter + "_property.operation", "equals");
                                counter++;
                            }
                        }
                    }
                }
                if(!StringUtils.isBlank(renderFormat)) {
                    // It means renderFormat is either HTML, PDF or BOTH. In any case, component will search for BOTH. So adding first search for BOTH and then adding search for exactg value of renderFormat.
                    queryMap.put("group."+predicateID + "_group." + counter + "_property", "./jcr:content/metadata/allowedRenderFormat");
                    queryMap.put("group."+predicateID + "_group." + counter + "_property.0_value", FormsPortalConstants.RENDER_FORMAT_BOTH);
                    queryMap.put("group."+predicateID + "_group." + counter + "_property.operation", "equals");
                    if(renderFormat != "BOTH") {
                        queryMap.put("group."+predicateID + "_group." + counter + "_property.1_value", renderFormat);
                    }
                    counter++;
                }
                predicateID++;
			}
            queryMap.put("group.p.or", "true");
        } else if (paramName.equals("fullTextPredicate[]") || paramName.equals("fullTextPredicate")) {
            if(paramValue != null){
                counter = 0;
                int pathID = 0;
                while(length-- >0 ){
                    if(paramValue[counter] != null && !paramValue[counter].isEmpty()){
                        queryMap.put(predicateID + "_group." + pathID + "_fulltext", paramValue[counter]);
                        pathID++;
                    }
                    counter++;
                }
                queryMap.put(predicateID+"_group.p.or", "true");
                predicateID++;
            }
        } else if (paramName.equals("lastModifiedDate.lowerBound")) {
            if (paramValue != null) {
                if (paramValue[0] != null && !paramValue[0].isEmpty()) {
                    queryMap.put("daterange.property", jcrContentNodePath + "/jcr:lastModified");
                    queryMap.put("daterange.lowerBound", paramValue[0]);
                }
            }
        } else if (paramName.equals("lastModifiedDate.upperBound")) {
            if (paramValue != null) {
                if (paramValue[0] != null && !paramValue[0].isEmpty()) {
                    queryMap.put("daterange.property", jcrContentNodePath + "/jcr:lastModified");
                    queryMap.put("daterange.upperBound", paramValue[0]);
                }
            }
        } else if (paramName.equals("tags")) {
            int tagID=0;
            if (paramValue != null) {
                while (length-- > 0) {
                    queryMap.put(predicateID+"_tagid",paramValue[tagID]);
                    queryMap.put(predicateID+"_tagid.property", metaDataNodePath+"/cq:tags");
                    predicateID++;
                    tagID++;
                }
            }
        } else if(paramName.equals("status")){
            if (paramValue != null) {
                int valueID=0;
                queryMap.put(predicateID + "_property", metaDataNodePath + "/status");
                while (length-- > 0) {
                    if (paramValue[valueID].equals("on")) {
                        paramValue[valueID] = "true";
                    }
                    queryMap.put(predicateID + "_property." + valueID + "_value", paramValue[valueID]);
                    valueID++;
                }
                predicateID++;
            }
        } else if (paramName.equals("orderby")) {
            String orderByValue = paramValue[0];
            if (orderByValue.equals("name")) {
                orderByValue = "title";
            }
            queryMap.put("orderby","@" + metaDataNodePath + "/" + orderByValue);
        } else if(paramName.equals("sort")){
            queryMap.put("orderby.sort", paramValue[0]);
        } else if(paramName.equals("offset")) {
            queryMap.put("p.offset", paramValue[0]);
        } else if (paramName.equals("limit")) {
            queryMap.put("p.limit", paramValue[0]);
        }  else if (paramName.equals("availableInMobileApp")) {
            queryMap.put(predicateID + "_group.0_property", metaDataNodePath + "/availableInMobileApp");
            queryMap.put(predicateID + "_group.0_property.operation", "equals");
            queryMap.put(predicateID + "_group.0_property.value", paramValue[0]);
            predicateID++;
        } else if(paramName.equals("src")){
            queryMap.put("src", paramValue[0]);
        } else {
            // As we are handling renderType out of this iteration, so added check to avoid adding renderType in full text.
            if (!paramName.equals(FormsPortalConstants.STR_RENDER_TYPE)) {
                // Those properties which can be found on metadata node by default with the same name as the request parameter
                if(paramValue != null){
                    counter = 0;
                    while (length-- > 0) {
                        queryMap.put(predicateID + "_fulltext", paramValue[counter]);
                        queryMap.put(predicateID + "_fulltext.relPath", metaDataNodePath + "/@" + paramName);
                        counter++;
                        predicateID++;
                    }
                }
            }
        }
    }
}
if(isExternalCall) {
    renderType = request.getParameter(FormsPortalConstants.STR_RENDER_TYPE);
    // It means search servlet is called from outside (not from search & lister component). So, in this case, need to search OOTB Assets with provided render type in request.
    if (StringUtils.isBlank(renderType)) {
        renderType = "HTML|PDF";
    }
    String[] allowedFormats = renderType.split("\\|");
    counter = 0;
    queryMap.put(predicateID+"_group.0_property", metaDataNodePath+"/allowedRenderFormat");
    for(String allowedFormat : allowedFormats) {
        queryMap.put(predicateID+"_group.0_property."+counter+"_value",allowedFormats[counter]);
        counter++;
    }
    if (renderType.toLowerCase().contains(FormsPortalConstants.RENDER_FORMAT_PDF.toLowerCase()) || renderType.toLowerCase().contains(FormsPortalConstants.RENDER_FORMAT_HTML.toLowerCase()) && !(renderType.toLowerCase().contains(FormsPortalConstants.RENDER_FORMAT_BOTH.toLowerCase()))) {
        queryMap.put(predicateID+"_group.0_property."+counter+"_value",FormsPortalConstants.RENDER_FORMAT_BOTH);
    }
    queryMap.put(predicateID+"_group.p.or","true");
    predicateID++;

    String[] allowedAssetTypes = {FormsPortalConstants.FORM_TEMPLATE, FormsPortalConstants.GUIDE, FormsPortalConstants.PDF_FORM, FormsPortalConstants.PRINT_FORM};
    counter = 0;
    for(String allowedAsset: allowedAssetTypes){
        queryMap.put(predicateID + "_group." + counter + "_property", jcrContentNodePath + "/" + FormsPortalConstants.STR_TYPE);
        queryMap.put(predicateID + "_group." + counter + "_property.value", allowedAsset);
        queryMap.put(predicateID + "_group." + counter + "_property.operation", "equals");
        counter++;
    }
    queryMap.put(predicateID + "_group.p.or", "true");
}

// Exclude all hidden forms.
++predicateID;
queryMap.put(predicateID + "_group.0_property", metaDataNodePath + "/hidden");
queryMap.put(predicateID + "_group.0_property.value", "No");


//Creating predicate group from Query Map
PredicateGroup predicates = PredicateGroup.create(queryMap);
//Creating query from predicate group
Query query               = queryBuilder.createQuery(predicates, resourceResolver.adaptTo(Session.class));
//Get result after executing the query
SearchResult result       = query.getResult ();
Session session           = slingRequest.getResourceResolver().adaptTo(Session.class);
Node currentNodeContent   = null;
Node currentMetadataNode  = null;
JSONArray respArr         = new JSONArray();
JSONObject resultCount    = new JSONObject();

resultCount.put("resultCount", result.getTotalMatches());
respArr.put(resultCount);

for (Iterator<Node> it = result.getNodes(); it.hasNext();) {
    String type = "";
    currentNode = it.next();
    String path = currentNode.getPath();
    if (currentNode.hasNode("jcr:content")) {
        currentNodeContent = session.getNode(path + "/jcr:content");
    }
    if (currentNode.hasNode(metaDataNodePath)) {
        currentMetadataNode = session.getNode(path + "/"+metaDataNodePath);
    }
    if (currentMetadataNode != null) {
        try {
            JSONObject respObj  = new JSONObject();

            //Certain fixed properties, need to be sent
            respObj.put("pdfStyle", "__FP_display_none");
            respObj.put("htmlStyle", "__FP_display_none");
            respObj.put("downloadStyle", "");
            respObj.put("pdfUrl", "");
            respObj.put("formUrl", "");
            respObj.put("downloadUrl", "");
            if(currentNodeContent.hasProperty("jcr:lastModified")) {
                respObj.put("lastModified",currentNodeContent.getProperty("jcr:lastModified").getDate().getTimeInMillis());
            }
            String pdfToolTip = "", htmlToolTip = "", currentAllowedRenderFormat = "";
            String contextPath = request.getContextPath();
            if(currentMetadataNode != null && currentMetadataNode.hasProperty("allowedRenderFormat")) {
                currentAllowedRenderFormat = currentMetadataNode.getProperty("allowedRenderFormat").getString();
            }
            if(currentNodeContent.hasProperty(FormsPortalConstants.STR_TYPE)) {
                type = currentNodeContent.getProperty(FormsPortalConstants.STR_TYPE).getString();
            }
            if (FormsPortalConstants.FORM_TEMPLATE.equals(type)){
                JSONObject jsonObj = objectMap.get(FormsPortalConstants.FORM_TEMPLATE_ASSET_PATH);
                if(jsonObj != null) {
                    if(!isExternalCall) {
                        renderType = jsonObj.has(FormsPortalConstants.STR_RENDER_TYPE) ? jsonObj.getString(FormsPortalConstants.STR_RENDER_TYPE) : "";
                        submitUrl = jsonObj.has(FormsPortalConstants.STR_SUBMIT_URL) ? jsonObj.getString(FormsPortalConstants.STR_SUBMIT_URL) : "";
                        htmlProfile = jsonObj.has(FormsPortalConstants.STR_HTML_PROFILE) ? jsonObj.getString(FormsPortalConstants.STR_HTML_PROFILE) : "";
                        pdfProfile = jsonObj.has(FormsPortalConstants.STR_PDF_PROFILE) ? jsonObj.getString(FormsPortalConstants.STR_PDF_PROFILE) : "";
                        htmlToolTip = jsonObj.has(FormsPortalConstants.STR_HTML_TOOLTIP) ? jsonObj.getString(FormsPortalConstants.STR_HTML_TOOLTIP) : i18n.get("Click here to view as HTML");
                        pdfToolTip = jsonObj.has(FormsPortalConstants.STR_PDF_TOOLTIP) ? jsonObj.getString(FormsPortalConstants.STR_PDF_TOOLTIP) : i18n.get("Click here to view as PDF");
                    }
                }
                respObj.put(FormsPortalConstants.STR_ASSET_TYPE,"Form");
                respObj.put(FormsPortalConstants.STR_TYPE, type);
                respObj.put("pdfUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf" + "&submitUrl=" + submitUrl + "&process=" + pdfProfile));
                respObj.put("formUrl", xssAPI.getValidHref(contextPath + path+"/jcr:content?submitUrl=" + submitUrl + "&profile=" + htmlProfile + "&source=fp"));
                if(renderType.contains(FormsPortalConstants.RENDER_FORMAT_BOTH) ||  renderType.contains(FormsPortalConstants.RENDER_FORMAT_HTML)) {
                    if ((currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_HTML) || currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_BOTH))) {
                        respObj.put("htmlStyle","");
                        if(!StringUtils.isBlank(htmlToolTip)) {
                            respObj.put(FormsPortalConstants.STR_HTML_LINK_TEXT,htmlToolTip);
                        }
                    }
                }
                if(renderType.contains(FormsPortalConstants.RENDER_FORMAT_BOTH) ||  renderType.contains(FormsPortalConstants.RENDER_FORMAT_PDF)){
                    if ((currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_PDF) || currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_BOTH))) {
                        respObj.put("pdfStyle","");
                        if(!StringUtils.isBlank(pdfToolTip)) {
                            respObj.put(FormsPortalConstants.STR_PDF_LINK_TEXT,pdfToolTip);
                        }
                    }
                }
                respObj.put("downloadUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf&action=download"));
            } else if (FormsPortalConstants.PDF_FORM.equals(type)) {
                JSONObject jsonObj = objectMap.get(FormsPortalConstants.PDF_FORM_ASSET_PATH);
                if(jsonObj != null) {
                    if(!isExternalCall) {
                        submitUrl = jsonObj.has(FormsPortalConstants.STR_SUBMIT_URL) ? jsonObj.getString(FormsPortalConstants.STR_SUBMIT_URL) : "";
                        pdfProfile = jsonObj.has(FormsPortalConstants.STR_PDF_PROFILE) ? jsonObj.getString(FormsPortalConstants.STR_PDF_PROFILE) : "";
                        pdfToolTip = jsonObj.has(FormsPortalConstants.STR_PDF_TOOLTIP) ? jsonObj.getString(FormsPortalConstants.STR_PDF_TOOLTIP) : i18n.get("Click here to view as PDF");
                    }
                }
                respObj.put(FormsPortalConstants.STR_ASSET_TYPE, "PDF Form");
                respObj.put(FormsPortalConstants.STR_TYPE, type);
                respObj.put("pdfUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf" + "&submitUrl=" + submitUrl + "&process=" + pdfProfile));
                respObj.put("pdfStyle", "");
                if(!StringUtils.isBlank(pdfToolTip)) {
                    respObj.put(FormsPortalConstants.STR_PDF_LINK_TEXT,pdfToolTip);
                }
                respObj.put("downloadUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf&action=download"));
            } else if (FormsPortalConstants.PRINT_FORM.equals(type)) {
                JSONObject jsonObj = objectMap.get(FormsPortalConstants.PRINT_FORM_ASSET_PATH);
                if(jsonObj != null) {
                    if(!isExternalCall) {
                        pdfProfile = jsonObj.has(FormsPortalConstants.STR_PDF_PROFILE) ? jsonObj.getString(FormsPortalConstants.STR_PDF_PROFILE) : "";
                        pdfToolTip = jsonObj.has(FormsPortalConstants.STR_PDF_TOOLTIP) ? jsonObj.getString(FormsPortalConstants.STR_PDF_TOOLTIP) : i18n.get("Click here to view as PDF");
                    }
                }
                respObj.put(FormsPortalConstants.STR_ASSET_TYPE, "Print Form");
                respObj.put(FormsPortalConstants.STR_TYPE, type);
                respObj.put("pdfUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf" + "&process=" + pdfProfile));
                respObj.put("pdfStyle", "");
                respObj.put("downloadUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=pdf&action=download"));
                if(!StringUtils.isBlank(pdfToolTip)) {
                    respObj.put(FormsPortalConstants.STR_PDF_LINK_TEXT,pdfToolTip);
                }
            } else if (FormsPortalConstants.GUIDE.equals(type)) {
                JSONObject jsonObj = objectMap.get(FormsPortalConstants.ADAPTIVE_FORM_ASSET_PATH);
                if(jsonObj != null) {
                    if(!isExternalCall) {
                        htmlProfile = jsonObj.has(FormsPortalConstants.STR_HTML_PROFILE) ? jsonObj.getString(FormsPortalConstants.STR_HTML_PROFILE) : "";
                        htmlToolTip = jsonObj.has(FormsPortalConstants.STR_HTML_TOOLTIP) ? jsonObj.getString(FormsPortalConstants.STR_HTML_TOOLTIP) : i18n.get("Click here to view as HTML");
                    }
                }
                respObj.put(FormsPortalConstants.STR_ASSET_TYPE, "Adaptive Form");
                respObj.put(FormsPortalConstants.STR_TYPE, type);
                respObj.put("formUrl", xssAPI.getValidHref(contextPath + path + "/jcr:content?type=guide&source=fp&wcmmode=disabled"));
                if (currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_HTML)) {
                    respObj.put("htmlStyle","");
                    if(!StringUtils.isBlank(htmlToolTip)) {
                        respObj.put(FormsPortalConstants.STR_HTML_LINK_TEXT,htmlToolTip);
                    }
                }
            } else if ("formset".equals(type)) {
                respObj.put(FormsPortalConstants.STR_ASSET_TYPE, "Form Set");
                respObj.put(FormsPortalConstants.STR_TYPE, type);
                respObj.put("formUrl", xssAPI.getValidHref(contextPath + path+"/jcr:content?submitUrl=" + submitUrl + "&source=fp"));
                respObj.put("pdfUrl", xssAPI.getValidHref(""));
                if (currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_HTML) || currentAllowedRenderFormat.equals(FormsPortalConstants.RENDER_FORMAT_BOTH)) {
                    respObj.put("htmlStyle","");
                }
            } else {
                /* It means asset searched by user is custom asset. Now, we are to find from all selected custom assets, which asset type is resultant asset. For this, we need to get search
                   criteria of all selected custom assets, check whether those criteria satisfy for resultant asset, if yes, then add metadata property of that custom asset to response object.
                */
                // Iterate over all selected custom asset types.
                for(int i =0;i< searchedPaths.size();i++) {
                    boolean flag = false;
                    String selectedAssetPath = searchedPaths.get(i);
                    Resource selectedAsset = serviceResourceResolver.getResource(selectedAssetPath);
                    if(selectedAsset != null) {
                        // get search criteria for selected asset
                        Resource selectedAssetSearchCriteria = selectedAsset.getChild("searchcriteria");
                        ValueMap selectedAssetValueMap = selectedAsset.adaptTo(ValueMap.class);
                        if(selectedAssetSearchCriteria != null) {
                            Iterable<Resource> selectedAssetSearchCriteriaChilds = selectedAssetSearchCriteria.getChildren();
                            Iterator<Resource> filters = selectedAssetSearchCriteriaChilds.iterator();
                            while(filters.hasNext()){
                                Resource filter = filters.next();
                                ValueMap map = filter.adaptTo(ValueMap.class);
                                String searchcriteria = map.get("name",String.class);
                                String searchCriteriaValue = map.get("value",String.class);
                                // check if resultant asset satisfy search criteria
                                if(currentNode.hasProperty(searchcriteria)) {
                                    String val = currentNode.getProperty(searchcriteria).getString();
                                    if(searchCriteriaValue.equals(val)) {
                                        flag = true;
                                    } else {
                                        flag = false;
                                        break;
                                    }
                                } else {
                                    flag = false;
                                    break;
                                }
                            }
						}
                        if(flag) {
                            for(String  str : selectedAssetValueMap.keySet()) {
                                String s = selectedAssetValueMap.get(str,String.class);
                                if(!StringUtils.isBlank(s)) {
                                    // If property name is jcr:title, assign the same into assetType
                                    if("jcr:title".equals(str)) {
                                        respObj.put("assetType", s);
                                    } else {
                                        respObj.put(str, s);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            respObj.put("id",currentNode.getIdentifier());
            respObj.put("path", currentNode.getPath());
            respObj.put("directLinky", "/dod/form?" + currentMetadataNode.getProperty("formNumber").getString());
            respObj.put(FormsPortalConstants.STR_PATH, xssAPI.getValidHref(contextPath + path));

            //! Code update to ensure that multi valued title and description fields do not crash the form search.
            String title = "";

            if (currentMetadataNode.hasProperty(FormsPortalConstants.STR_TITLE)) {
                Property property = currentMetadataNode.getProperty(FormsPortalConstants.STR_TITLE);
                if (property.isMultiple() == false) {
                    title = property.getString();
                }
            }

            if (title.isEmpty()) {
                if (currentMetadataNode.hasProperty("dc:title")) {
                    Property property = currentMetadataNode.getProperty("dc:title");
                    if (property.isMultiple() == false) {
                        title = property.getString();
                    }
                }
            }

            if (title.isEmpty()) {
                title = xssAPI.encodeForHTML(currentMetadataNode.getName());
            }
            respObj.put(FormsPortalConstants.STR_TITLE, title);
            respObj.put("name", title);

            String description = "";
            if (currentMetadataNode.hasProperty(FormsPortalConstants.STR_DESCRIPTION)) {
                Property property = currentMetadataNode.getProperty(FormsPortalConstants.STR_DESCRIPTION);
                if (property.isMultiple() == false) {
                	description = property.getString();
                }
            }

            if (description.isEmpty()) {
				if (currentMetadataNode.hasProperty("dc:description")) {
                    Property property = currentMetadataNode.getProperty("dc:description");
                    if (property.isMultiple() == false) {
                        description = property.getString();
                    }
                }
            }

            respObj.put(FormsPortalConstants.STR_DESCRIPTION, description);

            String formNumber = "";
            if (currentMetadataNode.hasProperty("formNumber") && !StringUtils.isBlank(currentMetadataNode.getProperty("formNumber").getString())) {
                formNumber = currentMetadataNode.getProperty("formNumber").getString();
            }
            respObj.put("formNumber", formNumber);

            String formName = "";
            if (currentMetadataNode.hasProperty("formName") && !StringUtils.isBlank(currentMetadataNode.getProperty("formName").getString())) {
                formName = currentMetadataNode.getProperty("formName").getString();
            }
            respObj.put("formName", formName);
            //properties asked by the client
            String[] propertiesAsked = request.getParameter("cutPoints").split(",");
            int lPropertiesAsked     = propertiesAsked.length;
            counter              = 0;
            while (lPropertiesAsked-- > 0) {
                if (currentMetadataNode.hasProperty(propertiesAsked[counter]) && !respObj.has(propertiesAsked[counter])) {
                    Property prop = currentMetadataNode.getProperty(propertiesAsked[counter]);
                    switch (prop.getType()) {
                        case PropertyType.STRING:
                            if(!prop.isMultiple()) {
                                respObj.put(propertiesAsked[counter], xssAPI.encodeForHTML(prop.getString()));
                            } else {
                                Value[] values = prop.getValues();
                                String respProp = "[";
                                for (int i = 0; i < values.length; i++) {
                                    respProp += ("\""+values[i].getString()+"\"");
                                    if (i < values.length-1) {
                                        respProp+=",";
                                    }
                                }
                                respProp += "]";
                                respObj.put(propertiesAsked[counter], new JSONArray(respProp));
                            }
                            break;
                        case PropertyType.DATE:
                            respObj.put(propertiesAsked[counter], prop.getDate().getTime().getTime());
                            break;
                        case PropertyType.BOOLEAN:
                            respObj.put(propertiesAsked[counter], prop.getBoolean());
                            break;
                        default:
                            respObj.put(propertiesAsked[counter], xssAPI.encodeForHTML(prop.getString()));
                            break;
                    }
                } else {
                    if (!respObj.has(propertiesAsked[counter])) {
                        respObj.put(propertiesAsked[counter],"");
                    }
                }
                counter++;
            }
            respArr.put(respObj);
        } catch (Exception e) {

        }
    }
}
String offset = xssAPI.encodeForHTML(request.getParameter("offset"));

response.getWriter().write("{\"success\":true");
response.getWriter().write(",\"response\":");
response.getWriter().write(respArr.toString());
response.getWriter().write(",\"offset\":"+offset);
response.getWriter().write("}");
}catch (Exception e) {
    // log exception here
} finally {
    if(serviceResourceResolver != null) {
        serviceResourceResolver.close();
    }
}
%>
