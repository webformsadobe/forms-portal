function getAssetMetadata(file, fileName, coverPage, formUrl, description) {
    // Earlier versions of IE do not support this property.
    if (!window.location.origin) {
      window.location.origin = window.location.protocol + "//"
      + window.location.hostname
      + (window.location.port ? ':' + window.location.port: '');
    }

    var servletPath = window.location.origin + file + ".properties.json";
	resetModalFields();
    // Retrieve the meta data information (JSON Object) by AJAX
    $.ajax({
        url : servletPath,
        success : function(jsonObj){
            $("#modalBookmark").text('');
            $("#modalBookmark").append('<span class="text-muted">'+ jsonObj.formNumber +'</span><a data-form-id="'+ jsonObj.formNumber +'" title="'+ jsonObj.formNumber + ' - ' + fileName  +'"><img src="assets/icons/bookmark.svg" class="icon-12 mr-1" alt="bookmark"></a>');
            window.updateBookmarks();
            var sponsorData = jsonObj.sponsorData.heading1 != "" ? jsonObj.sponsorData.heading1 : jsonObj.sponsorData.sponsor;
            var supplierData = jsonObj.supplierData.heading1 != "" ? jsonObj.supplierData.heading1 : jsonObj.supplierData.supplier;
            $("#sponsorData").text(sponsorData);
            $("#supplierName").text(supplierData);
            $("#keywords").text(jsonObj.keywords);
            $("#format").text(jsonObj.format);
            $("#modal-description").text(description);
            $("#classification").text(jsonObj.classification);
            $("#stockNumber").text(jsonObj.stockNumber);
            $("#controlled").text(jsonObj.controlled);
            $("#number").text(jsonObj.number);
            $("#oldNumber").text(jsonObj.oldNumber);
            $("#createDate").text(jsonObj.createDate);
            $("#unitOfIssue").text(jsonObj.unitOfIssue);
            $("#group").text(jsonObj.group);
            $("#versionDate").text(jsonObj.versionDate);
            $("#registryNumber").text(jsonObj.registryNumber);
            $("#assetName").text(fileName);
            $("#unit").text(jsonObj.unitOfIssue);
            $("#stockNumber").text(jsonObj.stockNumber);

			//Sponsor fields
			$("#sponsorHeading1").text(jsonObj.sponsorData.heading1);
			$("#sponsorHeading2").text(jsonObj.sponsorData.heading2);
            $("#contactOfficer").text(jsonObj.sponsorData.contactOfficer);
            $("#contactPosition").text(jsonObj.sponsorData.contactPosition);
            $("#location").text(jsonObj.sponsorData.location);
            $("#phone").text(jsonObj.sponsorData.phone);
            $("#mobile").text(jsonObj.sponsorData.mobile);
            $("#fax").text(jsonObj.sponsorData.fax);
            $("#email").text(jsonObj.sponsorData.email);

            var secContactOfficer = jsonObj.sponsorData.secondaryContactOfficer;
            var secContactPos = jsonObj.sponsorData.secondaryContactPosition;
            var secContactLoc = jsonObj.sponsorData.secondaryLocation;
            var secContactPhn = jsonObj.sponsorData.secondaryPhone;
            var secContactMob = jsonObj.sponsorData.secondaryMobile;
            var secContactFax = jsonObj.sponsorData.secondaryFax;
            var secContactEmail = jsonObj.sponsorData.secondaryEmail;

            if (secContactOfficer != null && secContactPos != null && secContactLoc != null && secContactPhn != null &&
                secContactMob != null && secContactFax != null && secContactEmail != null) {
                var secTestString = secContactOfficer.concat(secContactPos).concat(secContactLoc).concat(secContactPhn).concat(secContactFax).concat(secContactMob).concat(secContactEmail);

                if(secTestString == ""){
                    $("#secondaryInfoSection").addClass("hide");
                } else {
                    $("#secondaryInfoSection").removeClass("hide");
                    $("#secondaryContactOfficer").text(secContactOfficer);
                    $("#secondaryContactPosition").text(secContactPos);
                    $("#secondaryLocation").text(secContactLoc);
                    $("#secondaryPhone").text(secContactPhn);
                    $("#secondaryMobile").text(secContactMob);
                    $("#secondaryFax").text(secContactFax);
                    $("#secondaryEmail").text(secContactEmail);
                }
            }

            //Supplier fields
            $("#supplierHeading1").text(jsonObj.supplierData.heading1);
            $("#bodyContent").text(jsonObj.supplierData.content);

            if(coverPage === "true" || (typeof(formUrl) != "undefined" && formUrl != "" && formUrl.includes("formsanddocuments"))) {
                //Hide download button
				$("#formDownload").addClass("hide");
            } else {
                $("#formDownload").removeClass("hide");
				$("#formDownload").attr("href", "/dod/form?" + jsonObj.formNumber);
            }
        }
    });
}

function resetModalFields() {
	$("#formNumber").text('');
    $("#sponsorData").text('');
    $("#supplierName").text('');
    $("#keywords").text('');
    $("#reviewDate").text('');
    $("#format").text('');
    $("#classification").text('');
    $("#stockNumber").text('');
    $("#controlled").text('');
    $("#number").text('');
    $("#oldNumber").text('');
    $("#createDate").text('');
    $("#unitOfIssue").text('');
    $("#group").text('');
    $("#versionDate").text('');
    $("#registryDate").text('');
    $("#assetName").text('');

    //Sponsor fields
    $("#sponsorHeading1").text('');
    $("#sponsorHeading2").text('');
    $("#sponsorHeading3").text('');
    $("#contactOfficer").text('');
    $("#contactPosition").text('');
    $("#location").text('');
    $("#phone").text('');
    $("#fax").text('');
    $("#email").text(''); 
    $("#secondaryContactOfficer").text('');
    $("#secondaryContactPosition").text('');
    $("#secondaryLocation").text('');
    $("#secondnaryPhone").text('');
    $("#secondaryFax").text('');
    $("#secondaryEmail").text(''); 
    $("#tertiaryContactOfficer").text('');
    $("#tertiaryContactPosition").text('');
    $("#tertiaryLocation").text('');
    $("#tertiaryPhone").text('');
    $("#tertiaryFax").text('');
    $("#tertiaryEmail").text('');
    
    //Supplier fields
    $("#supplierHeading1").text('');
    $("#bodyContent").text('');
}

function openLinkUrl(event, isCoverPage, path, formUrl){
    var url = "";
    if(formUrl.indexOf("formsanddocuments") > 0){
		window.open(formUrl, "_self");
        url = formUrl;
    } else if(isCoverPage === "true") {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

        // Retrieve the meta data information (JSON Object) by AJAX
        url = "/content/dod/coverPage.html?file=" + path;
        window.open(url, "_self");
    } else {
		window.open(path, "_self");
    }
}


/**
What version of internet exploder are we using? Returns the version number of IE,
or -1 if the user is using a sensible browser.
**/
function aemDetectIE() {
    var ua = window.navigator.userAgent;
    var result = -1;

    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');
    var edge = ua.indexOf('Edge/');

    if (msie > 0) {
        // IE 10 or older => return version number
        result = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    } else if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        result = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    } else if (edge > 0) {
       // Edge (IE 12+) => return version number
       result = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return result;
}

/**
Generic UI functions for all dialog box and alert interactions.
**/
function dialogBoxInformation(textToDisplay) {
    $("#successText").text(textToDisplay);
    $("#successMsg").removeClass("hide");
    $("#errorMsg").addClass("hide");
}

function dialogBoxError(textToDisplay) {
    $("#errorText").text(textToDisplay);
    $("#errorMsg").removeClass("hide");
    $("#successMsg").addClass("hide");
}

/*--------------------------------------------------------------------------*/
/*-----------------------------Home Page Code-------------------------------*/
/*--------------------------------------------------------------------------*/

function syncDetails(){
   $.ajax({
       url : "/dod/syncDetails",
       type: 'POST',
       success : function(jsonObj){
           dialogBoxInformation("Data synchronized successfully.");
       },
       error : function(){
		   dialogBoxError.text("Error synchronizing data.");
       }
   });
}

function replicate(dataNode){
   $.ajax({
       url : "/dod/replicate",
       data : {'dataNode' : dataNode},
       type: 'POST',
       success : function(jsonObj){
		   dialogBoxInformation("Data replicated successfully.");
           if(dataNode === 'sponsor'){
           		$("#sponsorTable").DataTable().ajax.reload();
           } else if(dataNode === 'supplier'){
           		$("#supplierTbl").DataTable().ajax.reload();
           } else if(dataNode === 'news'){
           		$("#newsTable").DataTable().ajax.reload();
           } else if(dataNode === 'all'){
                $("#sponsorTable").DataTable().ajax.reload();
               	$("#supplierTbl").DataTable().ajax.reload();
           		$("#newsTable").DataTable().ajax.reload();
           }
       },
       error : function(){
           dialogBoxError("Error replicating data.");
       }
   });
}


/*--------------------------------------------------------------------------*/
/*-----------------------------SearchResult Page Code-----------------------*/
/*--------------------------------------------------------------------------*/
function getSearchResult(searchParam, offset, src, orderBy, sortOrder){
    var data = {'_charset_': 'utf-8',
        'limit' : '15',
        '0_group.0_type': 'dam:Asset',
        'orderby': orderBy,
        'sort': sortOrder,
        'cutPoints': 'coverpage,name,format,classification,group',
        'path': ['/content/dam/dodassets', '/content/dam/formsanddocuments/dodassets'],
        'assetsources': ['{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/adaptiveform", "renderType":"HTML", "htmlToolTip":"Click here to view as HTML", "pdfToolTip":"Click here to view as PDF"}',
                         '{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/pdffiles", "htmlToolTip":"Click here to view as HTML", "pdfToolTip":"Click here to view as PDF"}',
                         '{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/iptfiles", "htmlToolTip":"Click here to view as HTML", "pdfToolTip":"Click here to view as PDF"}']
        }

        data['offset'] = offset;
        data['src'] = src;
        data['fullTextPredicate'] = searchParam;

        //! @todo this belongs in a separate function as a callback, keeping it here creates a ugly monster
        $.ajax({
           url : "/content/dodFPServlet/root/responsivegrid/querybuilder/searchAssets",
           type: 'GET',
           data: data,
           dataType: "json",
           success : function(jsonObj){
               var jsonArray = jsonObj['response'];
               var length = jsonArray[0].resultCount;

               // Discards the first item in the array; the number of records returned.
               jsonArray.splice(0,1);

                // This kludge removes extraneous results from the bookmarks and top forms searches, because
                // these things link to the search page instead of directly pointing to the forms themselves.
                // I suspect this is because of the requirement to be able to display the forms metadata as per
                // the search results page - Nick S
               if(src === 'bookmark' || src === 'topForms') {
                    // Filter out all records for which the form number does not match the one searched for.
                    function testResultIsOriginal(value) {
                        return searchParam.indexOf(value.formNumber) >= 0;
                    }
                    jsonArray = jsonArray.filter(testResultIsOriginal);

                    // Adjust the numbers of rows returned to account for the fact that some may have been lost in the
                    // step above.
                    offset = 0;
                    length = jsonArray.length;
               }

               var startingRecord = (length === 0) ? 0 : parseInt(offset) + 1;
               $("#startRecNum").text(startingRecord);
               $("#endRecNum").text(parseInt(offset) + jsonArray.length);
               $("#totalNumOfRec").text(length);
               $('#search-results').text('');

               $("#previous").removeClass("hide");
               $("#next").removeClass("hide");
               if(parseInt(offset) == 0){
                    $("#previous").addClass("hide");
               }
               if(length - parseInt(offset) < 16){
                    $("#next").addClass("hide");
               }

                $.each(jsonArray, function(key, item) {
                    if(typeof(item) != "undefined") {
                        var isCoverPage = (item.coverpage === true) || (item.coverpage === 'true');
                        var resourceLink = isCoverPage ? item.path : item.directLinky;

                        // Limit description to 180 chars
    					item.description = (item.description ? item.description.replace(/(^.{180}).*$/,'$1...') : '' );

                        var elem = '<div id="search-result-' + key + '" class="row d-flex row-results border py-3">'+
                                    '<div class="col-info px-0">' +
                                        '<a data-toggle="modal" title="View Metadata" href="#" data-target="#metadataModal">' +
                                            '<svg class="info-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">'+
                                                '<circle class="st0" cx="12" cy="12" r="8.7"/><line class="st0" x1="12" y1="16" x2="12" y2="11.2"/><line class="st0" x1="12" y1="8.3" x2="12" y2="8"/>'+
                                            '</svg>'+
                                        '</a>'+
                                    '</div>'+
                                    '<div class="col-number">'+
                                        '<a data-toggle="modal" data-target="#metadataModal">'+
                                            '<span class="result-number">'+ item.formNumber +'</span>'+
                                        '</a>'+
                                    '</div>'+
                                    '<div class="col-name pl-0">'+
                                        '<a href="#" onclick="openLinkUrl(event,\'' + isCoverPage + '\', \'' + resourceLink + '\', \'' + item.formUrl + '\');"><span class="result-name">' + item.formName + '</span></a>'+
                                    '</div>'+
                                    '<div class="col-description">'+
                                        '<span class="result-sponsor">'+ item.description +'</span>'+
                                    '</div>'+
                                    '<div class="col-bookmarked">'+
                                        '<a data-form-id="'+ item.formNumber +'" title="'+ item.formNumber + ' - ' + item.formName  +'">'+
                                            '<img class="icon-12" src="assets/icons/bookmark.svg" alt="bookmark" />'+
                                        '</a>'+
                                    '</div>'+
                                '</div>';

                        $('#search-results').append(elem);
                        $('#' + 'search-result-' + key + ' [data-toggle="modal"]').on('click', function() { getAssetMetadata(item.path, item.formName, isCoverPage, item.formUrl, item.description) } );
                    }
                });
               window.updateBookmarks();
           }
       });
}

function getIE9SearchResult(searchParm, offset, src, orderBy, sortOrder){
				var data = {'_charset_': 'utf-8', 'limit' : '15', '0_group.0_type': 'dam:Asset', 'orderby': orderBy, 'sort': sortOrder,  'cutPoints': 'coverpage,name,format,classification,group', 
                			'path': ['/content/dam/dodassets', '/content/dam/formsanddocuments/dodassets'],
                			'assetsources': ['{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/adaptiveform","renderType":"HTML","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}',
                            				'{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/pdffiles","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}',
                            				'{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/iptfiles","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}']
                            }

                data['offset'] = offset;
    			data['src'] = src;
    			//data['offset'] = '0';
                data['fullTextPredicate'] = searchParm;
				$.ajax({
                   url : "/content/dodFPServlet/root/responsivegrid/querybuilder/searchAssets",
                   type: 'GET',
                   data: data,
                   dataType: "json",
                   success : function(jsonObj){
                       var jsonArray = jsonObj['response'];
                       var length = jsonArray[0].resultCount;
                       jsonArray.splice(0,1);
                       //delete jsonArray[0];
                       //jsonArray.sort(predicateBy('formNumber'));

                       $("#startRecNum").text(parseInt(offset) + 1);
                       $("#endRecNum").text(parseInt(offset) + jsonArray.length);
                       $("#totalNumOfRec").text(length);
                       $('#search-results').text('');

                       $("#previous").removeClass("hide");
                       $("#next").removeClass("hide");
                       if(parseInt(offset) == 0){
							$("#previous").addClass("hide");
                       } 
                       if (length - parseInt(offset) < 16){
							$("#next").addClass("hide");
                       }
                        $.each(jsonArray, function(key, item) {
                            if(typeof(item) != "undefined") {
                                    var elem = '<tr>'+
                                                    '<td scope="row">'+ item.formNumber +'</td>'+
                                                    '<td>'+ '<a href="#" onclick="openLinkUrl(event,\'' + item.coverpage + '\', \'' + item.path + '\', \'' + item.formUrl + '\');"><span class="result-name">' + item.formName + '</span></a>' +'</td>'+
                                                    '<td>'+ item.format + '</td>'+
                                                    '<td>'+ item.classification + '</td>'+
                                                '</tr>';

                                    $('#search-results').append(elem);
                            }
                        });
                   }
               });
}

function predicateBy(prop){
   return function(a,b){
      if( a[prop] > b[prop]){
          return 1;
      }else if( a[prop] < b[prop] ){
          return -1;
      }
      return 0;
   }
}

/*--------------------------------------------------------------------------*/
/*-----------------------------Search Page Code-----------------------*/
/*--------------------------------------------------------------------------*/
function getNewsDetails(){
    $.ajax({
    url : "/dod/getNodeDetails?node=newsData",
    type: 'GET',
    success : function(json) {
        var i = 0;
        $.each(json, function(key, item) {
            var currentDate = new Date();
            if(typeof(item) != "undefined") {
                if(item.expiryDate === "" || new Date(item.expiryDate)  >= currentDate) {
                    var elem = '';
                    if(i==0){
                        elem = '<div class="carousel-item active">'+
                        '<div class="whats-new-content pb-4">'+
                        '<h4 class="mt-4 mb-5 font-weight-light">' + item.newsHeading + '</h4>'+
                        '<p class="mr-5 mb-4 font-weight-light">' + item.newsContent + '</p>'+
                        '<a href="'+ item.newsLink +'" class="font-weight-normal text-light"><span>Read more</span> <img src="assets/icons/arrow-inverted.svg" class="icon-16" alt="arrow-icon"></a>'+
                        '</div>'+
                        '</div>';
                    } else {
                        elem = '<div class="carousel-item">'+
                            '<div class="whats-new-content pb-4">'+
                            '<h4 class="mt-4 mb-5 font-weight-light">' + item.newsHeading + '</h4>'+
                            '<p class="mr-5 mb-4 font-weight-light">' + item.newsContent + '</p>'+
                            '<a href="'+ item.newsLink +'" class="font-weight-normal text-light"><span>Read more</span> <img src="assets/icons/arrow-inverted.svg" class="icon-16" alt="arrow-icon"></a>'+
                            '</div>'+
                            '</div>';
                    }
                    $('#news-content').append(elem);
                    if(typeof(item.newsLink) == 'undefined' || item.newsLink === ''){
                        $('#news-content').find('a').last().addClass('hide');
                    }
                    i++;
                }
            }
        });
        window.adjustCarouselMinHeight();
    }
    });
}


function getCategoriesDetails(){
    			var allCategories = [];
				$.ajax({
                   url : "/dod/getNodeDetails?node=defenceCategories",
                   type: 'GET',
                   success : function(json){
                        $.each(json, function(key, item) {
                            if(typeof(item) != "undefined") {
										var elem = '';
										allCategories.push(item.query);
										elem = '<div class="col-lg-4 col-12 d-flex flex-row align-items-center mb-4">'+
                                                    '<a href="searchResult.html?search='+ item.query +'&src=category" class="category-list-item d-flex flex-row align-items-center justify-content-between inner-link--grey" href="#">'+
                                                        '<img src="assets/icons/defence-purchasing.svg" class="icon-32" alt="bookmarked icon">'+
                                                        '<span class="ml-2 flex-grow text-left">'+
                                                            '<span class="h5">'+ item.name +'</span>'+
                                                        '</span>'+
                                                        '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">'+
                                                        	'<line class="arrow" x1="4" y1="12" x2="20" y2="12"></line>'+
                                                            '<polyline class="arrow" points="14,6 20,12 14,18 "></polyline>'+
                                                        '</svg>'+
                                                    '</a>'+
                                                '</div>';
                                    $('#category-content').append(elem);
                            }
                        });
                   }
               });
}

function getBookmarkId(bookmarkIds, src){
    var data = {'_charset_': 'utf-8', 'limit' : '-1', '0_group.0_type': 'dam:Asset', 'orderby': 'formNumber', 'sort': 'asc',  'cutPoints': 'coverpage,name,format,classification,group',
                'path': ['/content/dam/dodassets', '/content/dam/formsanddocuments/dodassets'],
                'assetsources': ['{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/adaptiveform","renderType":"HTML","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}',
                                '{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/pdffiles","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}',
                                '{"path":"/mnt/overlay/fd/fp/extensions/querybuilder/assettypes/iptfiles","htmlToolTip":"Click here to view as HTML","pdfToolTip":"Click here to view as PDF"}']
                }

    data['offset'] = '0';
    data['src'] = src;
    data['fullTextPredicate'] = bookmarkIds;
    $.ajax({
        url : "/content/dodFPServlet/root/responsivegrid/querybuilder/searchAssets",
        type: 'GET',
        data: data,
        dataType: "json",
        success : function(jsonObj){
            var jsonArray = jsonObj['response'];

            jsonArray.splice(0,1);
            jsonArray.sort(predicateBy('formNumber'));

            var index = 1;
            $.each(jsonArray, function(key, item) {
                if(index < 6 && typeof(item) != "undefined" && bookmarkIds.indexOf(item.formNumber) >= 0) {
                    var isCoverPage = (item.coverpage === true) || (item.coverpage === 'true');
                    var resourceLink = isCoverPage ? item.path : item.directLinky;
                    var formName = item.formName.replace("'", "");

                    var elem = '<div class="d-inline-block my-3 px-3 bookmark-item">'+
                                    '<p class="mb-2">0' + index + '</p>'+
                                    '<div>'+
                                        '<span class="text-bottom"><span class="font-weight-light" title="' + item.formNumber + ' - ' + formName  +'" onclick="openLinkUrl(event,\'' + isCoverPage + '\', \'' + resourceLink + '\', \'' + item.formUrl + '\');" style="cursor: pointer;">' + item.formNumber + '</span></span>'+
                                        '<a data-form-id="' + item.formNumber + '" title="' + item.formNumber + ' - ' + formName  + '"><img src="/content/dod/assets/icons/bookmarked-primary.svg" class="icon-12" alt="bookmarked icon"></a>'+
                                    '</div>'+
                                '</div>';

                    $('#section-top-5-bookmarks').append(elem);
                    index++;
                }

            });
            window.updateBookmarks();
        }
    });
}
