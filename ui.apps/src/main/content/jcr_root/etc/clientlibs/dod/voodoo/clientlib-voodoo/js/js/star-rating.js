var radios = document.querySelectorAll('#star_rating input[type=radio]');
var output = document.querySelector('#star_rating output');

var do_something = function(stars) {
    // An AJAX request could send the data to the server
    output.textContent = stars;
};

// Iterate through all radio buttons and add a click
// event listener to the labels
Array.prototype.forEach.call(radios, function(el, i){
    var label = el.nextSibling.nextSibling;
    label.addEventListener("click", function(event){
        do_something(label.querySelector('span').textContent);
    });
});
