import Cookie from 'js-cookie'
import includes from 'array-includes'

$(document).on('click', '[data-form-id]', function () {
    var $a = $(this)
    var formId = $a.data('form-id')
    var bookmarks = Cookie.getJSON('form-bookmarks') || []
    var bookmarked = includes(bookmarks, formId)

    if (bookmarked) {
        bookmarks = bookmarks.filter(f => f !== formId)
        $a.find('img').attr('src', 'assets/icons/bookmark.svg')
    } else {
        bookmarks.push(formId)
        $a.find('img').attr('src', 'assets/icons/bookmarked-primary.svg')
    }

    Cookie.set('form-bookmarks', bookmarks, { expires: 365 })

    $(`[data-form-id="${formId}"]`).map(function () {
        const $a = $(this)
        setBookmarked($a, !bookmarked)
    })
})

function setBookmarked ($a, bookmarked) {
    bookmarked
        ? $a.find('img').attr('src', 'assets/icons/bookmarked-primary.svg')
        : $a.find('img').attr('src', 'assets/icons/bookmark.svg')
}

$(() => {
    const bookmarks = Cookie.getJSON('form-bookmarks') || []

    $('[data-form-id]').map(function () {
        const $a = $(this)
        const formId = $a.data('form-id')
        setBookmarked($a, includes(bookmarks, formId))
    })
})
