package com.adobe.core.service;

import org.json.JSONArray;

public interface GetUnpublishedNodeDetailsService {
	JSONArray getUnpublishedSponsorCRXNodeData(String path);
	JSONArray getUnpublishedSupplierCRXNodeData(String path);
	JSONArray getUnpublishedNewsCRXNodeData(String path);
}
