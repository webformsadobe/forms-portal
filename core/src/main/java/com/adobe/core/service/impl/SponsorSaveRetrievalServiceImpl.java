package com.adobe.core.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.query.Row;
import javax.jcr.query.RowIterator;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.SponsorSaveRetrievalService;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;

@Component(immediate = true,
		service = SponsorSaveRetrievalService.class,
		name = "com.adobe.core.service.SponsorSaveRetrievalService")
public class SponsorSaveRetrievalServiceImpl implements SponsorSaveRetrievalService {
	private static Logger log = LoggerFactory.getLogger(SponsorSaveRetrievalServiceImpl.class);

	private String nodePath = "/content/dodData/data/sponsorData";
	private String dodAssetPath = "/content/dam/dodassets";
	private String dodAFPath = "/content/dam/formsanddocuments/dodassets";
	private String sponsorJsonNodePath = "/content/dodData/data";
	private String dropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/dod/items/tabs/items/tab3/items/col1/items";
	private String formsDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/forms/items/tabs/items/tab1/items/col1/items";
	  
	@Reference
	private SlingRepository slingRepo;
	
	@Reference
	private Replicator replicator;
	
	@Reference
    private ResourceResolverFactory resolverFactory;
  
	@Reference
    private QueryBuilder builder;

	@Activate
	protected void activate(ComponentContext context) {
	}

	@Override
	public JSONObject saveDataToNode(String action, JSONObject inputJson, String id) {
		JSONObject respSponsorJsonObj = new JSONObject();
		try {
			log.info("Enter saveSponsorDataToNode");
			Session session = slingRepo.loginService("datawrite", null);
			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			
			// Create the path where sponsor data will be saved
			Node sponsorNode;
			Node sponsorJsonNode;
			if (!session.itemExists(nodePath)) {
				JcrUtil.createPath(nodePath, "sling:OrderedFolder", "sling:OrderedFolder", session, true);
			}
			
			if ("create".equals(action)) {
				String sponsorId = inputJson.getString("id");

				// Add node data
				if (!session.itemExists(nodePath + "/" + sponsorId)) {
					log.info("create node and update");
					sponsorNode = JcrUtil.createPath(nodePath + "/" + sponsorId, "nt:unstructured", "nt:unstructured",
							session, true);
					setNodeProperties(sponsorNode, inputJson, sponsorId);
					session.save();
					inputJson.put("DT_RowId", sponsorId);
					inputJson.put("id", sponsorId);
					JSONArray sponsorJsonArr = new JSONArray();
					sponsorJsonArr.put(inputJson);
					respSponsorJsonObj.put("data", sponsorJsonArr);
					//Add Sponsor Id and Name to json file
					if (!session.itemExists(sponsorJsonNodePath + "/sponsorID.json")) {
						sponsorJsonNode = session.getNode(sponsorJsonNodePath).addNode("sponsorID.json", "nt:file");
						Node jcrNode = sponsorJsonNode.addNode("jcr:content", "nt:resource");
						jcrNode.setProperty("jcr:mimeType", "application/json");
						jcrNode.setProperty("jcr:encoding", "utf-8");
						jcrNode.setProperty("jcr:data", "");
						session.save();
					} else {
						sponsorJsonNode = session.getNode(sponsorJsonNodePath + "/sponsorID.json");
					}
					if(sponsorJsonNode.hasNode("jcr:content")) {
						Node jcrNode = sponsorJsonNode.getNode("jcr:content");
						if(jcrNode.hasProperty("jcr:data")) {
							Binary rawData = jcrNode.getProperty("jcr:data").getBinary();
				            InputStream rawDataStream = rawData.getStream();
				            String jcrData = IOUtils.toString(rawDataStream, "utf-8");
							
							if("".equals(jcrData)) {
								JSONObject jsonData = new JSONObject();
								JSONArray jsonKeyValues = new JSONArray();
								JSONObject jsonKeyValue = new JSONObject();
								jsonKeyValue.put("text", sponsorId);
								jsonKeyValue.put("value", sponsorId);
								jsonKeyValues.put(jsonKeyValue);
								jsonData.put("options", jsonKeyValues);
								jcrNode.setProperty("jcr:data", jsonData.toString());
							} else {
								JSONObject jsonData = new JSONObject(jcrData);
								if(jsonData.has("options")){
									JSONArray jsonKeyValues = jsonData.getJSONArray("options");
									JSONObject jsonKeyValue = new JSONObject();
									jsonKeyValue.put("text", sponsorId);
									jsonKeyValue.put("value", sponsorId);
									jsonKeyValues.put(jsonKeyValue);
									jsonData.put("options", jsonKeyValues);
									jcrNode.setProperty("jcr:data", jsonData.toString());
								}
							}
						}
					}
					
					//Create node in conf folder for dropdown values; Workaround for saving schema every time on updating json file.
					Query query = queryManager
							.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ dropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
					QueryResult result = query.execute();
					NodeIterator nodeIter = result.getNodes();

					while (nodeIter.hasNext()) {
						Node node = nodeIter.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + sponsorId;
						Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured", "nt:unstructured",
								session, true);
						keyNode.setProperty("text", sponsorId);
						keyNode.setProperty("value", sponsorId);
						session.save();									
					}
					
					//Create node in conf folder for formdropdown values; Workaround for saving schema every time on updating json file.
					Query queryForms = queryManager
							.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ formsDropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
					QueryResult resultForms = queryForms.execute();
					NodeIterator nodeIterForms = resultForms.getNodes();

					while (nodeIterForms.hasNext()) {
						Node node = nodeIter.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + sponsorId;
						Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured", "nt:unstructured",
								session, true);
						keyNode.setProperty("text", sponsorId);
						keyNode.setProperty("value", sponsorId);
						session.save();									
					}
				} else {
					log.info("create but duplicate::" + sponsorId);
					respSponsorJsonObj.put("data", "Sponsor Id already exits.");
				}
			} else if ("edit".equals(action)) {
				// Edit node data
				String sponsorId = inputJson.getString("id");
				String sponsorName = inputJson.getString("sponsor");
				String key = sponsorName + "-" + sponsorId;
				String value = sponsorId;
				if (sponsorId.equals(id) || !session.itemExists(nodePath + "/" + sponsorId)) {
					log.info("edit node::" + id);
					sponsorNode = session.getNode(nodePath + "/" + id);
					sponsorNode.remove();
					session.save();
					sponsorNode = JcrUtil.createPath(nodePath + "/" + sponsorId, "nt:unstructured", "nt:unstructured",
							session, true);
					setNodeProperties(sponsorNode, inputJson, sponsorId);
					
					//Update node in conf folder for dropdown values; Workaround for saving schema every time on updating json file.
					Query query = queryManager
							.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ dropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
					QueryResult result = query.execute();
					NodeIterator nodeIter = result.getNodes();

					while (nodeIter.hasNext()) {
						Node node = nodeIter.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + sponsorId;
						Node keyNode = session.getNode(dropdownValuePath);
						keyNode.setProperty("text", sponsorId);
						keyNode.setProperty("value", sponsorId);
						session.save();									
					}
					
					//Update node in conf folder for formdropdown values; Workaround for saving schema every time on updating json file.
					Query queryForms = queryManager
							.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ formsDropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
					QueryResult resultForms = queryForms.execute();
					NodeIterator nodeIterForms = resultForms.getNodes();

					while (nodeIterForms.hasNext()) {
						Node node = nodeIter.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + sponsorId;
						Node keyNode = session.getNode(dropdownValuePath);
						keyNode.setProperty("text", sponsorId);
						keyNode.setProperty("value", sponsorId);
						session.save();									
					}

					inputJson.put("DT_RowId", sponsorId);
					JSONArray sponsorJsonArr = new JSONArray();
					sponsorJsonArr.put(inputJson);
					respSponsorJsonObj.put("data", sponsorJsonArr);
				} else {
					log.info("edit but duplicate::" + sponsorId);
					respSponsorJsonObj.put("data", "Sponsor Id already exits.");
				}
			} else if ("remove".equals(action)) {
				//Remove Sponsor Id and Name from json file
				sponsorJsonNode = session.getNode(sponsorJsonNodePath + "/sponsorID.json");
				if(sponsorJsonNode.hasNode("jcr:content")) {
					Node jcrNode = sponsorJsonNode.getNode("jcr:content");
					if(jcrNode.hasProperty("jcr:data")) {
						Binary rawData = jcrNode.getProperty("jcr:data").getBinary();
			            InputStream rawDataStream = rawData.getStream();
			            String jcrData = IOUtils.toString(rawDataStream, "utf-8");
						JSONObject jsonData = new JSONObject(jcrData);
						if(jsonData.has("options")){
							JSONArray jsonKeyValues = jsonData.getJSONArray("options");
							int index = -1;
							for (int i = 0; i < jsonKeyValues.length(); i++) {
								JSONObject jsonObj = jsonKeyValues.getJSONObject(i);
								if(jsonObj.get("value").equals(id)) {
									index = i;
									break;
								}
							}
							if(index != -1) {
								jsonKeyValues.remove(index);
								jsonData.put("options", jsonKeyValues);
								jcrNode.setProperty("jcr:data", jsonData.toString());
							}
						}
					}
				}
				
				String sponsorId = inputJson.getString("id");
				
				//Create node in conf folder for dropdown values; Workaround for saving schema every time on updating json file.
				Query query = queryManager
						.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
								+ dropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
				QueryResult result = query.execute();
				NodeIterator nodeIter = result.getNodes();

				while (nodeIter.hasNext()) {
					Node node = nodeIter.nextNode();
					String nodeName = node.getName();
					String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + sponsorId;
					Node ddNode = session.getNode(dropdownValuePath);
					ddNode.remove();
					session.save();									
				}
				
				//Create node in conf folder for formdropdown values; Workaround for saving schema every time on updating json file.
				Query queryForms = queryManager
						.createQuery("SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
								+ formsDropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'", Query.JCR_SQL2);
				QueryResult resultForms = queryForms.execute();
				NodeIterator nodeIterForms = resultForms.getNodes();

				while (nodeIterForms.hasNext()) {
					Node node = nodeIter.nextNode();
					String nodeName = node.getName();
					String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + sponsorId;
					Node ddNode = session.getNode(dropdownValuePath);
					ddNode.remove();
					session.save();									
				}
				
				//replicate deletion to publisher
				replicator.replicate(session, ReplicationActionType.DEACTIVATE, nodePath + "/" + id);
		        replicator.replicate(session, ReplicationActionType.DELETE, nodePath + "/" + id);
		        
		        //delete node from author
				sponsorNode = session.getNode(nodePath + "/" + id);
				sponsorNode.remove();
			}
			session.save();
			session.logout();
			log.info("After save saveSponsorDataToNode");
		} catch (Exception e) {
			log.error("Exception::: saveSponsorDataToNode", e);
			e.printStackTrace();
		}
		log.info("Exit saveSponsorDataToNode");
		return respSponsorJsonObj;
	}

	@Override
	public JSONObject getCRXNodeData() {
		JSONObject data = new JSONObject();
		try {
			log.info("Enter getCRXNodeData");
			Session session = slingRepo.loginService("datawrite", null);

			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			Query query = queryManager
					.createQuery("SELECT sponsor.* FROM [nt:unstructured] AS sponsor WHERE ISDESCENDANTNODE(sponsor, ["
							+ nodePath + "])", Query.JCR_SQL2);
			QueryResult result = query.execute();
			log.info("result columns" + result.getColumnNames()[0]);

			String[] columnNames = result.getColumnNames();
			RowIterator rowIter = result.getRows();
			data = new JSONObject(result.getRows());
			while (rowIter.hasNext()) {
				Row row = rowIter.nextRow();
				for (String columnName : columnNames) {
					Value value = row.getValue(columnName);
					log.info("column:::" + columnName + "::value:::" + value);
				}
			}
			session.save();
			session.logout();
			log.info("After save getCRXNodeData");
		} catch (Exception e) {
			log.info("Exception::: getCRXNodeData" + e);
		}
		log.info("Exit getCRXNodeData");
		return data;
	}
	
	public Boolean checkForReferences(String sponsor) {
		log.info("Enter checkForReferences");

		Boolean found = Boolean.valueOf(false);
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("sling.service.subservice", "datadownload");
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
			Session session = resourceResolver.adaptTo(Session.class);
			Map<String, String> map = new HashMap<String, String>();
			map.put("type", "dam:Asset");
			map.put("1_group_path", dodAssetPath);
			map.put("1_group_path", dodAFPath);
			map.put("1_group.p.or", "true");

			map.put("1_property", "jcr:content/metadata/sponsor");
			map.put("1_property.value", sponsor);
			builder = ((QueryBuilder) resourceResolver.adaptTo(QueryBuilder.class));

			log.info("Before create query");
			com.day.cq.search.Query query = builder.createQuery(PredicateGroup.create(map), session);
			SearchResult sr = query.getResult();
			log.info("Direct Acess to the form " + sr.getTotalMatches());
			if (sr.getTotalMatches() > 0L) {
				found = Boolean.valueOf(true);
			}

			session.logout();
		} catch (Exception e) {
			log.error("Exception checkForReferences:", e);
			e.printStackTrace();
		}
		log.info("Exit checkForReferences");
		return found;
	}

	private void setNodeProperties(Node sponsorNode, JSONObject inputJson, String sponsorId) {
		try {
			  sponsorNode.setProperty("sponsor", inputJson.getString("sponsor"));
		      sponsorNode.setProperty("heading1", inputJson.getString("heading1"));
		      sponsorNode.setProperty("heading2", inputJson.getString("heading2"));
		      sponsorNode.setProperty("contactOfficer", inputJson.getString("contactOfficer"));
		      sponsorNode.setProperty("contactPosition", inputJson.getString("contactPosition"));
		      sponsorNode.setProperty("location", inputJson.getString("location"));
		      sponsorNode.setProperty("phone", inputJson.getString("phone"));
		      sponsorNode.setProperty("fax", inputJson.getString("fax"));
		      sponsorNode.setProperty("email", inputJson.getString("email"));
		      sponsorNode.setProperty("mobile", inputJson.getString("mobile"));
		      sponsorNode.setProperty("secondaryContactOfficer", inputJson.getString("secondaryContactOfficer"));
		      sponsorNode.setProperty("secondaryContactPosition", inputJson.getString("secondaryContactPosition"));
		      sponsorNode.setProperty("secondaryLocation", inputJson.getString("secondaryLocation"));
		      sponsorNode.setProperty("secondaryPhone", inputJson.getString("secondaryPhone"));
		      sponsorNode.setProperty("secondaryFax", inputJson.getString("secondaryFax"));
		      sponsorNode.setProperty("secondaryEmail", inputJson.getString("secondaryEmail"));
		      sponsorNode.setProperty("secondaryMobile", inputJson.getString("secondaryMobile"));
		      sponsorNode.setProperty("id", sponsorId);
		      sponsorNode.setProperty("DT_RowId", sponsorId);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
