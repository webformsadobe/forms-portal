package com.adobe.core.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.jcr.*;

import com.adobe.core.constants.DepartmentOfDefenceConstants;
import com.adobe.core.service.ReportConfigurationService;
import com.day.cq.replication.Replicator;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.AssetBreakdownService;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

import static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT;


/**
 * This service populates asset meta data models
 */
@SuppressWarnings("unused")
@Component(immediate = true,
		service = AssetBreakdownService.class,
		name = "com.adobe.core.service.AssetBreakdownService")
public class AssetBreakdownServiceImpl implements AssetBreakdownService {
	private static final Logger log = LoggerFactory.getLogger(AssetBreakdownServiceImpl.class);

	private static final String NEW_LINE_SEPARATOR = "\n";

	private static final DateFormat DATE_PARSE_PATTERN  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	private static final DateFormat DATE_FORMAT_PATTERN = new SimpleDateFormat("yyyy-MM-dd");

	private String rootAssetNodePath = "/content/dam/dodassets/";
	private String rootAFNodePath = "/content/dam/formsanddocuments/dodassets/";


	@Reference
	private SlingRepository slingRepo;

	@Reference
	private ResourceResolverFactory resolverFactory;

	@Reference
	private QueryBuilder builder;

	@Reference
	private Replicator replicator;

	@Reference
	private ReportConfigurationService reportPropertyService;


	@Activate
	protected void activate() {
		// Empty activation method.
	}


	@Override
	public JSONObject getAssetRecordData(String assetType) {
		JSONObject data = new JSONObject();
		try {
			if ("all".equalsIgnoreCase(assetType)) {
				data = getAllRecords();
			} else {
				data = getAssetRecords(assetType);
			}
		} catch (Exception e) {
			log.error("Exception getAssetRecordData: {}", e.getMessage());
		}

		return data;
	}


	@Override
	public StringBuilder getAssetReportData(String assetType) {
		StringBuilder data = null;
		try {
			data = getAssetReport(assetType);
		} catch (Exception e) {
			log.error("Exception getAssetReportData: {}", e.getMessage());
		}

		return data;
	}


	private JSONObject getAllRecords() {
		JSONObject data = new JSONObject();
		JSONObject reportData = new JSONObject();

		reportData.put("pdf", getAssetRecords("pdf").get("pdf"));
		reportData.put("itp", getAssetRecords("itp").get("itp"));
		reportData.put("paperform", getAssetRecords("paperform").get("paperform"));
		reportData.put("adaptiveform", getAssetRecords("adaptiveform").get("adaptiveform"));
		data.put("all", reportData);

		return data;
	}

	/*
	 * private JSONObject getAllReportRecords() {
	 * log.info("Enter getAllReportRecords."); JSONObject data = new
	 * JSONObject(); JSONObject reportData = new JSONObject();
	 * reportData.put("pdf", getAssetReport("pdf").get("pdf"));
	 * reportData.put("itp", getAssetReport("itp").get("itp"));
	 * reportData.put("paperform",
	 * getAssetReport("paperform").get("paperform"));
	 * reportData.put("adaptiveform",
	 * getAssetReport("adaptiveform").get("adaptiveform")); data.put("all",
	 * reportData); log.info("Enter getAllReportRecords."); return data; }
	 */

	private JSONObject getAssetRecords(String assetType) {
		log.trace("Enter getAssetRecords");
		JSONObject data = new JSONObject();
		Map<String, List<Hit>> groupedNodes;
		JSONObject reportData = new JSONObject();

		try (ResourceResolver resourceResolver = getServiceResolver(DepartmentOfDefenceConstants.SERVICES.ADMIN_READER)){
			Session session = resourceResolver.adaptTo(Session.class);
			final Map<String, String> map = createSearchParameters(assetType);

			builder = resourceResolver.adaptTo(QueryBuilder.class);
			final com.day.cq.search.Query query = builder.createQuery(PredicateGroup.create(map), session);
			final SearchResult sr = query.getResult();

			List<Hit> hits = sr.getHits();
			groupedNodes = hits.stream().collect(Collectors.groupingBy(hit -> {
				try {
					String key = "";
					final String path = hit.getPath();
	                final Resource rs = resourceResolver.getResource(path);
	                if (rs != null) {
						Node node = rs.adaptTo(Node.class);
						if (node != null && node.hasNode(JCR_CONTENT) && node.getNode(JCR_CONTENT).hasNode(DepartmentOfDefenceConstants.METADATA_NODE)
								&& node.getNode(JCR_CONTENT).getNode(DepartmentOfDefenceConstants.METADATA_NODE).hasProperty(DepartmentOfDefenceConstants.SPONSOR_METADATA)) {
							key = node.getNode(JCR_CONTENT).getNode(DepartmentOfDefenceConstants.METADATA_NODE).getProperty(DepartmentOfDefenceConstants.SPONSOR_METADATA).getString();
						}
					}
					return key;
				} catch (ValueFormatException e) {
					log.error("Exception ValueFormatException: {}", e.getMessage());
				} catch (PathNotFoundException e) {
					log.error("Exception PathNotFoundException: {}", e.getMessage());
				} catch (RepositoryException e) {
					log.error("Exception RepositoryException: {}", e.getMessage());
				}
				return null;
			}));
			for (String key : groupedNodes.keySet()) {
				reportData.put(key, groupedNodes.get(key).size());
			}
			data.put(assetType, reportData);
		} catch (Exception e) {
			log.error("Exception getAssetRecords:", e);
		}
		log.trace("Exit getAssetRecords");
		return data;
	}


	private StringBuilder getAssetReport(String assetType) {
		log.trace("Enter getAssetReport");
		Map<String, List<Hit>> groupedNodes;
		StringBuilder result = new StringBuilder();

		final Pair<List<String>, List<String>> metadataProperties = getMetadataFields();
		final List<String> headers = metadataProperties.getLeft();
		final List<String> fields  = metadataProperties.getRight();

		write(result, headers);

		try (ResourceResolver resourceResolver = getServiceResolver(DepartmentOfDefenceConstants.SERVICES.ADMIN_READER)){
			Session session = resourceResolver.adaptTo(Session.class);

			final Map<String, String> map = createSearchParameters(assetType);

			builder = resourceResolver.adaptTo(QueryBuilder.class);
			final com.day.cq.search.Query query = builder.createQuery(PredicateGroup.create(map), session);
			final SearchResult sr = query.getResult();
			List<Hit> hits = sr.getHits();

			groupedNodes = hits.stream().collect(Collectors.groupingBy(hit -> {
				String key = "";
				try {
					final String path = hit.getPath();
	                final Resource rs = resourceResolver.getResource(path);
	                if (rs != null) {
						Node node = rs.adaptTo(Node.class);
						if (node != null && node.hasNode(JCR_CONTENT) && node.getNode(JCR_CONTENT).hasNode(DepartmentOfDefenceConstants.METADATA_NODE)
								&& node.getNode(JCR_CONTENT).getNode(DepartmentOfDefenceConstants.METADATA_NODE).hasProperty(DepartmentOfDefenceConstants.SPONSOR_METADATA)) {
							key = node.getNode(JCR_CONTENT).getNode(DepartmentOfDefenceConstants.METADATA_NODE).getProperty(DepartmentOfDefenceConstants.SPONSOR_METADATA).getString();
						}
					}
				} catch (ValueFormatException e) {
					log.error("Exception ValueFormatException: {}", e.getMessage());
				} catch (PathNotFoundException e) {
					log.error("Exception PathNotFoundException: {}", e.getMessage());
				} catch (RepositoryException e) {
					log.error("Exception RepositoryException: {}", e.getMessage());
				}
				return key;
			}));

			for (String key : groupedNodes.keySet()) {
				for (Hit hit : groupedNodes.get(key)) {
					final String path = hit.getPath();
	                final Resource rs = resourceResolver.getResource(path);
	                if (rs != null) {
						Node node = rs.adaptTo(Node.class);
						if (node != null && node.hasNode(JCR_CONTENT) && node.getNode(JCR_CONTENT).hasNode(DepartmentOfDefenceConstants.METADATA_NODE)) {
							List<String> records = new ArrayList<>(headers.size());
							Node metadataNode = node.getNode(JCR_CONTENT).getNode(DepartmentOfDefenceConstants.METADATA_NODE);
							for (String fieldName : fields) {
								String propertyValue = getProperty(metadataNode, fieldName);
								if (!propertyValue.isEmpty()) {
									if (fieldName.equals("versionDate")) {
										Date holdingDate = DATE_PARSE_PATTERN.parse(propertyValue);
										propertyValue = DATE_FORMAT_PATTERN.format(holdingDate);
									}

									if (propertyValue.contains(",")) {
										propertyValue = "\"" + propertyValue + "\"";
									}
									records.add(propertyValue);
								} else {
									records.add("");
								}
							}

							records.add(Boolean.toString(replicator.getReplicationStatus(session, node.getPath()).isActivated()));

							// Determine the file type of hte underlying binary.
							final String filename = node.getName().toUpperCase();
							boolean isItp = filename.contains(".ITP");
							boolean isPdf = filename.contains(".PDF");

							String fileType = "UNKNOWN";
							if (isItp && !isPdf) {
								fileType = "ITP";
							} else if (!isItp && isPdf) {
								fileType = "PDF";
							}
							if (metadataNode.hasProperty("coverpage") &&
									metadataNode.getProperty("coverpage").getBoolean()) {
								fileType = "PAPER";
							}
							records.add(fileType);

							write(result, records);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception getAssetReport: {}", e.getMessage());
		}
		log.trace("Exit getAssetReport");
		return result;
	}


	/**
	 * Adds a collection of strings as a CSV-formatted row to the stringBuffer provided.
	 *
	 * @param buffer The output buffer to write into
	 * @param items The collection of strings to add as a row.
	 */
	public void write(StringBuilder buffer, Iterable<String> items) {
		buffer.append(String.join(",", items));
		buffer.append(NEW_LINE_SEPARATOR);
	}


	/**
	 * Populates the search parameter map with the parameters required to execute the given search type.
	 */
	private Map<String, String> createSearchParameters(String searchType) {
		searchType = searchType.toLowerCase();
		Map<String, String> parameters = new HashMap<>();

		// Enter the common parameters.
		parameters.put("type", "dam:Asset");
		parameters.put("p.offset", "0");
		parameters.put("p.limit", "-1");

		switch (searchType) {
			case "pdf":
				parameters.put("path", rootAssetNodePath);

				parameters.put("1_property", "jcr:content/metadata/dc:format");
				parameters.put("1_property.value", "application/pdf");
				parameters.put("2_property", "jcr:content/metadata/coverpage");
				parameters.put("2_property.value", "false");
				break;
			case "itp":
				parameters.put("path", rootAssetNodePath);

				parameters.put("1_property", "jcr:content/metadata/dc:format");
				parameters.put("1_property.value", "application/vnd.shana.informed.formtemplate");
				parameters.put("2_property", "jcr:content/metadata/coverpage");
				parameters.put("2_property.value", "false");
				break;
			case "paperform":
				parameters.put("path", rootAssetNodePath);

				parameters.put("1_property", "jcr:content/metadata/coverpage");
				parameters.put("1_property.value", "true");
				break;
			case "adaptiveform":
				parameters.put("path", rootAFNodePath);

				parameters.put("1_property", "jcr:content/jcr:primaryType");
				parameters.put("1_property.value", "dam:AssetContent");
				parameters.put("2_property", "jcr:content/metadata/coverpage");
				parameters.put("2_property.value", "false");
				break;
			case "all":
				parameters.put("group.1_group.path", rootAFNodePath);
				parameters.put("group.2_group.path", rootAssetNodePath);

				parameters.put("group.1_group.1_property", "jcr:content/jcr:primaryType");
				parameters.put("group.1_group.1_property.value", "dam:AssetContent");

				parameters.put("group.2_group.1_property", "jcr:content/metadata/dc:format");
				parameters.put("group.2_group.1_property.value", "application/pdf");
				parameters.put("group.2_group.2_property", "jcr:content/metadata/dc:format");
				parameters.put("group.2_group.2_property.value", "application/vnd.shana.informed.formtemplate");
				parameters.put("group.2_group.p.or", "true");
				parameters.put("group.p.or", "true");
				break;
			default:
				break;
		}

		return parameters;
	}


	/**
	 * Creates the list of metadata properties to include in the report, and their corresponding column names.
	 *
	 */
	private Pair<List<String>, List<String>> getMetadataFields() {
		// Begin with the three standard fields.
		List<String> headers = new LinkedList<>();
		headers.add("Sponsor ID");
		headers.add("Form Name");
		headers.add("Form Number");

		List<String> properties = new LinkedList<>();
		properties.add("sponsor");
		properties.add("formName");
		properties.add("formNumber");

		// Add any custom properties which were added to the configuration.
		String[] unprocessedProperties = reportPropertyService.getPropertyNames();
		for(String unprocessedProperty: unprocessedProperties) {
			String[] parts = unprocessedProperty.split("=");
			if (parts.length == 2) {
				headers.add(parts[0]);
				properties.add(parts[1]);
			} else {
				log.warn("Ignoring misconfigured field name: {}", unprocessedProperty);
			}
		}

		// Add the final two fields at the end.
		// Header only - the fields are added by hard coding. Poor design and a candidate @todo some refactoring here.
		headers.add("Is Published");
		headers.add("Form Type");

		return new ImmutablePair<>(headers, properties);
	}


	/**
	 * Returns a resource resolver in the context of the service provided.
	 */
	private ResourceResolver getServiceResolver(String serviceName) throws LoginException {
		final Map<String, Object> param = new HashMap<>();
		param.put(ResourceResolverFactory.SUBSERVICE, serviceName);

		return resolverFactory.getServiceResourceResolver(param);
	}


	/**
	 * Returns the value of property stored in node. If the property is multivalued, it will return a string
	 * containing all the values in a listish format.
	 */
	private String getProperty(Node node, String propertyName) throws RepositoryException{
		String result = "";
		if (node.hasProperty(propertyName)) {
			Property property = node.getProperty(propertyName);

			if (property.isMultiple()) {
				Value[] values = property.getValues();
				List<String> properties = new LinkedList<>();

				for (Value value: values) {
					properties.add("\"" + value.toString() + "\"");
				}

				result = "[" +
						 String.join(", ", properties) +
						 "]";
			} else {
				result = property.getString();
			}
		}

		return result;
	}
}
