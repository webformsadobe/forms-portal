package com.adobe.core.service.impl;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.nodetype.NodeType;

import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.CustomReplicatorService;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;


@SuppressWarnings("unused")
@Component(immediate = true,
		service = CustomReplicatorService.class,
		name = "com.adobe.core.service.CustomReplicatorService")
public class CustomReplicatorServiceImpl implements CustomReplicatorService {
	private static Logger log = LoggerFactory.getLogger(CustomReplicatorServiceImpl.class);

	private final String contentNodePath = "/content/dodData/data";
	private final String newsNodePath = "/content/dodData/data/newsData";
	private final String sponsorNodePath = "/content/dodData/data/sponsorData";
	private final String supplierNodePath = "/content/dodData/data/supplierData";
	private final String dropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/dod/items/tabs/items/tab3/items/col1/items";
	private final String formsDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/forms/items/tabs/items/tab1/items/col1/items";
	
	@Reference
	private SlingRepository slingRepo;
	
	@Reference
    private Replicator replicator;

	@Activate
	protected void activate(ComponentContext context) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void replicate(String dataNode) throws RepositoryException, ReplicationException {
		log.trace("Enter replicate()");

		Session session = slingRepo.loginService("datawrite", null);
		replicator.replicate(session, ReplicationActionType.ACTIVATE, contentNodePath);

		if ("news".equals(dataNode)) {
			replicateNodes(newsNodePath, session);
		} else if ("sponsor".equals(dataNode)) {
			replicateNodes(sponsorNodePath, session);
		} else if ("supplier".equals(dataNode)) {
			replicateNodes(supplierNodePath, session);
		} else if ("all".equals(dataNode)) {
			replicateNodes(newsNodePath, session);
			replicateNodes(sponsorNodePath, session);
			replicateNodes(supplierNodePath, session);

			replicator.replicate(session, ReplicationActionType.ACTIVATE, dropdownPath);
			replicateNodes(dropdownPath, session);

			replicator.replicate(session, ReplicationActionType.ACTIVATE, formsDropdownPath);
			replicateNodes(formsDropdownPath, session);
		}

		session.save();
		session.logout();

		log.trace("Exit replicate()");
	}

	/**
	 * Activates all child nodes below baseNode.
	 */
	private void replicateNodes(String baseNodePath, Session session) throws ReplicationException, RepositoryException {
		log.trace("Replication nodes below {}", baseNodePath);

		Node baseNode = session.getNode(baseNodePath);
		NodeIterator childNodes  = baseNode.getNodes();
		List<Node> inactiveNodes = getNonActiveNodes(childNodes);

		for (Node inactiveNode: inactiveNodes) {
			replicator.replicate(session, ReplicationActionType.ACTIVATE,
					inactiveNode.getPath());
		}

		log.trace("Exit replicate()");
	}


	private List<Node> getNonActiveNodes(NodeIterator childNodes) {
		log.trace("Enter getNonActiveNodes");

		Iterable<Node> nodeIterable = () -> childNodes;
		Stream<Node> nodeStream = StreamSupport.stream(nodeIterable.spliterator(), false);
		
		List<Node> filteredNodes = nodeStream.filter((node) -> {
			try {
				return (!node.hasProperty("cq:lastReplicationAction")
						|| !node.getProperty("cq:lastReplicationAction").getString().equals("Activate"))
						&& node.getPrimaryNodeType().isNodeType(NodeType.NT_UNSTRUCTURED);
			} catch (ValueFormatException e) {
				log.error("Exception ValueFormatException:", e);
				e.printStackTrace();
			} catch (PathNotFoundException e) {
				log.error("Exception PathNotFoundException:", e);
				e.printStackTrace();
			} catch (RepositoryException e) {
				log.error("Exception RepositoryException:", e);
				e.printStackTrace();
			}

			return false;
		}).collect(Collectors.toList());

		log.trace("Exit getNonActiveNodes:: {}", filteredNodes.size());
		return filteredNodes;
	}
}
