package com.adobe.core.service;

import org.json.JSONObject;

/**
 * Service that fetches the asset meta data
 */
public interface AssetBreakdownService {
	StringBuilder getAssetReportData(String assetType);
	JSONObject getAssetRecordData(String assetType);
}
