package com.adobe.core.service;

import ch.qos.logback.classic.Logger;

public interface LoggerUtilsService {
	Logger createLogger(String loggerName, String dataSourceName);
}
