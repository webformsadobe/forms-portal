package com.adobe.core.service;

import org.json.JSONObject;

public interface NewsSaveRetrievalService {
	JSONObject saveDataToNode(String action, JSONObject data, String id);
}
