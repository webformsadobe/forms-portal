package com.adobe.core.service.impl;

import com.adobe.core.configurations.ReportingConfiguration;
import com.adobe.core.service.ReportConfigurationService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;

@Component(
        immediate = true,
        service = ReportConfigurationService.class,
        name = "com.adobe.core.service.ReportConfigurationService")
@Designate(ocd = ReportingConfiguration.class)
public class ReportConfigurationServiceImpl implements ReportConfigurationService{
    private ReportingConfiguration config;

    @Activate
    protected void activate(ReportingConfiguration config) {
        this.config = config;
    }

    @Override
    public String[] getPropertyNames() {
        return config.getPropertyNames();
    }
}
