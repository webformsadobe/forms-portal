package com.adobe.core.service;

import com.day.cq.replication.ReplicationException;

import javax.jcr.RepositoryException;

/**
 * Service that replicates userdata
 */
public interface CustomReplicatorService {
	void replicate(String dataNode) throws RepositoryException, ReplicationException;
}
