package com.adobe.core.service.impl;

import java.text.ParseException;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import com.adobe.core.models.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.AssetMetaDataService;
import com.day.cq.commons.jcr.JcrUtil;

@Component(immediate = true,
		service = AssetMetaDataService.class,
		name = "com.adobe.core.service.AssetMetaDataService")
public class AssetMetaDataServiceImpl implements AssetMetaDataService {
	private static final Logger log = LoggerFactory.getLogger(AssetMetaDataServiceImpl.class);

	private String dropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/dod/items/tabs/items/tab3/items/col1/items";
	private String formsDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/forms/items/tabs/items/tab3/items/col1/items";
	private String supplierDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/dod/items/tabs/items/tab3/items/col1/items";
	private String supplierFormsDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/forms/items/tabs/items/tab3/items/col1/items";
	private String jsonDataNodePath = "/content/dodData/data";
	private Session session;

	@Reference
	private SlingRepository slingRepo;

	@Activate
	protected void activate() {
	}

	/**
	 * Obtains an AssetMetaData model object for the given asset
	 *
	 * @param assetPath The location of the asset to read data from.
	 * @param request The incoming HTTP request object
	 * @return An AssetMetaData object holding the asset metadata for the given asset. If no such asset is found, the
	 * 			object returned will contain no data.
	 * @throws RepositoryException if an error occurs reading from the repository.
	 */
	public AssetMetaData getAssetMetadata(String assetPath, SlingHttpServletRequest request)
			throws RepositoryException {
		log.trace("Enter AssetMetaDataServiceImpl.getAssetMetaData()");
		ResourceResolver resolver = request.getResourceResolver();
		Resource assetResource = resolver.getResource(assetPath);
		AssetMetaData assetMetaData = AssetMetaDataFactory.create();
		if (null != assetResource) {
			Node node = assetResource.adaptTo(Node.class);
			if (node != null) {
				if ((node.hasNode("jcr:content")) && (node.getNode("jcr:content").hasNode("metadata"))) {
					Node metaDataNode = node.getNode("jcr:content").getNode("metadata");
					if (metaDataNode != null) {
						assetMetaData = getMetadataProperties(metaDataNode);
					}
				}
			}
		}
		log.trace("Exit AssetMetaDataServiceImpl.getAssetMetaData()");
		return assetMetaData;
	}

	private AssetMetaData getMetadataProperties(Node metaDataNode) {
		log.trace("Enter AssetMetaDataServiceImpl.getMetadataProperties()");

		AssetMetaData assetMetaData = null;
		try {
			assetMetaData = AssetMetaDataFactory.create(metaDataNode);

			final String sponsorId  = assetMetaData.getSponsor();
			final String supplierId = assetMetaData.getSupplier();

			SponsorData  sponsorDataObj  = SponsorFactory.createIfExists(sponsorId, slingRepo, log);
			SupplierData supplierDataObj = SupplierFactory.createIfExists(supplierId, slingRepo, log);

			assetMetaData.setSponsorData(sponsorDataObj);
			assetMetaData.setSupplierData(supplierDataObj);

			log.trace("Exit AssetMetaDataServiceImpl.getMetadataProperties()");
		} catch (RepositoryException e) {
			log.error("RepositoryException", e);
			e.printStackTrace();
		} catch (ParseException e) {
			log.error("ParseException", e);
			e.printStackTrace();
		}
		return assetMetaData;
	}

	public void syncSponsorAndSupplierDetails() {
		JSONObject sponsorData  = new JSONObject();
		JSONObject supplierData = new JSONObject();

		String sponsorItemNodeName = "";
		String formSponsorItemNodeName = "";
		String supplierItemNodeName = "";
		String formSupplierItemNodeName = "";
		try {
			log.trace("Enter syncSponsorAndSupplierDetails");
			session = slingRepo.loginService("datawrite", null);
			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			
			// Remove all values from sponsor dropdown

			Query ddQuery = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ dropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'",
					"JCR-SQL2");

			QueryResult ddResult = ddQuery.execute();
			NodeIterator nodeIter = ddResult.getNodes();
			if (nodeIter.hasNext()) {
				Node node = nodeIter.nextNode();
				sponsorItemNodeName = node.getName();

				Node dropdownNode = session.getNode(dropdownPath + "/" + sponsorItemNodeName + "/items/");
				NodeIterator ddNodes = dropdownNode.getNodes();
				while (ddNodes.hasNext()) {
					Node ddNode = ddNodes.nextNode();
					ddNode.remove();
					session.save();
				}
			}

			Query ddFormQuery = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ formsDropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/sponsor'",
					"JCR-SQL2");

			QueryResult ddFormResult = ddFormQuery.execute();
			NodeIterator formNodeIter = ddFormResult.getNodes();
			if (formNodeIter.hasNext()) {
				Node node = formNodeIter.nextNode();
				formSponsorItemNodeName = node.getName();

				Node dropdownNode = session.getNode(formsDropdownPath + "/" + formSponsorItemNodeName + "/items/");
				NodeIterator ddNodes = dropdownNode.getNodes();
				while (ddNodes.hasNext()) {
					Node ddNode = ddNodes.nextNode();
					ddNode.remove();
					session.save();
				}
			}

			// Add new values to sponsor dropdown
			List<SponsorData> sponsors = SponsorFactory.createAllAsList(slingRepo, log);
			JSONArray jsonArray = new JSONArray();
			for (SponsorData sponsor: sponsors) {
				final String sponsorId = sponsor.getSponsorId();
				if (sponsorId != null && !sponsorId.isEmpty()) {

					JSONObject obj = new JSONObject();
					obj.put("text", sponsorId);
					obj.put("value", sponsorId);
					jsonArray.put(obj);

					String dropdownValuePath = dropdownPath + "/" + sponsorItemNodeName + "/items/" + sponsorId;
					Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured", "nt:unstructured", session,
							true);

					keyNode.setProperty("text", sponsorId);
					keyNode.setProperty("value", sponsorId);

					String formDropdownValuePath = formsDropdownPath + "/" + formSponsorItemNodeName + "/items/"
							+ sponsorId;
					Node formKeyNode = JcrUtil.createPath(formDropdownValuePath, "nt:unstructured", "nt:unstructured",
							session, true);

					formKeyNode.setProperty("text", sponsorId);
					formKeyNode.setProperty("value", sponsorId);
				}
			}
			session.save();

			// Add all values to sponsor json file
			sponsorData.put("options", jsonArray);
			if (!session.itemExists(jsonDataNodePath + "/sponsorID.json")) {
				Node sponsorJsonNode = session.getNode(jsonDataNodePath).addNode("sponsorID.json", "nt:file");
				Node jcrNode = sponsorJsonNode.addNode("jcr:content", "nt:resource");
				jcrNode.setProperty("jcr:mimeType", "application/json");
				jcrNode.setProperty("jcr:encoding", "utf-8");
				jcrNode.setProperty("jcr:data", "");
				session.save();
			}
			Node sponsorJsonNode = session.getNode(jsonDataNodePath + "/sponsorID.json");
			if (sponsorJsonNode.hasNode("jcr:content")) {
				Node jcrNode = sponsorJsonNode.getNode("jcr:content");
				jcrNode.setProperty("jcr:data", "");
				jcrNode.setProperty("jcr:data", sponsorData.toString());
				session.save();
			}

			// Remove all values from supplier dropdown
			Query ddSupplierQuery = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ supplierDropdownPath + "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
					"JCR-SQL2");

			QueryResult ddSupplierResult = ddSupplierQuery.execute();
			NodeIterator supplierNodeIter = ddSupplierResult.getNodes();
			if (supplierNodeIter.hasNext()) {
				Node node = supplierNodeIter.nextNode();
				supplierItemNodeName = node.getName();

				Node dropdownNode = session.getNode(supplierDropdownPath + "/" + supplierItemNodeName + "/items/");
				NodeIterator ddNodes = dropdownNode.getNodes();
				while (ddNodes.hasNext()) {
					Node ddNode = ddNodes.nextNode();
					ddNode.remove();
					session.save();
				}
			}

			Query ddSupplierFormQuery = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ supplierFormsDropdownPath
							+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
					"JCR-SQL2");

			QueryResult ddSupplierFormResult = ddSupplierFormQuery.execute();
			NodeIterator supplierFormNodeIter = ddSupplierFormResult.getNodes();
			if (supplierFormNodeIter.hasNext()) {
				Node node = supplierFormNodeIter.nextNode();
				formSupplierItemNodeName = node.getName();

				Node dropdownNode = session
						.getNode(supplierFormsDropdownPath + "/" + formSupplierItemNodeName + "/items/");
				NodeIterator ddNodes = dropdownNode.getNodes();
				while (ddNodes.hasNext()) {
					Node ddNode = ddNodes.nextNode();
					ddNode.remove();
					session.save();
				}
			}

			// Get all of the suppliers
			List<SupplierData> suppliers = SupplierFactory.createAllAsList(slingRepo, log);
			JSONArray supplierJsonArray = new JSONArray();
			for (SupplierData supplier: suppliers) {
				final String supplierId = supplier.getSupplierId();

				if (supplierId != null && !supplierId.isEmpty()) {
					JSONObject obj = new JSONObject();
					obj.put("text", supplierId);
					obj.put("value", supplierId);
					supplierJsonArray.put(obj);

					String dropdownValuePath = supplierDropdownPath + "/" + supplierItemNodeName + "/items/" + supplierId;
					Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured", "nt:unstructured", session,
							true);

					keyNode.setProperty("text", supplierId);
					keyNode.setProperty("value", supplierId);

					String formDropdownValuePath = supplierFormsDropdownPath + "/" + formSupplierItemNodeName + "/items/"
							+ supplierId;
					Node formKeyNode = JcrUtil.createPath(formDropdownValuePath, "nt:unstructured", "nt:unstructured",
							session, true);

					formKeyNode.setProperty("text", supplierId);
					formKeyNode.setProperty("value", supplierId);
				}
			}
			session.save();

			// Add all values to supplier json file
			supplierData.put("options", supplierJsonArray);
			if (!session.itemExists(jsonDataNodePath + "/supplierID.json")) {
				Node supplierJsonNode = session.getNode(jsonDataNodePath).addNode("supplierID.json", "nt:file");
				Node jcrNode = supplierJsonNode.addNode("jcr:content", "nt:resource");
				jcrNode.setProperty("jcr:mimeType", "application/json");
				jcrNode.setProperty("jcr:encoding", "utf-8");
				jcrNode.setProperty("jcr:data", "");
				session.save();
			}
			Node supplierJsonNode = session.getNode(jsonDataNodePath + "/supplierID.json");
			if (supplierJsonNode.hasNode("jcr:content")) {
				Node jcrNode = supplierJsonNode.getNode("jcr:content");
				jcrNode.setProperty("jcr:data", "");
				jcrNode.setProperty("jcr:data", supplierData.toString());
				session.save();
			}

			log.trace("After save syncSponsorAndSupplierDetails");
		} catch (Exception e) {
			log.error("Exception::: syncSponsorAndSupplierDetails", e);
			e.printStackTrace();
		}
		log.trace("Exit syncSponsorAndSupplierDetails");
	}
}
