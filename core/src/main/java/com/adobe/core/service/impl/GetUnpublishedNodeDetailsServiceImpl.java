package com.adobe.core.service.impl;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.GetUnpublishedNodeDetailsService;


@Component(immediate = true,
		service = GetUnpublishedNodeDetailsService.class,
		name = "com.adobe.core.service.GetUnpublishedNodeDetailsService")
public class GetUnpublishedNodeDetailsServiceImpl implements GetUnpublishedNodeDetailsService {
	private static Logger log = LoggerFactory.getLogger(GetUnpublishedNodeDetailsServiceImpl.class);

	private Session session;

	@Reference
	private SlingRepository slingRepo;

	@Activate
	protected void activate(ComponentContext context) {
	}
	
	@Override
	public JSONArray getUnpublishedSponsorCRXNodeData(String path) {
		JSONArray data = new JSONArray();
		try {
			log.info("Enter getUnpublishedSponsorCRXNodeData");
			session = slingRepo.loginService("datawrite", null);

			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			Query query = queryManager
					.createQuery("SELECT sponsor.* FROM [nt:unstructured] AS sponsor WHERE ISDESCENDANTNODE(sponsor, ["
							+ path + "]) AND (sponsor.[cq:lastReplicationAction] IS NULL OR sponsor.[cq:lastReplicationAction] <> 'Activate')" , Query.JCR_SQL2);
			QueryResult result = query.execute();
			NodeIterator nodeItr = result.getNodes();
			
			while (nodeItr.hasNext()) {
				Node node = nodeItr.nextNode();
				JSONObject jsonObj = new JSONObject();
				getSponsorNodeProperties(node, jsonObj);
				data.put(jsonObj);
			}
			session.save();
			log.info("After save getUnpublishedSponsorCRXNodeData");
		} catch (Exception e) {
			log.info("Exception::: getUnpublishedSponsorCRXNodeData" + e);
		}
		log.info("Exit getUnpublishedSponsorCRXNodeData");
		return data;
	}

	@Override
	public JSONArray getUnpublishedSupplierCRXNodeData(String path) {
		JSONArray data = new JSONArray();
		try {
			log.info("Enter getUnpublishedSupplierCRXNodeData");
			session = slingRepo.loginService("datawrite", null);

			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			Query query = queryManager
					.createQuery("SELECT supplier.* FROM [nt:unstructured] AS supplier WHERE ISDESCENDANTNODE(supplier, ["
							+ path + "]) AND (supplier.[cq:lastReplicationAction] IS NULL OR supplier.[cq:lastReplicationAction] <> 'Activate')", Query.JCR_SQL2);
			QueryResult result = query.execute();
			NodeIterator nodeItr = result.getNodes();

			while (nodeItr.hasNext()) {
				Node node = nodeItr.nextNode();
				JSONObject jsonObj = new JSONObject();
				getSupplierNodeProperties(node, jsonObj);
				data.put(jsonObj);
			}
			session.save();
			log.info("After save getUnpublishedSupplierCRXNodeData");
		} catch (Exception e) {
			log.info("Exception::: getUnpublishedSupplierCRXNodeData" + e);
		}
		log.info("Exit getUnpublishedSupplierCRXNodeData");
		return data;
	}

	@Override
	public JSONArray getUnpublishedNewsCRXNodeData(String path) {
		JSONArray data = new JSONArray();
		try {
			log.info("Enter getUnpublishedNewsCRXNodeData");
			session = slingRepo.loginService("datawrite", null);

			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			Query query = queryManager
					.createQuery("SELECT news.* FROM [nt:unstructured] AS news WHERE ISDESCENDANTNODE(news, ["
							+ path + "]) AND (news.[cq:lastReplicationAction] IS NULL OR news.[cq:lastReplicationAction] <> 'Activate')", Query.JCR_SQL2);
			QueryResult result = query.execute();
			NodeIterator nodeItr = result.getNodes();
			
			while (nodeItr.hasNext()) {
				Node node = nodeItr.nextNode();
				JSONObject jsonObj = new JSONObject();
				getNewsNodeProperties(node, jsonObj);
				data.put(jsonObj);
			}
			session.save();
			log.info("After save getUnpublishedNewsCRXNodeData");
		} catch (Exception e) {
			log.info("Exception::: getUnpublishedNewsCRXNodeData" + e);
		}
		log.info("Exit getUnpublishedNewsCRXNodeData");
		return data;
	}
	
	private void getSponsorNodeProperties(Node sponsorNode, JSONObject jsonObj) {
		try {
			jsonObj.put("id", (sponsorNode.hasProperty("id") ? sponsorNode.getProperty("id").getString() : ""));
			jsonObj.put("DT_RowId", (sponsorNode.hasProperty("id") ? sponsorNode.getProperty("id").getString() : ""));
			jsonObj.put("sponsor", (sponsorNode.hasProperty("sponsor") ? sponsorNode.getProperty("sponsor").getString() : ""));
			jsonObj.put("heading1", (sponsorNode.hasProperty("heading1") ? sponsorNode.getProperty("heading1").getString() : ""));
			jsonObj.put("heading2", (sponsorNode.hasProperty("heading2") ? sponsorNode.getProperty("heading2").getString() : ""));
			jsonObj.put("contactOfficer", (sponsorNode.hasProperty("contactOfficer") ? sponsorNode.getProperty("contactOfficer").getString() : ""));
			jsonObj.put("contactPosition", (sponsorNode.hasProperty("contactPosition") ? sponsorNode.getProperty("contactPosition").getString() : ""));
			jsonObj.put("location", (sponsorNode.hasProperty("location") ? sponsorNode.getProperty("location").getString() : ""));
			jsonObj.put("phone", (sponsorNode.hasProperty("phone") ? sponsorNode.getProperty("phone").getString() : ""));
			jsonObj.put("fax", (sponsorNode.hasProperty("fax") ? sponsorNode.getProperty("fax").getString() : ""));
			jsonObj.put("email", (sponsorNode.hasProperty("email") ? sponsorNode.getProperty("email").getString() : ""));
			jsonObj.put("mobile", (sponsorNode.hasProperty("mobile") ? sponsorNode.getProperty("mobile").getString() : ""));
			jsonObj.put("secondaryContactOfficer", (sponsorNode.hasProperty("secondaryContactOfficer") ? sponsorNode.getProperty("secondaryContactOfficer").getString() : ""));
			jsonObj.put("secondaryContactPosition", (sponsorNode.hasProperty("secondaryContactPosition") ? sponsorNode.getProperty("secondaryContactPosition").getString() : ""));
			jsonObj.put("secondaryLocation", (sponsorNode.hasProperty("secondaryLocation") ? sponsorNode.getProperty("secondaryLocation").getString() : ""));
			jsonObj.put("secondaryPhone", (sponsorNode.hasProperty("secondaryPhone") ? sponsorNode.getProperty("secondaryPhone").getString() : ""));
			jsonObj.put("secondaryFax", (sponsorNode.hasProperty("secondaryFax") ? sponsorNode.getProperty("secondaryFax").getString() : ""));
			jsonObj.put("secondaryEmail", (sponsorNode.hasProperty("secondaryEmail") ? sponsorNode.getProperty("secondaryEmail").getString() : ""));
			jsonObj.put("secondaryMobile", (sponsorNode.hasProperty("secondaryMobile") ? sponsorNode.getProperty("secondaryMobile").getString() : ""));
		} catch (Exception e) {
			log.error("Exception::: getSponsorNodeProperties", e);
			e.printStackTrace();
		}
	}
	
	private void getSupplierNodeProperties(Node supplierNode, JSONObject jsonObj) {
		try {
			jsonObj.put("id", (supplierNode.hasProperty("id") ? supplierNode.getProperty("id").getString() : ""));
			jsonObj.put("DT_RowId", (supplierNode.hasProperty("id") ? supplierNode.getProperty("id").getString() : ""));
			jsonObj.put("supplier", (supplierNode.hasProperty("supplier") ? supplierNode.getProperty("supplier").getString() : ""));
			jsonObj.put("heading1", (supplierNode.hasProperty("heading1") ? supplierNode.getProperty("heading1").getString() : ""));
			jsonObj.put("text", (supplierNode.hasProperty("text") ? supplierNode.getProperty("text").getString() : ""));
		} catch (Exception e) {
			log.error("Exception::: getSupplierNodeProperties", e);
			e.printStackTrace();
		}
	}
	
	private void getNewsNodeProperties(Node newsNode, JSONObject jsonObj) {
		try {
			jsonObj.put("id", (newsNode.hasProperty("id") ? newsNode.getProperty("id").getString() : ""));
			jsonObj.put("DT_RowId", (newsNode.hasProperty("id") ? newsNode.getProperty("id").getString() : ""));
			jsonObj.put("newsHeading", (newsNode.hasProperty("newsHeading") ? newsNode.getProperty("newsHeading").getString() : ""));
			jsonObj.put("newsContent", (newsNode.hasProperty("newsContent") ? newsNode.getProperty("newsContent").getString() : ""));
			jsonObj.put("newsLink", (newsNode.hasProperty("newsLink") ? newsNode.getProperty("newsLink").getString() : ""));
			jsonObj.put("expiryDate", (newsNode.hasProperty("expiryDate") ? newsNode.getProperty("expiryDate").getString() : ""));
		} catch (Exception e) {
			log.error("Exception::: getNewsNodeProperties", e);
			e.printStackTrace();
		}
	}
}
