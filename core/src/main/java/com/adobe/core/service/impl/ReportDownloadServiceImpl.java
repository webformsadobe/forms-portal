package com.adobe.core.service.impl;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import com.adobe.core.service.FormsPortalConfigurationService;
import com.day.crx.JcrConstants;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.ReportDownloadService;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")
@Component(immediate = true,
		service = ReportDownloadService.class,
		name = "com.adobe.core.service.ReportDownloadService")
public class ReportDownloadServiceImpl implements ReportDownloadService {
	private static Logger log = LoggerFactory.getLogger(ReportDownloadServiceImpl.class);

	private static final String JCR_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
	private static final String DESIRED_DATE_FORMAT = "EEE, d MMM yyyy h:mm a";

	private final SimpleDateFormat jcrFormatter  = new SimpleDateFormat(JCR_DATE_FORMAT);
	private final SimpleDateFormat jsonFormatter = new SimpleDateFormat(DESIRED_DATE_FORMAT);

	@Reference
	private SlingRepository slingRepo;

	@Reference
	private FormsPortalConfigurationService osgiConfig;

	@Activate
	protected void activate(ComponentContext context) {
		// Null activation method
	}

	@Override
	public JSONArray getReportData() {
		JSONArray data = new JSONArray();
		if (osgiConfig.getOfflineReportEnabled()) {
			final String path = osgiConfig.getOfflineReportStoragePath();

			try {
				log.trace("Enter getReportData");
				Session session = slingRepo.loginService("offline-service", null);

				Workspace workspace = session.getWorkspace();
				QueryManager queryManager = workspace.getQueryManager();
				Query query = queryManager
						.createQuery("SELECT report.* FROM [nt:file] AS report WHERE ISDESCENDANTNODE(report, ["
								+ path + "])", Query.JCR_SQL2);
				QueryResult result = query.execute();
				NodeIterator nodeItr = result.getNodes();

				while (nodeItr.hasNext()) {
					final Node node = nodeItr.nextNode();
					final Node content = node.getNode(JcrConstants.JCR_CONTENT);

					// Get the size of the archive, measured in megabytes.
					long fileSize = content.getProperty(JcrConstants.JCR_DATA).getBinary().getSize();
					fileSize = (fileSize / 1024) / 1024;

					// Get the creation date
					final String jcrDateCreated = content.getProperty(JcrConstants.JCR_LASTMODIFIED).getString();
					final Date dateFileCreated = jcrFormatter.parse(jcrDateCreated);
					final String dateCreated = jsonFormatter.format(dateFileCreated);

					// The full file path, report name
					final String fullPath = path + node.getName();
					final String reportName = node.getName().replace("_", " ");

					JSONObject jsonObj = new JSONObject();
					jsonObj.put("name", reportName);
					jsonObj.put("id", node.getName());
					jsonObj.put("path", fullPath);
					jsonObj.put("fileSize", fileSize);
					jsonObj.put("dateCreated", dateCreated);
					jsonObj.put("DT_RowId", node.getName());
					data.put(jsonObj);

					log.debug("Node found : {}", node.getName());
				}
				session.logout();
			} catch (Exception e) {
				log.error("Exception: getReportData", e);
			}
		}
		log.trace("Exit getReportData");
		return data;
	}


	@Override
	public void deleteReport(String filename) {
		if (osgiConfig.getOfflineReportEnabled()) {
			String filePath = osgiConfig.getOfflineReportStoragePath() + filename;
			Node reportNode;
			try {
				Session localSession = slingRepo.loginService("offline-service", null);

				reportNode = localSession.getNode(filePath);
				reportNode.remove();

				localSession.save();
				localSession.logout();
			} catch (PathNotFoundException e) {
				log.info("PathNotFoundException::: deleteReport", e);
			} catch (RepositoryException e) {
				log.info("RepositoryException::: deleteReport", e);
			}
		}
	}
}
