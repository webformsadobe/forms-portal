package com.adobe.core.service.impl;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import com.adobe.core.models.SupplierData;
import com.adobe.core.models.SupplierFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.GetNodeDetailsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(immediate = true,
        service = GetNodeDetailsService.class,
        name = "com.adobe.core.service.GetNodeDetailsService")
public class GetNodeDetailsServiceImpl implements GetNodeDetailsService {
	// Creates the static constant mappings between node property names and JSON object property names.
    private static final Map<String, String> newsNodeProperties     = new HashMap<String, String>() {{
        put("id",          "id");
        put("DT_RowId",    "id");
        put("newsHeading", "newsHeading");
        put("newsContent", "newsContent");
        put("newsLink",    "newsLink");
        put("expiryDate",  "expiryDate");
    }};

    private static final Map<String, String> sponsorNodeProperties  = new HashMap<String, String>() {{
		put("id",       "id");
		put("DT_RowId", "id");
		put("sponsor",  "sponsor");
		put("heading1", "heading1");
		put("heading2", "heading2");
		put("phone",    "phone");
		put("fax",      "fax");
		put("email",    "email");
		put("mobile",   "mobile");

		put("contactOfficer",  	 "contactOfficer");
		put("contactPosition",   "contactPosition");
		put("location",          "location");
		put("secondaryLocation", "secondaryLocation");
		put("secondaryPhone",    "secondaryPhone");
		put("secondaryFax",      "secondaryFax");
		put("secondaryEmail",    "secondaryEmail");
		put("secondaryMobile",   "secondaryMobile");

		put("secondaryContactOfficer",  "secondaryContactOfficer");
		put("secondaryContactPosition", "secondaryContactPosition");
	}};

    // The log service to be used in this session.
    private static Logger log = LoggerFactory.getLogger(GetNodeDetailsServiceImpl.class);

    @Reference
    private SlingRepository slingRepo;

    @Activate
    protected void activate(ComponentContext context) {
    }


	/**
	 * Returns sponsor node data in JSON format.
	 *
	 * @param path The path to the root node containing the sponsor metadata.
	 * @return A JSONArray of zero or more sponsor nodes, stored in JSONObject format.
	 */
	@Override
    public JSONArray getSponsorCRXNodeData(String path) {
        log.trace("Entering getSponsorCRXNode");
        Session session = null;

        JSONArray data = new JSONArray();
        try {
            session = slingRepo.loginService("datadownload", null);

            Workspace workspace = session.getWorkspace();
            QueryManager queryManager = workspace.getQueryManager();
            Query query = queryManager
                    .createQuery("SELECT sponsor.* FROM [nt:unstructured] AS sponsor WHERE ISDESCENDANTNODE(sponsor, ["
                            + path + "])", Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeItr = result.getNodes();

            while (nodeItr.hasNext()) {
                Node node = nodeItr.nextNode();
                JSONObject jsonObj = new JSONObject();
                getSponsorNodeProperties(node, jsonObj);
                data.put(jsonObj);
            }

            log.trace("After save getSponsorCRXNodeData");
        } catch (Exception e) {
            log.warn("Exception::: getSponsorCRXNodeData" + e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
        }

        log.trace("Exit getSponsorCRXNodeData");
        return data;
    }


    /**
     * Returns supplier node data in JSON format.
     *
     * @param path The root node holding the supplier data.
     * @return A JSONArray containing zero or more instances of JSONObjects
     */
    @Override
    public JSONArray getSupplierCRXNodeData(String path) {
        log.trace("Enter getSupplierCRXNodeData");

        JSONArray data = new JSONArray();
        List<SupplierData> allSuppliers = SupplierFactory.createAllAsList(slingRepo, log);

        for(SupplierData supplier: allSuppliers) {
            data.put(supplier.JSON());
        }

        log.trace("Exit getSupplierCRXNodeData");
        return data;
    }


    /**
     * Returns the news articles in JSON format.
     *
     * @todo find out where the filtering by expiry date is occuring.
     * @param path The root node in which all of the news is stored.
     * @return A JSONArray object of zero or more JSONObject instances which hold the desired data.
     */
    @Override
    public JSONArray getNewsCRXNodeData(String path) {
        log.trace("Enter getNewsCRXNodeData");
        JSONArray data = new JSONArray();
        Session session = null;

        try {
            session = slingRepo.loginService("datadownload", null);

            Workspace workspace = session.getWorkspace();
            QueryManager queryManager = workspace.getQueryManager();
            Query query = queryManager
                    .createQuery("SELECT news.* FROM [nt:unstructured] AS news WHERE ISDESCENDANTNODE(news, ["
                            + path + "])", Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeItr = result.getNodes();

            while (nodeItr.hasNext()) {
                Node node = nodeItr.nextNode();
                JSONObject jsonObj = new JSONObject();
                getNewsNodeProperties(node, jsonObj);
                data.put(jsonObj);
            }

            log.trace("After save getNewsCRXNodeData");
        } catch (Exception e) {
            log.warn("Exception::: getNewsCRXNodeData" + e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
        }

        log.trace("Exit getNewsCRXNodeData");
        return data;
    }

    @Override
    public JSONArray getCategoriesData(String path) {
        log.trace("Entering getCategoriesData");
        JSONArray data = new JSONArray();
        Session session = null;

        try {
            session = slingRepo.loginService("datadownload", null);

            if (session.itemExists(path)) {
                Node categoriesNode = session.getNode(path);
                NodeIterator nodeItr = categoriesNode.getNodes();

                if (nodeItr.hasNext()) {
                    while (nodeItr.hasNext()) {
                        Node node = nodeItr.nextNode();

                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("name", node.getProperty("jcr:title").getString());
                        jsonObj.put("query", node.getPath().split("/tags/")[1]);
                        data.put(jsonObj);
                    }
                } else {
                    log.debug("Categories path found, but no news nodes are found within it.");
                }
            } else {
                log.debug("Categories path failed the isExists() test.");
            }

            log.trace("After save getCategoriesData");
        } catch (Exception e) {
            log.warn("Exception::: getCategoriesData" + e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
        }

        log.trace("Exit getCategoriesData");
        return data;
    }


	/**
	 * Constructs and returns a JSON object which holds the data for a sponsor node.
	 *
	 * @param sponsorNode The supplier node to read data from.
	 * @param jsonObj The JSON object holding the data.
	 */
    private void getSponsorNodeProperties(Node sponsorNode, JSONObject jsonObj) {
        log.trace("Entering getSponsorNodeProperties");
        getNodePropertiesAsJSON(sponsorNode, sponsorNodeProperties, jsonObj);
        log.trace("Exiting getSponsorNodeProperties");
    }

	/**
	 * Constructs and returns a JSON object which holds the data for a supplier node.
	 *
	 * @param supplierNode The supplier node to read data from.
	 * @param jsonObj The JSON object holding the data.
	 */
//	private void getSupplierNodeProperties(Node supplierNode, JSONObject jsonObj) {
//        log.trace("Entering getSupplierNodeProperties");
//        getNodePropertiesAsJSON(supplierNode, supplierNodeProperties, jsonObj);
//        log.trace("Exiting getSupplierNodeProperties");
//    }

    /**
     * Constructs and returns a JSON object containing the data for the given newsData node.
     *
     * @param newsNode The node for which to extract and return data.
     * @param jsonObj A JSON object to be sent to the front end.
     */
    private void getNewsNodeProperties(Node newsNode, JSONObject jsonObj) {
        log.trace("Entering getNewsNodeProperties");
		getNodePropertiesAsJSON(newsNode, newsNodeProperties, jsonObj);
        log.trace("Exiting getNewsNodeProperties");
    }

    /**
     * Implements a mapping of node properties to a JSON object.
     *
     * @param node The node from which to read the values of the properties.
     * @param propertyMap A mapping of json object property names onto node property names.
     * @param json The JSON object into which to place the property values.
     */
    private void getNodePropertiesAsJSON(Node node, Map<String, String> propertyMap, JSONObject json) {
        for (Map.Entry<String, String> entry : propertyMap.entrySet()) {
            String jsonPropertyName = entry.getKey();
            String nodePropertyName = entry.getValue();

            json.put(jsonPropertyName, getProperty(node, nodePropertyName));
        }
    }


    /**
     * Returns a node's property as a string, if the node has that property. If it doesn't, return the empty string.
     *
     * @param node The node from which to extract the property.
     * @param propertyName The name of the property to extract.
     * @return The value of the property, or the empty string "" if the property does not exist.
     */
    private String getProperty(Node node, String propertyName) {
        String res = "";
        try {
            if (node.hasProperty(propertyName)) {
                res = node.getProperty(propertyName).getString();
            } else {
                log.warn("Unable to locate property " + propertyName + " in node " + node.getIdentifier());
            }
        } catch (RepositoryException e) {
            log.warn("Unable to locate node " + node.toString() + ". Stack trace follows on debug level.");
            log.debug(e.toString());
        }
        return res;
    }
}
