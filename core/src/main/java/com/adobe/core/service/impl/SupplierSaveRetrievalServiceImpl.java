package com.adobe.core.service.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import com.adobe.core.models.SupplierFactory;
import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.SupplierSaveRetrievalService;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;

@Component(immediate = true,
		service = SupplierSaveRetrievalService.class,
		name = "com.adobe.core.service.SupplierSaveRetrievalService")
public class SupplierSaveRetrievalServiceImpl implements SupplierSaveRetrievalService {
	private static Logger log = LoggerFactory.getLogger(SupplierSaveRetrievalServiceImpl.class);

	private String dodAssetPath = "/content/dam/dodassets";
	private String dodAFPath = "/content/dam/formsanddocuments/dodassets";
	private String supplierJsonNodePath = "/content/dodData/data";
	private String dropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/dod/items/tabs/items/tab3/items/col1/items";
	private String formsDropdownPath = "/conf/global/settings/dam/adminui-extension/metadataschema/forms/items/tabs/items/tab1/items/col1/items";

	@Reference
	private SlingRepository slingRepo;
	
	@Reference
	private Replicator replicator;
	
	@Reference
    private ResourceResolverFactory resolverFactory;

	@Activate
	protected void activate(ComponentContext context) {
	}

	@Override
	public JSONObject saveDataToNode(String action, JSONObject inputJson, String id) {
		JSONObject respSupplierJsonObj = new JSONObject();
		respSupplierJsonObj.put("error", false);
		respSupplierJsonObj.put("errorMessage", "");

		try {
			log.info("Enter saveSupplierDataToNode");
			Session session = slingRepo.loginService("datawrite", null);
			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			
			// Create the path where supplier data will be saved
			final String nodePath = SupplierFactory.SUPPLIER_NODE_PATH;
			Node supplierNode;
			Node supplierJsonNode;
			
			if (!session.itemExists(nodePath)) {
				JcrUtil.createPath(nodePath, "sling:OrderedFolder", "sling:OrderedFolder", session, true);
			}

			if ("create".equals(action)) {
				String supplierId = inputJson.getString("id");

				// Add node data
				if (!session.itemExists(nodePath + "/" + supplierId)) {
					log.info("create node and update");
					supplierNode = JcrUtil.createPath(nodePath + "/" + supplierId, "nt:unstructured", "nt:unstructured",
							session, true);
					setNodeProperties(supplierNode, inputJson, supplierId);
					
					inputJson.put("DT_RowId", supplierId);
					inputJson.put("id", supplierId);
					JSONArray supplierJsonArr = new JSONArray();
					supplierJsonArr.put(inputJson);
					respSupplierJsonObj.put("data", supplierJsonArr);
					
					//Add Sponsor Id and Name to json file
					if (!session.itemExists(supplierJsonNodePath + "/supplierID.json")) {
						supplierJsonNode = session.getNode(supplierJsonNodePath).addNode("supplierID.json", "nt:file");
						Node jcrNode = supplierJsonNode.addNode("jcr:content", "nt:resource");
						jcrNode.setProperty("jcr:mimeType", "application/json");
						jcrNode.setProperty("jcr:encoding", "utf-8");
						jcrNode.setProperty("jcr:data", "");
						session.save();
					} else {
						supplierJsonNode = session.getNode(supplierJsonNodePath + "/supplierID.json");
					}
					
					if(supplierJsonNode.hasNode("jcr:content")) {
						Node jcrNode = supplierJsonNode.getNode("jcr:content");
						if(jcrNode.hasProperty("jcr:data")) {
							Binary rawData = jcrNode.getProperty("jcr:data").getBinary();
				            InputStream rawDataStream = rawData.getStream();
				            String jcrData = IOUtils.toString(rawDataStream, "utf-8");
				            
							if("".equals(jcrData)) {
								JSONObject jsonData = new JSONObject();
								JSONArray jsonKeyValues = new JSONArray();
								JSONObject jsonKeyValue = new JSONObject();
								jsonKeyValue.put("text", supplierId);
								jsonKeyValue.put("value", supplierId);
								jsonKeyValues.put(jsonKeyValue);
								jsonData.put("options", jsonKeyValues);
								jcrNode.setProperty("jcr:data", jsonData.toString());
							} else {
								JSONObject jsonData = new JSONObject(jcrData);
								if(jsonData.has("options")){
									JSONArray jsonKeyValues = jsonData.getJSONArray("options");
									JSONObject jsonKeyValue = new JSONObject();
									jsonKeyValue.put("text", supplierId);
									jsonKeyValue.put("value", supplierId);
									jsonKeyValues.put(jsonKeyValue);
									jsonData.put("options", jsonKeyValues);
									jcrNode.setProperty("jcr:data", jsonData.toString());
								}
							}
						}
					}
					
					//create dropdown value under conf folder (workaround for product issue with dropdown json file path)
					javax.jcr.query.Query query = queryManager.createQuery(
							"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ dropdownPath
									+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
							"JCR-SQL2");

					QueryResult result = query.execute();
					NodeIterator nodeIter = result.getNodes();
					while (nodeIter.hasNext()) {
						Node node = nodeIter.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + supplierId;
						Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured",
								"nt:unstructured", session, true);

						keyNode.setProperty("text", supplierId);
						keyNode.setProperty("value", supplierId);
						session.save();
					}
					
					javax.jcr.query.Query queryForms = queryManager.createQuery(
							"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
									+ formsDropdownPath
									+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
							"JCR-SQL2");

					QueryResult resultForms = queryForms.execute();
					NodeIterator nodeIterForms = resultForms.getNodes();
					while (nodeIterForms.hasNext()) {
						Node node = nodeIterForms.nextNode();
						String nodeName = node.getName();
						String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + supplierId;
						Node keyNode = JcrUtil.createPath(dropdownValuePath, "nt:unstructured",
								"nt:unstructured", session, true);

						keyNode.setProperty("text", supplierId);
						keyNode.setProperty("value", supplierId);
						session.save();
					}
				} else {
					respSupplierJsonObj.put("data", "Supplier Id already exits.");
				}
			} else if ("edit".equals(action)) {
				editNode(session, id, inputJson, respSupplierJsonObj);
			} else if ("remove".equals(action)) {
				boolean referencesExist = checkForReferences(id);
				if (referencesExist) {
					respSupplierJsonObj.put("error", true);
					respSupplierJsonObj.put("errorMessage", "Supplier has references within forms. Please remove the references before deleting.");

					respSupplierJsonObj.put("found", true);
				} else {
					boolean deleteSuccessful = deleteNode(session, id);
					if (!deleteSuccessful) {
						log.warn("SupplierSaveRetrievalService reports a failure to delete supplier ID " + inputJson.getString("id"));

						respSupplierJsonObj.put("error", true);
						respSupplierJsonObj.put("errorMessage", "An error occured while deleting this supplier. Please refresh the screen and try again.");
					}
				}
			}
			session.save();
			session.logout();

			log.trace("After save saveSupplierDataToNode");
		} catch (Exception e) {
			log.error("Exception::: saveSupplierDataToNode", e);
		}
		log.info("Exit saveSupplierDataToNode");
		return respSupplierJsonObj;
	}


	private void editNode(Session session, String oldSupplierId, JSONObject inputJson, JSONObject respSupplierJsonObj) {
		final String newSupplierId = inputJson.getString("id");
		final String nodePath      = SupplierFactory.SUPPLIER_NODE_PATH;

		try {
			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();

			// Edit node data
			Node supplierNode;
			if (newSupplierId.equals(oldSupplierId) || !session.itemExists(nodePath + "/" + newSupplierId)) {
				log.trace("Editing supplier node::" + oldSupplierId);
				supplierNode = session.getNode(nodePath + "/" + oldSupplierId);
				supplierNode.remove();
				session.save();

				supplierNode = JcrUtil.createPath(nodePath + "/" + newSupplierId, "nt:unstructured", "nt:unstructured",
						session, true);
				setNodeProperties(supplierNode, inputJson, newSupplierId);

				// update dropdown value under conf folder (workaround for product issue with dropdown json file path)
				Query query = queryManager.createQuery(
						"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
								+ dropdownPath
								+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
						"JCR-SQL2");

				QueryResult result = query.execute();
				NodeIterator nodeIter = result.getNodes();
				while (nodeIter.hasNext()) {
					Node node = nodeIter.nextNode();
					String nodeName = node.getName();
					String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + newSupplierId;
					Node keyNode = session.getNode(dropdownValuePath);

					keyNode.setProperty("text", newSupplierId);
					keyNode.setProperty("value", newSupplierId);
					session.save();
				}

				Query queryForms = queryManager.createQuery(
						"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
								+ formsDropdownPath
								+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
						"JCR-SQL2");

				QueryResult resultForms = queryForms.execute();
				NodeIterator nodeIterForms = resultForms.getNodes();
				while (nodeIterForms.hasNext()) {
					Node node = nodeIterForms.nextNode();
					String nodeName = node.getName();
					String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + newSupplierId;
					Node keyNode = session.getNode(dropdownValuePath);

					keyNode.setProperty("text", newSupplierId);
					keyNode.setProperty("value", newSupplierId);
					session.save();
				}

				inputJson.put("DT_RowId", newSupplierId);
				JSONArray supplierJsonArr = new JSONArray();
				supplierJsonArr.put(inputJson);
				respSupplierJsonObj.put("data", supplierJsonArr);
			} else {
				respSupplierJsonObj.put("error", true);
				respSupplierJsonObj.put("errorMessage", "Supplier Id already exits.");
			}
		} catch (Exception e) {
			log.error("Exception in SupplierSaveServiceImpl.DeleteNode():", e);

			respSupplierJsonObj.put("error", true);
			respSupplierJsonObj.put("errorMessage", "Unable to save changes. Please try again.");

			e.printStackTrace();
		}
	}

	/** Searches the forms assets folders for any forms connected to hte supplier we are trying to delete. **/
	public boolean checkForReferences(String supplier) {
		log.trace("Enter checkForReferences");

		boolean found = false;
		try {
			Map<String, Object> param = new HashMap<>();
			param.put("sling.service.subservice", "datadownload");
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
			Session localSession = resourceResolver.adaptTo(Session.class);

			if (localSession != null) {
				Map<String, String> map = new HashMap<>();
				map.put("type", "dam:Asset");
				map.put("1_group_path", dodAssetPath);
				// @todo this needs to be repaired.
//			map.put("1_group_path", dodAFPath);
				map.put("1_group.p.or", "true");

				map.put("1_property", "jcr:content/metadata/supplier");
				map.put("1_property.value", supplier);
				QueryBuilder builder = resourceResolver.adaptTo(QueryBuilder.class);

				com.day.cq.search.Query query = builder.createQuery(PredicateGroup.create(map), localSession);
				SearchResult sr = query.getResult();
				found = sr.getTotalMatches() > 0;

				localSession.logout();
			}
		} catch (Exception e) {
			log.error("Exception checkForReferences:", e);
			e.printStackTrace();
		}
		log.trace("Exit checkForReferences");
		return found;
	}

	/**
	 * Deletes this supplier node from the data store. Also removes it from the JSON lists and various dropdowns.
	 *
	 * @param id The Supplier ID to delete.
	 * @return True or false on success or failure.
	 */
	private boolean deleteNode(Session session, String id) {
		boolean success = false;

		try {
			Workspace workspace = session.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();

			// Remove Sponsor Id and Name from json file. Probably a better way to do this.
			Node supplierJsonNode = session.getNode(supplierJsonNodePath + "/supplierID.json");
			if (supplierJsonNode.hasNode("jcr:content")) {
				Node jcrNode = supplierJsonNode.getNode("jcr:content");
				if (jcrNode.hasProperty("jcr:data")) {
					Binary rawData = jcrNode.getProperty("jcr:data").getBinary();
					InputStream rawDataStream = rawData.getStream();
					String jcrData = IOUtils.toString(rawDataStream, "utf-8");
					JSONObject jsonData = new JSONObject(jcrData);

					if (jsonData.has("options")) {
						JSONArray jsonKeyValues = jsonData.getJSONArray("options");
						int index = -1;
						for (int i = 0; i < jsonKeyValues.length() && index == -1; i++) {
							JSONObject jsonObj = jsonKeyValues.getJSONObject(i);
							if (jsonObj.get("value").equals(id)) {
								index = i;
							}
						}
						if (index != -1) {
							jsonKeyValues.remove(index);
							jsonData.put("options", jsonKeyValues);
							jcrNode.setProperty("jcr:data", jsonData.toString());

							session.save();
						}
					}
				}
			}

			// Delete dropdown value under conf folder (workaround for product issue with dropdown json file path)
			javax.jcr.query.Query query = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ dropdownPath
							+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
					"JCR-SQL2");

			QueryResult result = query.execute();
			NodeIterator nodeIter = result.getNodes();
			while (nodeIter.hasNext()) {
				Node node = nodeIter.nextNode();
				String nodeName = node.getName();
				String dropdownValuePath = dropdownPath + "/" + nodeName + "/items/" + id;
				Node ddNode = session.getNode(dropdownValuePath);
				ddNode.remove();
				session.save();
			}

			Query queryForms = queryManager.createQuery(
					"SELECT * FROM [nt:unstructured] AS dropdownNode WHERE ISDESCENDANTNODE(dropdownNode, ["
							+ formsDropdownPath
							+ "])  AND dropdownNode.name =  './jcr:content/metadata/supplier'",
					"JCR-SQL2");

			QueryResult resultForms = queryForms.execute();
			NodeIterator nodeIterForms = resultForms.getNodes();
			while (nodeIterForms.hasNext()) {
				Node node = nodeIterForms.nextNode();
				String nodeName = node.getName();
				String dropdownValuePath = formsDropdownPath + "/" + nodeName + "/items/" + id;
				Node ddNode = session.getNode(dropdownValuePath);
				ddNode.remove();
				session.save();
			}

			final String nodePath = SupplierFactory.SUPPLIER_NODE_PATH + "/" + id;
			//replicate deletion to publisher
			replicator.replicate(session, ReplicationActionType.DEACTIVATE, nodePath);
			replicator.replicate(session, ReplicationActionType.DELETE, nodePath);

			//delete node from author
			Node supplierNode = session.getNode(nodePath);
			supplierNode.remove();

			session.save();
			success = true;
		} catch (Exception e) {
			log.error("Exception in SupplierSaveServiceImpl.DeleteNode():", e);
			e.printStackTrace();
		}

		return success;
	}

	
	private void setNodeProperties(Node supplierNode, JSONObject inputJson, String supplierId) {
		try {
			supplierNode.setProperty("supplier", inputJson.getString("supplier"));
			supplierNode.setProperty("heading1", inputJson.getString("heading1"));
			supplierNode.setProperty("text", inputJson.getString("content"));
			supplierNode.setProperty("id", supplierId);
			supplierNode.setProperty("DT_RowId", supplierId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
