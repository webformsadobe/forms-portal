package com.adobe.core.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.json.JSONObject;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.AssetAccessService;
import com.day.commons.datasource.poolservice.DataSourcePool;

/**
 * This service populates asset meta data models
 */
@Component(immediate = true,
		service = AssetAccessService.class,
		name = "com.adobe.core.service.AssetAccessService")
public class AssetAccessServiceImpl implements AssetAccessService {

	private static final Logger log = LoggerFactory.getLogger(AssetAccessServiceImpl.class);
	private SimpleDateFormat fullDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	@Reference
	private DataSourcePool source;

	/**
	 */
	@Activate
	protected void activate() {
	}

	@Override
	public JSONObject getAssetReportData(String startDate, String endDate) {
		log.info("Enter getAssetReportData.");
		JSONObject data = new JSONObject();
		long startDateMillis = 0;
		long endDateMillis = 0;
		try {
			//Date pre-processing before querying
			if(StringUtils.isEmpty(startDate) && StringUtils.isEmpty(endDate)){
				Date eDate = new Date(System.currentTimeMillis());
				Date sDate = new Date(System.currentTimeMillis() - 30 * 24 * 3600 * 1000l);
				startDateMillis = dateFormat.parse(dateFormat.format(sDate)).getTime();
				endDateMillis = fullDateFormat.parse(dateFormat.format(eDate) + " 23:59:59").getTime();
				
			} else {
				Date sDate = dateFormat.parse(startDate);
				startDateMillis = sDate.getTime();

				Date eDate = fullDateFormat.parse(endDate + " 23:59:59");
				endDateMillis = eDate.getTime();
			}
			data = getAssetReport(startDateMillis, endDateMillis);
		} catch (Exception e) {
			log.error("Exception getAssetReportData:", e);
			e.printStackTrace();
		}
		log.info("Exit getAssetReportData.");
		return data;
	}

	private JSONObject getAssetReport(long startDateMillis, long endDateMillis) {
		log.info("Enter getAssetReport");
		JSONObject jsonObject = new JSONObject();
		Connection connection = null;
		Map<String, List<Record>> result = new HashMap<String, List<Record>>();
		try {
			DataSource dataSource = (DataSource) source.getDataSource("dodDatasource");
			connection = dataSource.getConnection();
			
			try (Stream<Record> stream = DSL.using(connection)
					.resultQuery(
							"SELECT timestmp, formatted_message, thread_name, arg3 FROM logging_event WHERE timestmp > {0} AND timestmp < {1} AND arg3 IS NOT NULL",
							DSL.cast(startDateMillis, Long.class), DSL.cast(endDateMillis, Long.class)) 
					.fetchStream()) {
				
				result = stream.collect(Collectors.groupingBy(
						record -> {
							Long timeStamp = Long.valueOf(record.getValue("timestmp").toString());
							Date date = new Date(timeStamp);
							String actualDate = dateFormat.format(date);
							return actualDate;
						}
				));
				
				for (String key : result.keySet()) {
					jsonObject.put(key, result.get(key).size());
				}
			}

		} catch (Exception e) {
			log.error("Exception getAssetReport:", e);
			e.printStackTrace();
		} finally {
			try {
				/**
				 * Close the connection
				 */
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				log.error("SQLException occurred in getAssetReport" + e.getMessage());
			}
		}
		log.info("Exit getAssetReport");
		return jsonObject;
	}

}
