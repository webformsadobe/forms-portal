package com.adobe.core.service.impl;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.jcr.*;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.adobe.core.constants.DepartmentOfDefenceConstants;
import com.adobe.core.models.SupplierData;
import com.adobe.core.models.SupplierFactory;
import com.adobe.core.models.WfsFactory;
import com.adobe.core.service.FormsPortalConfigurationService;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.crx.JcrConstants;
import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.models.Wfs;
import com.adobe.core.service.OfflineCDGenerationService;

import com.day.cq.dam.api.Asset;

@SuppressWarnings("unused")
@Component(immediate = true,
		service = OfflineCDGenerationService.class,
		name = "com.adobe.core.service.OfflineCDGenerationService")
public class OfflineCDGenerationServiceImpl implements OfflineCDGenerationService {
	private static final Logger log = LoggerFactory.getLogger(OfflineCDGenerationServiceImpl.class);

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private ZipOutputStream zipOutputStream;

	@Reference
	private SlingRepository slingRepo;
	
	@Reference
    private ResourceResolverFactory resolverFactory;

	@Reference
	private Replicator replicator;

	@Reference
    private FormsPortalConfigurationService reportManager;

	@Reference
	private QueryBuilder queryBuilder;

    @Activate
    protected void activate(ComponentContext context) {
    	// Nothing need occur here.
    }


	/**
	 * Executes the scheduled automatic data extraction.
	 */
	public void executeScheduledExtraction() {
		log.info("Starting scheduled offline CD report generation.");

		boolean result = false;
		final String outputFilename = getArchiveFilename("Auto_Generated_Archive_");

		// If there is an old report lying around, grab a reference to it.
		try {
			Map<String, Object> param = new HashMap<>();
			param.put(ResourceResolverFactory.SUBSERVICE, "offline-service");
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
			Session reportSession = resourceResolver.adaptTo(Session.class);

			final Map<String, String> map = new HashMap<>();
			map.put("type", JcrConstants.NT_FILE);
			map.put("path", reportManager.getOfflineReportStoragePath());
			map.put("fulltext", "Auto_Generated_Archive");

			Query query = queryBuilder.createQuery(PredicateGroup.create(map), resourceResolver.adaptTo(Session.class));
			SearchResult oldFiles = query.getResult();
			List<String> oldResources = new LinkedList<>();
			for (final Hit hit : oldFiles.getHits()) {
				// Returns the path of the hit result
				String path = hit.getPath();
				oldResources.add(path);
				log.debug("Cached clean up request for old auto generated report {}", path);
			}

			result = executeDataExtraction(outputFilename);

			if (result && reportSession != null) {
				// Delete the old file/s.
				for (String pathName: oldResources) {
					Node node = reportSession.getNode(pathName);
					if (!node.getName().equals(outputFilename)) {
						node.remove();
					} else {
						log.debug("Skipping deletion of {} as it is the new report archive.", node.getPath());
					}
				}
				reportSession.save();
				reportSession.logout();
			}
		} catch (RepositoryException e) {
			log.error("RepositoryException:: ", e);
		} catch (LoginException e) {
			log.error("Login exception:: ", e);
		}

		log.info(result ? "Offline CD Report generated successfully." : "Offline CD Report failed to generate.");
	}


	/**
	 * User has requested a manual regeneration of the data archive.
	 *
	 * @return "Success" on success and "Failure" on failure.
	 */
	public String extractSaveMetadata() {
		log.info("Manually triggering offline CD report generation.)");

		final String outputFilename = getArchiveFilename("Forms_Report_");
		final String success = executeDataExtraction(outputFilename) ? "Success" : "Failure";

		log.info("Result of offline CD report generation: {}", success);
		return success;
	}


	/**
	 * Returns the name of the archive file to store this report inside of.
	 *
	 */
	private String getArchiveFilename(String archivePrefix) {
		final LocalDateTime now = LocalDateTime.now();
		final String formatDateTime = now.format(formatter);

		return archivePrefix + formatDateTime + ".zip";
	}


	/**
	 * Performs the metadata extraction and zip file creation.
	 *
	 * @param outputFilename The name of the archive file to generate.
	 * @return True if the operation was a success, false otherwise.
	 */
	private boolean executeDataExtraction(String outputFilename) {
		log.trace("Enter AssetMetadataExporterServiceImpl.executeDataExtraction() with filename {}", outputFilename);
		boolean success = false;

		try {
			Session session = slingRepo.loginService("offline-service", null);
			String folderPath = reportManager.getOfflineReportStoragePath();

			// Find the parent node ...
			if (folderPath != null && !folderPath.isEmpty() && session.nodeExists(folderPath)) {
				log.debug("Shooting for folder {}", folderPath);

				// Find the parent node ...
				Node reportNode = session.getNode(folderPath);

				// Create a new folder node ...
				Node zipNode;
				Node zipContentNode;

				// .. only if it doesnt already exist
				if (!reportNode.hasNode(outputFilename)) {
					log.debug("Creating output file at {}", outputFilename);
					zipNode = reportNode.addNode(outputFilename, JcrConstants.NT_FILE);
					zipContentNode = zipNode.addNode(JcrConstants.JCR_CONTENT, JcrConstants.NT_RESOURCE);
				} else {
					log.debug("File already exists: {}", outputFilename);
					zipNode = reportNode.getNode(outputFilename);
					zipContentNode = zipNode.getNode(JcrConstants.JCR_CONTENT);
				}

				final PipedInputStream pis = new PipedInputStream();
				final PipedOutputStream pos = new PipedOutputStream(pis);
				Executors.newSingleThreadExecutor().submit(() -> {
					try {
						zipOutputStream = new ZipOutputStream(pos);

						getAssetMetadata();

						zipOutputStream.flush();
						zipOutputStream.close();
					} catch (IOException e) {
						log.error("IOException:: ", e);
					}
				});
				Binary binary = session.getValueFactory().createBinary(pis);

				zipContentNode.setProperty(JcrConstants.JCR_DATA, binary);
				zipContentNode.setProperty(JcrConstants.JCR_LASTMODIFIED, Calendar.getInstance());

				// The auto-created properties are added when the session is saved ...
				session.save();
				session.logout();

				success = true;
			} else {
				log.error("Unable to generate ZIP file; either the output directory is not configured correctly or the service account does not have permission to read and write to it.");
			}

		} catch (AccessDeniedException e) {
			log.error("AccessDeniedException::", e);
		} catch (Exception e) {
			log.error("Exception::", e);
		} finally {
			try {
				if (zipOutputStream != null) {
					zipOutputStream.flush();
					zipOutputStream.close();
				}
			} catch (IOException e) {
				log.error("IOException::", e);
			}
		}

		log.trace("Exit AssetMetadataExporterServiceImpl.executeDataExtraction()");
    	return success;
	}


	private void getAssetMetadata() {
		try {
			log.trace("Enter getAssetMetadata");

			Map<String, Object> param = new HashMap<>();
			param.put(ResourceResolverFactory.SUBSERVICE, "datadownload");
			ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);

			Session readerSession = resourceResolver.adaptTo(Session.class);

			Workspace workspace = readerSession.getWorkspace();
			QueryManager queryManager = workspace.getQueryManager();
			javax.jcr.query.Query query = queryManager.createQuery(
					"SELECT asset.* FROM [dam:Asset] AS asset WHERE ISDESCENDANTNODE(asset, ["
							+ DepartmentOfDefenceConstants.PATHS.ASSET_PATH + "])",
					javax.jcr.query.Query.JCR_SQL2);
			QueryResult result = query.execute();
			NodeIterator nodeItr = result.getNodes();

			// Load the HTML template to use for the cover pages.
			StringBuilder html = loadHTMLTemplate(resourceResolver);

			// Load all the supplier metadata.
			Map<String, SupplierData> suppliers = SupplierFactory.createAllAsMap(slingRepo, log);

			while (nodeItr.hasNext()) {
				Node node = nodeItr.nextNode();
				Resource rs = resourceResolver.getResource(node.getPath());
				if (rs != null) {
					Asset asset = rs.adaptTo(Asset.class);
					if (asset != null && node.hasNode(DepartmentOfDefenceConstants.PATHS.ASSET_METADATA_NODE)) {
						log.debug("Processing form {}", node.getName());
						Node metadataNode = node.getNode(DepartmentOfDefenceConstants.PATHS.ASSET_METADATA_NODE);

						// Ensure only visible forms are included.
						boolean hidden = false;
						if (metadataNode.hasProperty(DepartmentOfDefenceConstants.HIDDEN)) {
							Property isHidden = metadataNode.getProperty(DepartmentOfDefenceConstants.HIDDEN);
							hidden = isHidden.getString().equals("Yes");
						}

						// Ensure only published ("activated") forms are included.
						ReplicationStatus replicationStatus = replicator.getReplicationStatus(readerSession, node.getPath());
						boolean published = replicationStatus.isActivated();

						if (hidden) {
							log.debug("Rejecting form {} as it is hidden.", node.getName());
						} else if (!published) {
							log.debug("Rejecting form {} as it is not in a published state.", node.getName());
						} else {
							Wfs wfs = WfsFactory.create(metadataNode);

							SupplierData supplier = null;
							if (!wfs.getSupplier().isEmpty()) {
								supplier = suppliers.get(wfs.getSupplier());
							}
							createZip(wfs, supplier, asset, html);
						}
					} else {
						log.warn("Rejected as this resource did not translate into an asset:: {}", node.getName());
					}
				} else {
					log.warn("Rejected as this node did not convert to a resource:: {}", node.getName());
				}
			}

			resourceResolver.close();
			log.trace("Exit save getAssetMetadata");
		} catch (Exception e) {
			log.error("Exception::: getAssetMetadata", e);
		}
	}


	/**
	 * Creates the entry within the zip file for the given asset.
	 *
	 * IF the asset is a physical PDF / ITP file, that file will be added to the ZIP file.
	 *
	 * If it is a 'cover page' form, it's metadata will be read and added in the form of an HTML file.
	 */
	private void createZip(Wfs wfs, SupplierData supplier, Asset asset, StringBuilder html) {
		JAXBContext jaxbContext;
		String fileName;
		String name = wfs.getName();

		try {
			fileName = asset.getName();
			if(fileName != null) {
				name = fileName.split("\\.")[0];

				if (wfs.getIsCoverpage() && (!wfs.getObsolete())) {
					Map<String, String> variables = getVariables(wfs, supplier);

					ZipEntry entry = new ZipEntry(name + ".htm");
					zipOutputStream.putNextEntry(entry);

					StringBuilder htmlTemplate = applyVariables(html, variables);
					zipOutputStream.write(htmlTemplate.toString().getBytes(), 0, htmlTemplate.length());
				} else {
					InputStream in = asset.getOriginal().getStream();
					ZipEntry entry = new ZipEntry(fileName);
					zipOutputStream.putNextEntry(entry);

					IOUtils.copy(in, zipOutputStream);
					in.close();
				}
			}

			// Create xml from java object and save it into zip
			jaxbContext = JAXBContext.newInstance(Wfs.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			String xmlFileName = name + ".xml";
			ZipEntry zipEntry = new ZipEntry(xmlFileName);

			zipOutputStream.putNextEntry(zipEntry);
			jaxbMarshaller.marshal(wfs, zipOutputStream);
		} catch (IOException e) {
			log.error("IOException::: createZip: ", e);
		} catch (JAXBException e) {
			log.error("JAXBException:: createZip: ", e);
		}
	}


	/**
	 * Generates and returns a hashmap of data name / value pairs for injection into the HTML template.
	 *
	 */
	private Map<String, String> getVariables(Wfs wfs, SupplierData supplier) {
		Map<String, String> variables = new HashMap<>();

		boolean ignoreSupplier = wfs.getFormat().equals("E");
		boolean ignoreContent = (wfs.getCoversheetString().isEmpty() && wfs.getCoversheetLinkName().isEmpty());
		boolean ignoreBoth = ignoreContent || ignoreSupplier;

		if (supplier != null) {
			variables.put("SUPPLIER_NAME", supplier.getHeading1());
			variables.put("SUPPLIER_CONTENT", supplier.getContent());
		}

		variables.put("FORM_NUMBER", wfs.getNumber());
		variables.put("FORM_NAME", wfs.getName());

		variables.put("CONTENT", wfs.getCoversheetString());
		variables.put("LINK_NAME", wfs.getCoversheetLinkName());
		variables.put("LINK_ADDRESS", wfs.getCoversheetLinkAddr());

		addIfVariable(variables, "CONTENT", !ignoreContent);
		addIfVariable(variables, "SUPPLIER", !ignoreSupplier);
		addIfVariable(variables, "BOTH", !ignoreBoth);

		return variables;
	}


	/**
	 * Loads the template for the XML cover pages used in this report.
	 *
	 */
	private StringBuilder loadHTMLTemplate(ResourceResolver resourceResolver) throws RepositoryException, IOException {
		StringBuilder result = null;
		Resource dataResource;
		Node rootNode = null;
		Node contentNode = null;

		dataResource = resourceResolver.getResource(DepartmentOfDefenceConstants.PATHS.OFFLINE_REPORT_XML_TEMPLATE);
		if (dataResource != null) {
			rootNode = dataResource.adaptTo(Node.class);
		}

		if (rootNode != null) {
			contentNode = rootNode.getNode(JcrConstants.JCR_CONTENT);
		}

		if (contentNode != null) {
			InputStream is = contentNode.getProperty(JcrConstants.JCR_DATA).getBinary().getStream();
			result = new StringBuilder();
			String line;

			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				result.append(line);
			}
		}

		return result;
	}


	/**
	 * Adds an IF-ENDIF variable pair to the collection. Includes the generation of true / false logics
	 * based on the value of the IF resolution
	 */
	private void addIfVariable(Map<String, String> variables, String variableName, boolean value) {
		final String ifKey = "IF_" + variableName;
		final String enifKey = "ENDIF_" + variableName;

		variables.put(ifKey, value ? "" : "<!--");
		variables.put(enifKey, value ? "" : "-->");
	}


	private StringBuilder applyVariables(StringBuilder masterTemplate, Map<String, String> variables) {
		StringBuilder htmlTemplate = new StringBuilder(masterTemplate);

		for (Map.Entry<String, String> entry : variables.entrySet()) {
			final String key = "{" + entry.getKey() + "}";
			final String value = entry.getValue();

			int startIndexName = htmlTemplate.indexOf(key);
			htmlTemplate.replace(startIndexName, startIndexName + key.length(), value);
		}

		return htmlTemplate;
	}
}
