package com.adobe.core.service;

import org.json.JSONArray;

public interface ReportDownloadService {
	// Returns a list of all offline CD archives found in the filesystem.
	// The location of these archives is set by the FP OSGI configuration.
	JSONArray getReportData();

	// Deletes the given report, if it exists.
	// Note that the filename contains the relative path to the file to delete
	// from the base offline storage path set in the OSGI configuration.
	// Files which don't exist will be silently ignored.
	void deleteReport(String filename);
}
