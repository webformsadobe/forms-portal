package com.adobe.core.service;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;

import com.adobe.core.models.AssetMetaData;

/**
 * Service that fetches the asset meta data
 */
public interface AssetMetaDataService {

	/**
	 * @return asset meta data object
	 * @param paramString
	 *            of the asset for which the metadata is to be fetched
	 * @param paramSlingHttpServletRequest
	 *            Sling Http servlet request
	 */
	AssetMetaData getAssetMetadata(String paramString,
			SlingHttpServletRequest paramSlingHttpServletRequest)
			throws RepositoryException;

	/**
	 * Regenerates the metadata associate with sponsors and suppliers; Ensures that the drop downs in the asset
	 * metadata screens contain the most up to date information.
	 */
	void syncSponsorAndSupplierDetails();
}
