package com.adobe.core.service;

import org.json.JSONObject;

/**
 * Service that fetches the asset meta data
 */
public interface AssetAccessService {
	JSONObject getAssetReportData(String startDate, String endDate);
}
