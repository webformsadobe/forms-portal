package com.adobe.core.service;

import org.json.JSONArray;

/**
 * GetNodeDetailsService
 *
 * This service provides a RESTful API into different types of data stored in the Webforms system.
 *
 * News nodes, categories, suppliers and sponsors may all be read through this interface. The data is returned
 * in JSON format, as an array guaranteed to contain zero or more JSON Objects.
 *
 */
public interface GetNodeDetailsService {
	JSONArray getSponsorCRXNodeData(String path);
	JSONArray getSupplierCRXNodeData(String path);
	JSONArray getNewsCRXNodeData(String path);
	JSONArray getCategoriesData(String path);
}
