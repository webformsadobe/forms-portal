package com.adobe.core.service;

import org.json.JSONObject;

public interface SupplierSaveRetrievalService {
	JSONObject saveDataToNode(String action, JSONObject data, String id);
	boolean checkForReferences(String paramString);
}
