/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.adobe.core.service;

import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.osgi.service.component.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A simple demo for cron-job like tasks that get executed regularly.
 * It also demonstrates how property values can be set. Users can
 * set the property values in /system/console/configMgr
 */
@SuppressWarnings("unused")
@Component(
        service = AutomatedArchivalService.class,
        immediate = true,
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        name = "com.adobe.core.service.AutomatedArchivalService"
)
public class AutomatedArchivalService {
    @Reference
    private FormsPortalConfigurationService reportManager;

    // The scheduler for rescheduling jobs.
    @Reference
    private Scheduler scheduler;

    @Reference
    private OfflineCDGenerationService archiver;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Runnable job = () -> archiver.executeScheduledExtraction();

    @Activate
    @Modified
    public void activate() {
        final String cronExpression = reportManager.getOfflineReportGenerationFrequency();
        logger.debug("Activating AutomatedExtractionTask; cron expression is {}", cronExpression);

        ScheduleOptions options = scheduler.EXPR(cronExpression);
        options.canRunConcurrently(false);
        options.name("Automated-Forms-Archiver");

        scheduler.schedule(job, options);
    }
}
