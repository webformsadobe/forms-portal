package com.adobe.core.service.impl;

import javax.sql.DataSource;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.LoggerUtilsService;
import com.day.commons.datasource.poolservice.DataSourcePool;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.db.DBAppender;
import ch.qos.logback.core.db.DataSourceConnectionSource;


@Component(
		service = LoggerUtilsService.class,
		property = {
				Constants.SERVICE_DESCRIPTION + "=Util class to log into db",
		},
		name = "com.adobe.core.service.LoggerUtilsService")
public class LoggerUtilsServiceImpl implements LoggerUtilsService{
	private org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private DataSourcePool source;
	
	@Activate
	protected void activate(ComponentContext context) {
	}
	

	public Logger createLogger(String loggerName, String dataSourceName) {
		Logger logger = (Logger) LoggerFactory.getLogger(loggerName);

		try {
			DataSourceConnectionSource connectionSource = new DataSourceConnectionSource();
			DataSource dataSource = (DataSource) source.getDataSource(dataSourceName);

			connectionSource.setDataSource(dataSource);
			connectionSource.setContext(logger.getLoggerContext());
			connectionSource.start();

			DBAppender dbAppender = new DBAppender();
			dbAppender.setName("dodDBAppender");
			dbAppender.setConnectionSource(connectionSource);
			dbAppender.setContext(logger.getLoggerContext());

			AsyncAppender asyncAppender = new AsyncAppender();
			asyncAppender.addAppender(dbAppender);
			asyncAppender.setName("asyncDodDBAppender");
			asyncAppender.setContext(logger.getLoggerContext());
			dbAppender.start();
			asyncAppender.start();

			logger.addAppender(asyncAppender);
		} catch (DataSourceNotFoundException e) {
			log.warn("Unable to locate data source " + dataSourceName + ". Logging failed to execute.");
		} catch (Exception e) {
			log.error("LoggerUtil Exception: ", e);
			log.error("LoggerUtil Exception: " + e.getMessage());
			e.printStackTrace();
		}
		return logger;
	}
}
