package com.adobe.core.service;

public interface OfflineCDGenerationService {
	/**
	 * Executes the manual (user-controlled) extraction.
	 * @return TBA whether there is any need to return anything at all here.
	 */
	String extractSaveMetadata();

	/**
	 * Executes the scheduled automatic report generation procedure.
	 */
	void executeScheduledExtraction();
}
