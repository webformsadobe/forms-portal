package com.adobe.core.service.impl;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.core.service.NewsSaveRetrievalService;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;

@Component(immediate = true,
		service = NewsSaveRetrievalService.class,
		name = "com.adobe.core.service.NewsSaveRetrievalService")
public class NewsSaveRetrievalServiceImpl implements NewsSaveRetrievalService {
	private static Logger log = LoggerFactory.getLogger(NewsSaveRetrievalServiceImpl.class);

	private String nodePath = "/content/dodData/data/newsData";
	private Session session;

	@Reference
	private SlingRepository slingRepo;

	@Reference
	private Replicator replicator;
	
	@Activate
	protected void activate(ComponentContext context) {
	}

	@Override
	public JSONObject saveDataToNode(String action, JSONObject inputJson, String id) {
		JSONObject respNewsJsonObj = new JSONObject();
		try {
			log.info("Enter saveNewsDataToNode");
			session = slingRepo.loginService("datawrite", null);
			// Create the path where news data will be saved
			Node newsNode;
			if (!session.itemExists(nodePath)) {
				JcrUtil.createPath(nodePath, "sling:OrderedFolder", "sling:OrderedFolder", session, true);
			}
			if ("create".equals(action)) {
				String newsId = inputJson.getString("id");
				// Add node data
				if (!session.itemExists(nodePath + "/" + newsId)) {
					log.info("create node and update");
					newsNode = JcrUtil.createPath(nodePath + "/" + newsId, "nt:unstructured", "nt:unstructured",
							session, true);
					setNodeProperties(newsNode, inputJson, newsId);

					inputJson.put("DT_RowId", newsId);
					inputJson.put("id", newsId);
					JSONArray newsJsonArr = new JSONArray();
					newsJsonArr.put(inputJson);
					respNewsJsonObj.put("data", newsJsonArr);
				} else {
					respNewsJsonObj.put("data", "News Id already exits.");
				}
			} else if ("edit".equals(action)) {
				String newsId = inputJson.getString("id");
				// Edit node data
				if (newsId.equals(id) || !session.itemExists(nodePath + "/" + newsId)) {
					newsNode = session.getNode(nodePath + "/" + id);
					newsNode.remove();
					session.save();
					newsNode = JcrUtil.createPath(nodePath + "/" + newsId, "nt:unstructured", "nt:unstructured",
							session, true);
					setNodeProperties(newsNode, inputJson, newsId);

					inputJson.put("DT_RowId", newsId);
					JSONArray newsJsonArr = new JSONArray();
					newsJsonArr.put(inputJson);
					respNewsJsonObj.put("data", newsJsonArr);
				} else {
					respNewsJsonObj.put("data", "News Id already exits.");
				}
			} else if ("remove".equals(action)) {
				replicator.replicate(session, ReplicationActionType.DEACTIVATE, nodePath + "/" + id);
		        replicator.replicate(session, ReplicationActionType.DELETE, nodePath + "/" + id);
				newsNode = session.getNode(nodePath + "/" + id);
				newsNode.remove();
			}
			session.save();
			log.info("After save saveNewsDataToNode");
		} catch (Exception e) {
			log.info("Exception::: saveNewsDataToNode" + e);
		}
		log.info("Exit saveNewsDataToNode");
		return respNewsJsonObj;
	}

	private void setNodeProperties(Node newsNode, JSONObject inputJson, String newsId) {
		try {
			  newsNode.setProperty("newsHeading", inputJson.getString("newsHeading"));
		      newsNode.setProperty("newsContent", inputJson.getString("newsContent"));
		      newsNode.setProperty("newsLink", inputJson.getString("newsLink"));
		      newsNode.setProperty("expiryDate", inputJson.getString("expiryDate"));
		      newsNode.setProperty("id", newsId);
		      newsNode.setProperty("DT_RowId", newsId);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
