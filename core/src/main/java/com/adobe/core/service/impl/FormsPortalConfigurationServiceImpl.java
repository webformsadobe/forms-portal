package com.adobe.core.service.impl;

import com.adobe.core.configurations.FormsPortalConfiguration;
import com.adobe.core.service.FormsPortalConfigurationService;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;


@SuppressWarnings("unused")
@Component(immediate = true,
        service = FormsPortalConfigurationService.class,
        name = "com.adobe.core.service.FormsPortalConfigurationService")
@Designate(ocd = FormsPortalConfiguration.class)
public class FormsPortalConfigurationServiceImpl implements FormsPortalConfigurationService {
    private FormsPortalConfiguration config;

    @Activate
    protected void activate(FormsPortalConfiguration config) {
        this.config = config;
    }

    @Override
    public boolean getOfflineReportEnabled() { return config.allowOfflineReports(); }

    @Override
    public String getOfflineReportStoragePath() {
        String path = config.offlineReportStoragePath();
        if (!path.endsWith("/")) {
            path += "/";
        }

        return path;
    }

    @Override
    public String getOfflineReportGenerationFrequency() { return config.offlineReportGenerationFrequency(); }

    @Override
    public boolean getManualOfflineReportEnabled() { return config.allowManualOfflineReport(); }

    @Override
    public boolean getUpdateMetadataEnabled() { return config.updateMetadataEnabled(); }

    @Override
    public boolean getCollectionDisabled() {
        return config.dataCollectionEnabled();
    }

    @Override
    public String getDatasourceName() {
        return config.dataSourceName();
    }

    @Override
    public String getRedirectionRoot() { return config.rootRedirectURI(); }

    @Override
    public boolean getForceModernBrowser() { return config.forceLatestBrowser(); }
}
