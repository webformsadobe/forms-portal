package com.adobe.core.service;

public interface ReportConfigurationService {
    String[] getPropertyNames();
}
