package com.adobe.core.service;

/**
 * Service that fetches the sponsor metadata, in CSV format.
 */
public interface SponsorReportService {
    StringBuffer getSponsorReportData();
}
