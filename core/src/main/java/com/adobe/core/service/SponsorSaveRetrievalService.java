package com.adobe.core.service;

import org.json.JSONObject;

public interface SponsorSaveRetrievalService {
	JSONObject getCRXNodeData();
	JSONObject saveDataToNode(String action, JSONObject data, String id);
	Boolean checkForReferences(String paramString);
}
