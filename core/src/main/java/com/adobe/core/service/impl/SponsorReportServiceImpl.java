package com.adobe.core.service.impl;

import com.adobe.core.models.SponsorData;
import com.adobe.core.models.SponsorFactory;
import com.adobe.core.service.SponsorReportService;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


/**
 * This service populates asset meta data models
 */
@Component(immediate = true,
		service = SponsorReportService.class,
		name = "com.adobe.core.service.SponsorReportService")
public class SponsorReportServiceImpl implements SponsorReportService {
	private static final Logger log = LoggerFactory.getLogger(SponsorReportServiceImpl.class);

	private static final String NEW_LINE_SEPARATOR = "\n";

	@Reference
	private SlingRepository slingRepo;


	/**
	 *
	 */
	@Activate
	protected void activate() {
	}

	@Override
	public StringBuffer getSponsorReportData() {
		log.info("Enter getSponsorReportData.");
		StringBuffer data = null;
		try {
			data = getSponsorReport();
		} catch (Exception e) {
			log.error("Exception getSponsorReportData:", e);
			e.printStackTrace();
		}
		log.info("Exit getSponsorReportData.");
		return data;
	}

	private StringBuffer getSponsorReport() {
		log.trace("Enter getSponsorReport");

		StringBuffer result = new StringBuffer();
		List<SponsorData> allSponsors = SponsorFactory.createAllAsList(slingRepo, log);

		final List<String> headers = Arrays.asList(
				"Sponsor ID", "Sponsor", "Heading 1", "Heading 2",
				"Contact Officer", "Contact Position", "Location", "Phone", "Fax", "Email", "Mobile",
				"2nd Contact Officer", "2nd Contact Position", "2nd Location", "2nd Phone", "2nd Fax", "2nd Email", "2nd Mobile");
		write(result, headers);

		for (SponsorData sponsor: allSponsors) {
			List<String> records = new ArrayList<>(headers.size());
			records.add("\"" + sponsor.getSponsor() + "\"");
			records.add("\"" + sponsor.getSponsorId() + "\"");
			records.add("\"" + sponsor.getHeading1() + "\"");
			records.add("\"" + sponsor.getHeading2() + "\"");
			records.add("\"" + sponsor.getContactOfficer() + "\"");
			records.add("\"" + sponsor.getContactPosition() + "\"");
			records.add("\"" + sponsor.getLocation() + "\"");
			records.add("\"" + sponsor.getPhone() + "\"");
			records.add("\"" + sponsor.getFax() + "\"");
			records.add("\"" + sponsor.getEmail() + "\"");
			records.add("\"" + sponsor.getMobile() + "\"");
			records.add("\"" + sponsor.getSecondaryContactOfficer() + "\"");
			records.add("\"" + sponsor.getSecondaryContactPosition() + "\"");
			records.add("\"" + sponsor.getSecondaryLocation() + "\"");
			records.add("\"" + sponsor.getSecondaryPhone() + "\"");
			records.add("\"" + sponsor.getSecondaryFax() + "\"");
			records.add("\"" + sponsor.getSecondaryEmail() + "\"");
			records.add("\"" + sponsor.getSecondaryMobile() + "\"");

			write(result, records);
		}

		log.trace("Exit getSponsorReport");
		return result;
	}

	/**
	 * Adds a collection of strings as a CSV-formatted row to the stringBuffer provided.
	 *
	 * @param buffer The output buffer to write into
	 * @param items  The collection of strings to add as a row.
	 */
	public void write(StringBuffer buffer, Iterable<String> items) {
		buffer.append(String.join(",", items));
		buffer.append(NEW_LINE_SEPARATOR);
	}
}
