package com.adobe.core.service;

/**
 * FormsPortalConfigurationService
 *
 * This container class provides an intertace to the OSGI configuration settings for the 'offline CD report'
 * functionality. Services and tasks which require access to the configuration information may reach it through
 * this class's accessors, rather than leaning in to the OSgi designate class directly.
 */
public interface FormsPortalConfigurationService {
    // Returns whether or not the offline report generation system is enabled for this instance.
    boolean getOfflineReportEnabled();

    // Returns the path of the offline report repository, as a string. The return value may be empty but it is never null.
    String getOfflineReportStoragePath();

    // Returns the cron-interval string for the automated task.
    String getOfflineReportGenerationFrequency();

    // Returns a flag indicating whether manual report generation is enabled.
    boolean getManualOfflineReportEnabled();

    // Returns true if the update asset metadata servlet is to be enabled. It is recommended to keep this setting off
    // in the production environment.
    boolean getUpdateMetadataEnabled();

    // Returns the name of the JDBC data source to connect to.
    String getDatasourceName();

    // Returns true if data collection has been disabled.
    boolean getCollectionDisabled();

    // Returns the root URI to use on the redirection filter.
    String getRedirectionRoot();

    // Returns whether or not to ignore IE's compatibility mode
    boolean getForceModernBrowser();
}
