package com.adobe.core.configurations;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(
        name = "AEM DoD - Forms Portal - Metadata Report Fields",
        description = "Configure the metadata fields which will be exported by the .csv report generator.")
public @interface ReportingConfiguration {
    @AttributeDefinition(name = "Metadata Columns",
            description = "Enter the metadata property names to include in the report. Invalid entries will silently fail.")
    String[] getPropertyNames();
}
