package com.adobe.core.configurations;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "AEM DoD - Forms Portal - Configuration", description = "Service Configuration")
public @interface FormsPortalConfiguration {
    @AttributeDefinition(name = "Enable Offline CD Generation",
            description="Check this box to enable the production of the offline content archives. " +
                        "It is recommended that this be checked only on the Author instance.")
    boolean allowOfflineReports() default false;

    @AttributeDefinition(name = "Offline Reports Path",
            description = "Path into which to save the offline CD archives.")
    String offlineReportStoragePath();

    @AttributeDefinition(name = "Offline Reports Frequency",
            description="A cron-expression to schedule the automatic Offline CD generation process. " +
                        "The default is set to trigger at 9:00PM every weeknight.")
    String offlineReportGenerationFrequency() default "0 0 21 ? * MON-FRI";

    @AttributeDefinition(name = "Allow Manually Triggered Offline CD Generation",
    description="Check this box to allow manual data export requests. If unchecked, only the scheduled automatic extractions will be run.")
    boolean allowManualOfflineReport() default false;

    @AttributeDefinition(name = "Enable the Update Metadata Service",
            description="Check this box to enable this service. Take care to disable the service when you are done using it.")
    boolean updateMetadataEnabled() default false;

    @AttributeDefinition(name = "Enable Usage Data Collection",
            description="Check this box to enble the data collection service.")
    boolean dataCollectionEnabled() default false;

    @AttributeDefinition(name = "JDBC Data Source Name",
            description = "This needs to match the data source name provided in the JDBC database connection configuration.")
    String dataSourceName() default "";

    @AttributeDefinition(name="Webforms-Redirect Base URL Mapping",
            description="Set the root URI which the webforms-redirect filter will redirect external queries to. " +
                        "The default value is set to the correct setting for PROD-PUB.")
    String rootRedirectURI() default "https://formsportal";

    @AttributeDefinition(name="Force Compatibility Mode Off",
            description="Adds an X-UA-Compatibility header which forces IE11 to act like IE11 even if the site is being viewed" +
                    " in compatibility mode.")
    boolean forceLatestBrowser() default false;
}
