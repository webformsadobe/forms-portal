package com.adobe.core.filters;

import com.adobe.core.service.FormsPortalConfigurationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.*;
import java.io.IOException;


/**
 * Implementation of a simple redirection filter.
 */
@SuppressWarnings("unused")
@Component(
        service = { Filter.class },
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        property = {
                "service.description=DoD WebForms Redirection Filter",
                "sling.filter.scope=REQUEST",
                "service.ranking:Integer=25500"
        })
public class RedirectFilter implements Filter {
    private static final String FORM_STUB = "/dod/form?";
    private static final String REDIR_STUB = "/content/dod/search.html";

    @Reference
    private FormsPortalConfigurationService manager;

    /** Empty initialisation method. This is a very simple filter. **/
    @Override
    public void init(FilterConfig filterConfig) {
    }

    /**
     * Issue redirections to resources based on the incoming URI.
     *
     * If the URI ends with /form, and it is not /dod/form, redirect to that location.
     * Otherwise, if the URI contains /webforms-redirect, redirect to the home page
     * Otherwise, do nothing and carry on down the filter chain.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{
        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
        final String reqURI = slingRequest.getRequestURI();

        if ((reqURI.endsWith("/form")) && (!reqURI.endsWith("/dod/form"))) {
            String queryString = slingRequest.getQueryString();
            slingResponse.sendRedirect(manager.getRedirectionRoot() + FORM_STUB + queryString);
        } else if (reqURI.contains("/webforms-redirect/")) {
            slingResponse.sendRedirect(manager.getRedirectionRoot() + REDIR_STUB );
        } else {
            chain.doFilter(request, slingResponse);
        }
    }

    /** Empty destruction method. This is a very simple filter. **/
    @Override
    public void destroy() {
    }
}
