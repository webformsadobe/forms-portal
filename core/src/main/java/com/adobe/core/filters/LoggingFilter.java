package com.adobe.core.filters;

import java.io.IOException;
import java.util.UUID;

import javax.jcr.Session;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;

import com.adobe.core.service.FormsPortalConfigurationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.LoggerUtilsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings("unused")
@Component(
		service = { Filter.class },
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				"service.description=User activity logging filter.",
				"sling.filter.scope=REQUEST",
				"service.ranking:Integer=26000"
		})
public class LoggingFilter implements Filter {
	private final static String COOKIE_NAME = "AEMLogSession";

	@Reference
	private LoggerUtilsService loggerUtilsService;

	@Reference
	private FormsPortalConfigurationService manager;

	// The database logger itself.
    private ch.qos.logback.classic.Logger dbLogger;

    // The error log.
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException {
		final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
		final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;

		final String reqURI = slingRequest.getRequestURI();

		// Log this activity.
		if (!manager.getCollectionDisabled()) {
			logger.trace("Inside of the data collection phase");
			// Get the session ID, if it exists.
			String sessionId = getSessionId(slingRequest, slingResponse);

			// Get the name of the currently logged in user.
			final ResourceResolver resourceResolver = slingRequest.getResourceResolver();
			final Session session = resourceResolver.adaptTo(Session.class);
			String username = "anonymous";		// If none then default to anon
			if (session != null) {
				username = session.getUserID();
			}

			String action = null;
			String arguments = "";

			String[] reqURISplit = reqURI.split("/");
			boolean isDownload = false;
			boolean isRedirect = false;

			if (reqURISplit.length > 0) {
				String page = reqURISplit[reqURISplit.length - 1];
				boolean isHTML = page.endsWith(".html") & !page.equals("aemform.iframe.html") & !page.equals("404.html");
				isDownload = reqURI.equals("/dod/form");
				isRedirect = reqURI.contains("/webforms-redirect/");
				if (isHTML) {
					switch (page) {
						case "search.html":
							action = "home";
							break;
						case "searchResult.html":
						case "searchResult-ie.html":
							action = request.getParameter("src");
							if (action == null) {
								action = "search";
							}
							arguments = request.getParameter("search");
							break;
						case "helpSupport.html":
							action = "help";
							break;
						case "contact.html":
							action = "contact";
							break;
						case "coverPage.html":
							action = "coverPage";
							break;
					}
				}
			}

			if (isDownload) {
				action = "download";
				arguments = slingRequest.getQueryString();
			}

			if (isRedirect) {
				action = "redirect";
			}

			if (action != null) {
				logger.trace("Attempting to save request to database");

				String referrer = slingRequest.getHeader("referer");
				if (referrer == null) {
				    referrer = "No referrer";
                }
				this.dbLogger.info(referrer, sessionId, username, action, arguments);
			}
		}

		logger.trace("Exiting the filter");
		filterChain.doFilter(slingRequest, slingResponse);
	}

    /**
     * Creates the database logger connection.
     */
	public void init(FilterConfig filterConfig) {
        this.dbLogger = loggerUtilsService.createLogger("LoggingFilter", manager.getDatasourceName());
	}

    /**
     * Releases the connection to the database.
     */
	public void destroy() {
        this.dbLogger.detachAndStopAllAppenders();
	}


	/**
	 * Returns the ID of the current session. If no session has yet been established, create one.
	 *
	 * @return A String holding the current session ID. This value is guaranteed never to be null or an empty string.
	 */
	private String getSessionId(SlingHttpServletRequest slingRequest, SlingHttpServletResponse slingResponse) {
		String sessionId = "";

		Cookie[] cookies = slingRequest.getCookies();
		if (cookies != null) {
			for (Cookie cookie: cookies) {
				if (cookie.getName().equals(COOKIE_NAME)) {
					sessionId = cookie.getValue();
				}
			}
		}

		// If it doesn't exist, make it exist, and add it to the response header.
		if (sessionId.isEmpty()) {
			sessionId = UUID.randomUUID().toString();

			Cookie newCookie = new Cookie(COOKIE_NAME, sessionId);
			newCookie.setMaxAge(-1);        // Store not thee cookie
			newCookie.setPath("/");			// Across the entirety of the site please.
			slingResponse.addCookie(newCookie);
		}

		return sessionId;
	}
}
