package com.adobe.core.filters;

import com.adobe.core.models.Browser;
import com.adobe.core.service.FormsPortalConfigurationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

import static com.adobe.aemds.guide.utils.JcrResourceConstants.CQ_PAGE;

/**
 * Modify the HTTP response headers. This filter controls the Dispatcher/No-Cache header, and sets the
 * browser compatibility headers for IE.
 *
 * Other modifications to the response headers should be included in this filter.
 */
@SuppressWarnings("unused")
@Component(
        service = { Filter.class },
        property = {
                "service.description=HTTP Response Header Filter",
                "sling.filter.scope=REQUEST",
                "service.ranking:Integer=25000"
        })
public class ResponseHeaderFilter implements Filter {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String COMPAT_HEADER_NAME = "X-UA-Compatible";
    private static final String COMPAT_OVERRIDE = "IE=edge";

    @Reference
    private FormsPortalConfigurationService config;

    /** Empty initialisation method. This is a very simple filter. **/
    @Override
    public void init(FilterConfig filterConfig) {
        // Do nothing.
    }

    /**
     * Applies these browser compatibility headers to the HTTP response.
     *
     * If the file being returned is not a JS or a CSS file, disable dispatcher caching.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException{
        final SlingHttpServletRequest slingRequest   = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;

        // Save the browser type for future use
        Browser userBrowser = new Browser(slingRequest);
        request.setAttribute("com.adobe.core.models.Browser", userBrowser);

        // Set the compatibility header.
        final String compatibility = config.getForceModernBrowser() ? COMPAT_OVERRIDE : userBrowser.compatibilityHeader();
        slingResponse.addHeader(COMPAT_HEADER_NAME, compatibility);

        // Only certain types of data are not to be cached.
        final String resourceType = slingRequest.getResource().getResourceType();
        if (resourceType.contains(CQ_PAGE) ||
                resourceType.contains("servlet")) {
            slingResponse.addHeader("Dispatcher", "no-cache");
            logger.debug("Adding no-cache header to resource type: {}", resourceType);
        }

        chain.doFilter(slingRequest, slingResponse);
    }

    /** Empty destruction method. This is a very simple filter. **/
    @Override
    public void destroy() {
        // Do nothing.
    }
}
