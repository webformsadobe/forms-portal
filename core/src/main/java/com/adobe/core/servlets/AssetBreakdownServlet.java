package com.adobe.core.servlets;

import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletOutputStream;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.AssetBreakdownService;


@SuppressWarnings("unused")
@Component(service = Servlet.class, property = { 
		ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/getAssetBreakdown",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"
})
public class AssetBreakdownServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;
    
    @Reference
    private transient AssetBreakdownService assetMetaDataService;
    
    @Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        final String assetType = request.getParameter("assetType");
        if (Boolean.parseBoolean(request.getParameter("report"))) {
            final ServletOutputStream out = response.getOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(out);

            final String fileName = "form_metadata_by_sponsor_" + todayForFilename() + ".csv";
            StringBuilder dataBuffer = assetMetaDataService.getAssetReportData(assetType);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

            writer.write(dataBuffer.toString());
            writer.close();
            out.flush();
        } else {
            JSONObject respJson = assetMetaDataService.getAssetRecordData(assetType);

            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            response.getWriter().write(respJson.toString());
        }
    }
}
