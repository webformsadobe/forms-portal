package com.adobe.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.dam.api.Asset;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;


@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_PATHS   + "=/dod/form",
        ServletResolverConstants.SLING_SERVLET_METHODS + "={'GET','POST'}"
})
public class DoDAssetDownloadServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;

    @Reference
    private ResourceResolverFactory resolverFactory;
    @Reference
    private QueryBuilder builder;


    /**
     * Executes the servlet request. Searches for a form asset with a form number that matches the parameter
     * passed to this servlet via the command line.
     *
     * Converting this servlets URI signature from /dod/form?FORMNUMBER to /dod/form/FORMNUMBER is a @todo
     * Adding support for searchable / non-searchable metadata is a @todo
     *
     * @param request the HTTP request as received
     * @param response The response to send back to the user's browser.
     * @throws Exception This method could throw many exceptions.
     */
    @Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws Exception {
        List<RequestParameter> parameters = request.getRequestParameterList();

        if (parameters.size() != 1) {
            logger.warn("Incorrect number of parameters passed to servlet. Expected 1, received " + parameters.size());
            try {
                response.sendError(404);
            } catch (IOException e) {
                logger.error("IOException when attempting to redirect to 404 page.");
                logger.debug(exceptionStacktraceToString(e));
            }
        } else {
            //! Obtain a valid resourceResolver and session object for the 'nodeReader' system service.
            Map<String, Object> param = new HashMap<>();
            param.put("sling.service.subservice", "datadownload");

            ResourceResolver resourceResolver = null;
            Session session = null;

            try {
                resourceResolver = resolverFactory.getServiceResourceResolver(param);
                session = resourceResolver.adaptTo(Session.class);

                if (session != null) {
                    final String formNumber = parameters.get(0).getName();
                    final Asset asset = findAsset(formNumber, resourceResolver, session);

                    if (asset != null) {
                        ServletOutputStream out = response.getOutputStream();
                        response.setContentType(asset.getMimeType());
                        response.setHeader("Content-Disposition", "attachment;filename=" + asset.getName());
                        InputStream in = asset.getOriginal().getStream();

                        IOUtils.copy(in, out);
                        in.close();
                        out.flush();
                        out.close();
                    } else {
                        logger.warn("Asset not found. Search parameter was " + formNumber);
                        response.sendRedirect("/content/dod/404.html");
                    }
                }
            } catch (Exception e) {
                logger.error("Exception in dodAssetDownloadServlet.processGet: " + e);
                logger.error("Stack trace available in debug.");
                logger.debug(exceptionStacktraceToString(e));
            } finally {
                if (session != null && session.isLive()) {
                    session.logout();
                }
                if (resourceResolver != null && resourceResolver.isLive()) {
                    resourceResolver.close();
                }
            }
        }
    }


    /**
     * Searches for any asset which matches the given form number.
     *
     * @param findFirst The form number to search for.
     * @return An Asset if a match is found, or null otherwise.
     */
    private Asset findAsset(String findFirst, ResourceResolver resolver, Session session) {
        logger.trace("findAsset: Searching for form number " + findFirst);

        //! Construct the search query.
        Map<String, String> map = new HashMap<>();

        map.put("type", "dam:Asset");
        map.put("path", "/content/dam/dodassets/");
        map.put("1_property", "jcr:content/metadata/formNumber");
        map.put("1_property.value", findFirst.toUpperCase());
        map.put("2_property", "jcr:content/metadata/coverpage");
        map.put("2_property.value", "false");

        //! Execute the search on the asset repository.
        Query query = builder.createQuery(PredicateGroup.create(map), session);
        SearchResult sr = query.getResult();

        Asset asset = null;
        final long numHits = sr.getTotalMatches();
        if (numHits == 1) {
            try {
                String path = sr.getHits().get(0).getPath();
                Resource rs = resolver.getResource(path);

                if (rs != null) {
                    asset = rs.adaptTo(Asset.class);
                }
            } catch (RepositoryException e) {
                logger.warn("Repository exception produced by DodAssetDownloadServlet.findAsset. " + e);
            }
        } else {
            logger.warn("Failed to find exactly one instance of " + findFirst + ". See debug for more info.");
            logger.debug("Number of matching assets found: " + numHits);
        }

        return asset;
    }
}
