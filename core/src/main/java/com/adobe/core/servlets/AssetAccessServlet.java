package com.adobe.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.AssetAccessService;


@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/getAssetAccess",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"
})
public class AssetAccessServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;

    @Reference
    private AssetAccessService assetAccessService;

    @Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");

        JSONObject respJson = assetAccessService.getAssetReportData(startDate, endDate);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(respJson.toString());
    }
}
