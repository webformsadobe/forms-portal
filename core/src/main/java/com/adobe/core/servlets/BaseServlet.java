package com.adobe.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * BaseServlet
 *
 * This class wraps around the base SlingAllMethodsServlet class and provides exception logging and trace monitoring
 * infrastructure.
 *
 * Servlets which derive from this class will automatically have access to the Logger service through the protected
 * member logger.
 *
 * To subclass this servlet, override processGet and / or processPost. The implementation of these methods provided
 * by this class default to 'method not implemented'.
 */
public abstract class BaseServlet extends SlingAllMethodsServlet {
    protected final transient Logger logger  = LoggerFactory.getLogger(this.getClass());

    protected final String myClass = this.getClass().getName();

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");


    // Child classes need to provide an implementation of this method. It contains the code to execute the task.
    protected void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws Exception {
        handleMethodNotImplemented(request, response);
    }


    protected void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
        throws Exception {
        handleMethodNotImplemented(request, response);
    }


    /**
     * This method wraps around the base SlingSafeMethodsServlets class to provide automatic exception reporting
     * and log handling infrastructure to GET- servlets.
     * @param request The incoming HTTP request
     * @param response The HTTP response to send out.
     */
    @Override
    public final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        logger.trace("Entered {}", myClass);
        try {
            processGet(request, response);
        } catch (Exception e) {
            logger.warn("Exception: {}. Stack trace available in debug.", e.getMessage());
            logger.debug(exceptionStacktraceToString(e));
        }

        logger.trace("Exited {}", myClass);
    }


    /**
     * This method wraps around the base SlingSafeMethodsServlets class to provide automatic exception reporting
     * and log handling infrastructure to GET- servlets.
     * @param request The incoming HTTP request
     * @param response The HTTP response to send out.
     */
    @Override
    public final void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        logger.trace("Entered {}", myClass);
        try {
            processPost(request, response);
        } catch (Exception e) {
            logger.warn("Exception: {}. Stack trace available in debug.", e.getMessage());
            logger.debug(exceptionStacktraceToString(e));
        }

        logger.trace("Exited {}", myClass);
    }


    /**
     * This handy function saves all parameters to the log files.
     *
     * @param request The HTTP request containing the parameters which are to be saved.
     */
    protected final void logParameters(SlingHttpServletRequest request) {
        List<RequestParameter> parameters = request.getRequestParameterList();

        logger.debug("Starting parameter dump:");
        for (RequestParameter parameter: parameters) {
            final String parameterString = parameter.getName() + ": " + parameter.getString();
            logger.debug(parameterString);
        }
        logger.debug("Parameter dump complete.");
    }

    /**
     * A handy utility function to get the stacktrace of an exception and return it as a string.
     * @param e The exception to process.
     * @return The exception's stacktrace, as a string.
     */
    protected static String exceptionStacktraceToString(Exception e)
    {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintStream printStream          = new PrintStream(byteStream);

        e.printStackTrace(printStream);
        printStream.close();

        return byteStream.toString();
    }


    /**
     * A utility function which returns today's date as a string in YYYYMMDD format.
     */
    protected final String todayForFilename() {
        return LocalDateTime.now().format(DATE_FORMAT);
    }
}
