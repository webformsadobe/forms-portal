package com.adobe.core.servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.NewsSaveRetrievalService;


/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type.
 *
 * Note that inbound parameters containing the data to save are passed in the format data[0][fieldname]
 * and the fieldname is the only bit we are interested in, so the prefix needs to be stripped away.
 */
@Component(
		immediate = true,
		service = Servlet.class,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=CRX save retrieval servlet",
				"sling.servlet.methods={'GET','POST'}",
				"sling.servlet.paths=" + "/dod/newsData"}
)
public class NewsSaveRetrievalServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final String REQ_PARM_PREFIX = "data[";

	@Reference
	NewsSaveRetrievalService nodeSaveRetrievalService;

	@Override
	protected void processGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
		// @todo
	}

	@Override
	protected void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		String action = request.getParameter("action");
		List<RequestParameter> requestParameters = request.getRequestParameterList();
		JSONObject inputObject = new JSONObject();
		String id = "";

		if(requestParameters.size() > 1){
			String repParmName = requestParameters.get(1).getName();
			String[] subStrings = StringUtils.substringsBetween(repParmName, "[", "]");
			if(subStrings.length > 0){
				id = subStrings[0];
			}
			logger.debug("ID:::" + id);
		}

		if(!"remove".equals(action)) {
			inputObject.put("id", request.getParameter(REQ_PARM_PREFIX + id + "][id]"));
			inputObject.put("newsHeading", request.getParameter(REQ_PARM_PREFIX + id + "][newsHeading]"));
			inputObject.put("newsContent", request.getParameter(REQ_PARM_PREFIX + id + "][newsContent]"));
			inputObject.put("newsLink", request.getParameter(REQ_PARM_PREFIX + id + "][newsLink]"));
			inputObject.put("expiryDate", request.getParameter(REQ_PARM_PREFIX + id + "][expiryDate]"));
		}

		JSONObject responseData = nodeSaveRetrievalService.saveDataToNode(action, inputObject, id);

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().write(responseData.toString());
	}
}
