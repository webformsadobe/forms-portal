/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.adobe.core.servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.SponsorSaveRetrievalService;


/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type.
 * idempotent.
 */
@Component(
		immediate = true,
		service = Servlet.class,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=CRX save retrieval servlet",
				"sling.servlet.methods={'GET','POST'}",
				"sling.servlet.paths=" + "/dod/sponsorData" }
)
public class SponsorSaveRetrievalServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final String REQ_PARM_PREFIX = "data[";

	@Reference
	SponsorSaveRetrievalService nodeSaveRetrievalService;

	@Override
	protected void processGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws IOException {
		JSONObject data = nodeSaveRetrievalService.getCRXNodeData();
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().write(data.toString());
	}


	@Override
	public void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		String action = request.getParameter("action");

		List<RequestParameter> requestParameters = request.getRequestParameterList();
		JSONObject inputObject = new JSONObject();
	    JSONObject responseData = new JSONObject();
		String id = "";
		if(requestParameters.size() > 1){
			String repParmName = requestParameters.get(1).getName();
			String[] subStrings = StringUtils.substringsBetween(repParmName, "[", "]");
			if(subStrings.length > 0){
				id = subStrings[0];
			}
			logger.debug("ID:::" + id);
		}
		
		inputObject.put("sponsor", request.getParameter(REQ_PARM_PREFIX + id + "][sponsor]"));
		inputObject.put("id", request.getParameter(REQ_PARM_PREFIX + id + "][id]"));
		inputObject.put("heading1", request.getParameter(REQ_PARM_PREFIX + id + "][heading1]"));
		inputObject.put("heading2", request.getParameter(REQ_PARM_PREFIX + id + "][heading2]"));
		inputObject.put("contactOfficer", request.getParameter(REQ_PARM_PREFIX + id + "][contactOfficer]"));
		inputObject.put("contactPosition", request.getParameter(REQ_PARM_PREFIX + id + "][contactPosition]"));
		inputObject.put("location", request.getParameter(REQ_PARM_PREFIX + id + "][location]"));
		inputObject.put("phone", request.getParameter(REQ_PARM_PREFIX + id + "][phone]"));
		inputObject.put("fax", request.getParameter(REQ_PARM_PREFIX + id + "][fax]"));
		inputObject.put("email", request.getParameter(REQ_PARM_PREFIX + id + "][email]"));
		inputObject.put("mobile", request.getParameter(REQ_PARM_PREFIX + id + "][mobile]"));
		inputObject.put("secondaryContactOfficer", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryContactOfficer]"));
		inputObject.put("secondaryContactPosition",	request.getParameter(REQ_PARM_PREFIX + id + "][secondaryContactPosition]"));
		inputObject.put("secondaryLocation", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryLocation]"));
		inputObject.put("secondaryPhone", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryPhone]"));
		inputObject.put("secondaryFax", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryFax]"));
		inputObject.put("secondaryEmail", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryEmail]"));
		inputObject.put("secondaryMobile", request.getParameter(REQ_PARM_PREFIX + id + "][secondaryMobile]"));
		
		if (!"remove".equals(action)) {
			responseData = nodeSaveRetrievalService.saveDataToNode(action, inputObject, id);
		} else {
			Boolean found = nodeSaveRetrievalService.checkForReferences(id);
			if (found) {
				responseData.put("found", found);
				responseData.put("id", id);
			} else {
				responseData = nodeSaveRetrievalService.saveDataToNode(action, inputObject, id);
			}
		}
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().write(responseData.toString());
	}
}
