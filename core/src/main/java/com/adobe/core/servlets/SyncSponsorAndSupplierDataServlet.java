package com.adobe.core.servlets;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.AssetMetaDataService;


/**
 * Servlet that gives the Sponsor data saved on crx.
 */
@Component(
    immediate = true,
    service = Servlet.class,
    name = "com.adobe.core.servlets.SyncSponsorAndSupplierDataServlet",
    property = {
        "sling.servlet.paths=/dod/syncDetails",
        "sling.servlet.methods={'GET','POST'}"
    })
public class SyncSponsorAndSupplierDataServlet extends BaseServlet {
    private static final long serialVersionUID = 1L;

    /**
     * AssetMetaDataService for populating asset meta data models
     */
    @Reference
    private transient AssetMetaDataService assetMetaDataService;

    @Override
    protected final void processGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        assetMetaDataService.syncSponsorAndSupplierDetails();
    }

    @Override
    protected final void processPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
		assetMetaDataService.syncSponsorAndSupplierDetails();
    }
}
