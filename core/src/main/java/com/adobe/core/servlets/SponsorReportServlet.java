package com.adobe.core.servlets;

import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletOutputStream;

import com.adobe.core.service.SponsorReportService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/getSponsorReport",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"
})
public class SponsorReportServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;

    @Reference
    SponsorReportService sponsorReportService;

    @Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        final ServletOutputStream out = response.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out);

        StringBuffer dataBuffer = sponsorReportService.getSponsorReportData();
        final String outputFilename = "sponsor_list_" + todayForFilename() + ".csv";

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename=" + outputFilename);

        writer.write(dataBuffer.toString());
        writer.close();
        out.flush();
    }
}
