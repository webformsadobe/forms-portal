package com.adobe.core.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.json.JSONObject;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.SupplierSaveRetrievalService;


/**
 * Servlet that writes some sample content into the response. It is mounted for
 * all resources of a specific Sling resource type.
 */
@Component(
		immediate = true,
		service = Servlet.class,
		configurationPolicy = ConfigurationPolicy.REQUIRE,
		property = {
				Constants.SERVICE_DESCRIPTION + "=CRX save retrieval servlet",
				"sling.servlet.methods={'GET','POST'}",
				"sling.servlet.paths=" + "/dod/supplierData" }
)
public class SupplierSaveRetrievalServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;
	private static final String REQ_PARM_PREFIX = "data[";

	@Reference
	SupplierSaveRetrievalService nodeSaveRetrievalService;

	@Override
	public void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
		String action = request.getParameter("action");
		List<RequestParameter> requestParameters = request.getRequestParameterList();
		JSONObject inputObject = new JSONObject();
		String id = "";

		// This may appear to be superfluous but consider what happens when someone edits the Supplier Id
		// The old / current ID is the one being extracted here. The new value / thing being changed to
		// lies within the [id] field.
		if(requestParameters.size() > 1){
			String repParmName = requestParameters.get(1).getName();
			String[] subStrings = StringUtils.substringsBetween(repParmName, "[", "]");
			if(subStrings.length > 0){
				id = subStrings[0];
			}
			logger.debug("ID:::" + id);
		}
		
		inputObject.put("supplier", request.getParameter(REQ_PARM_PREFIX + id + "][supplier]"));
		inputObject.put("id", 		request.getParameter(REQ_PARM_PREFIX + id + "][id]"));
		inputObject.put("heading1", request.getParameter(REQ_PARM_PREFIX + id + "][heading1]"));
		inputObject.put("content",  request.getParameter(REQ_PARM_PREFIX + id + "][content]"));

		final JSONObject responseData = nodeSaveRetrievalService.saveDataToNode(action, inputObject, id);

		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().write(responseData.toString());
	}
}
