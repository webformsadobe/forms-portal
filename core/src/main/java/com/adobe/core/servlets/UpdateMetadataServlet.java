package com.adobe.core.servlets;

import com.adobe.core.service.FormsPortalConfigurationService;
import com.day.cq.dam.api.Asset;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.*;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.Servlet;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;


/**
 * This servlet allows the operator to make bulk updates to metadata en masse. This should never be enabled by
 * default on production, and only ever used by Adobe staff with the appropriate training and knowledge.
 *
 * Bulk changes to asset metadata could produce unforeseen and irreversable consequences.
 */
@Component(service = Servlet.class, property = { Constants.SERVICE_DESCRIPTION + "=Bulk Asset Metadata Update Service",
        "sling.servlet.methods={'GET'}", "sling.servlet.paths=" + "/dod/updateMetadata" })
public class UpdateMetadataServlet extends BaseServlet{
    private final String ASSETS_PATH = "/content/dam/dodassets/";

    @SuppressWarnings("unused")
    @Reference
    private SlingRepository slingRepo;

    @SuppressWarnings("unused")
    @Reference
    private ResourceResolverFactory resolverFactory;

    @SuppressWarnings("unused")
    @Reference
    private FormsPortalConfigurationService osgiConfig;

    // Whatever you need to test, test it here.
    protected void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws Exception {
        // Is this servlet enabled? Check it now.
        boolean amEnabled = osgiConfig.getUpdateMetadataEnabled();

        if (amEnabled) {
            final String metadataItem = request.getParameter("item");
            final String metadataFrom = request.getParameter("from");
            final String metadataTo = request.getParameter("to");

            logger.info("Metadata updater received a request to update asset metadata item " + metadataItem
                    + " from " + metadataFrom + " to " + metadataTo);

            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html");
            Writer writer = response.getWriter();

            executeUpdate(metadataItem, metadataFrom, metadataTo, writer);
        } else {
            logger.warn("Update metadata servlet called but the servlet is disabled by the OSGI configuration.");
        }
    }


    private void executeUpdate(String metadataItem, String metadataFrom, String metadataTo, Writer writer)
            throws Exception {
        Map<String, Object> param = new HashMap<>();
        param.put("sling.service.subservice", "datawrite");
        ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
        Session session = resourceResolver.adaptTo(Session.class);
        Workspace workspace = session.getWorkspace();
        QueryManager queryManager = workspace.getQueryManager();

        javax.jcr.query.Query query = queryManager.createQuery(
                "SELECT asset.* FROM [dam:Asset] AS asset WHERE ISDESCENDANTNODE(asset, [" + ASSETS_PATH + "])",
                javax.jcr.query.Query.JCR_SQL2);
        QueryResult result = query.execute();
        NodeIterator nodeItr = result.getNodes();

        int nullItems = 0;
        int affectedItems = 0;
        while (nodeItr.hasNext()) {
            Node node = nodeItr.nextNode();
            Resource rs = resourceResolver.getResource(node.getPath());
            Asset asset = rs.adaptTo(Asset.class);
            if (asset != null && node.hasNode("jcr:content/metadata")) {
                Node metadataNode = node.getNode("jcr:content/metadata");
                if (metadataNode.hasProperty(metadataItem)) {
                    final Property itemProperty = metadataNode.getProperty(metadataItem);
                    final String currentItem = itemProperty.getString();

                    if (currentItem.equals(metadataFrom)) {
                        affectedItems += 1;

                        metadataNode.setProperty(metadataItem, metadataTo);
                    }
                } else {
                    nullItems += 1;
                }
            }
        }

        session.save();
        session.logout();

        writer.write("<html><body>\n");
        writer.write("Number of affected records: " + affectedItems + "<p>\n");
        writer.write("Number of assets without this metadata value: " + nullItems + "\n");
        writer.write("</body></html>\n");
    }
}
