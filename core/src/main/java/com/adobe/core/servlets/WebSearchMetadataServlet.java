package com.adobe.core.servlets;

import com.adobe.core.constants.DepartmentOfDefenceConstants;
import com.day.cq.commons.Externalizer;
import com.day.cq.search.QueryBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.jcr.api.SlingRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.Servlet;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * Web Search Metadata Servlet
 *
 * This servlet returns the metadata for each non-hidden form on the publish server to the web crawler.
 *
 */
@SuppressWarnings("unused")
@Component(
        service = Servlet.class,
        property = {
                ServletResolverConstants.SLING_SERVLET_PATHS   + "=/bin/forms-portal/websearch",
                ServletResolverConstants.SLING_SERVLET_METHODS + "={'GET','POST'}"
        },
        configurationPolicy = ConfigurationPolicy.REQUIRE)
public class WebSearchMetadataServlet extends BaseServlet {
    private static final String METADATA_NODE_PATH = "jcr:content/metadata";
    private static final String FORM_ASSET_PATH = "/content/dam/dodassets";

    @Reference
    private SlingRepository slingRepo;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private QueryBuilder builder;

    @Override
    protected void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        final JSONObject responseData = generateJSONData();

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(responseData.toString());
    }

    @Override
    protected void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        processPost(request, response);
    }


    private JSONObject generateJSONData() {
        logger.trace("Entering generateJSONData");

        JSONArray data = new JSONArray();
        int formsCount = 0;

        try {
            Map<String, Object> param = new HashMap<>();
            param.put("sling.service.subservice", "datadownload");

            ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
            Session session = resourceResolver.adaptTo(Session.class);

            Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);

            Workspace workspace = session.getWorkspace();
            QueryManager queryManager = workspace.getQueryManager();
            Query query = queryManager
                    .createQuery("SELECT asset.* FROM [dam:Asset] AS asset WHERE ISDESCENDANTNODE(asset, ["
                            + FORM_ASSET_PATH + "])", Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeItr = result.getNodes();

            while (nodeItr.hasNext()) {
                final Node currentNode = nodeItr.nextNode();
                Node currentMetadataNode = null;

                String path = currentNode.getPath();
                if (currentNode.hasNode(METADATA_NODE_PATH)) {
                    currentMetadataNode = session.getNode(path + "/" + METADATA_NODE_PATH);
                }

                if (currentMetadataNode != null) {
                    // Exclude hidden forms.
                    String value = "";
                    if (currentMetadataNode.hasProperty(DepartmentOfDefenceConstants.HIDDEN)) {
                        value = currentMetadataNode.getProperty(DepartmentOfDefenceConstants.HIDDEN).getString();
                    }

                    if (!value.equals("Yes")) {
                        JSONObject respObj = new JSONObject();
                        String uriBuilder = "/dod/form?" + currentMetadataNode.getProperty("formNumber").getString();

                        respObj.put("formNumber", getProperty(currentMetadataNode, "formNumber"));
                        respObj.put("formName", getProperty(currentMetadataNode, "formName"));
                        respObj.put("keywords", getProperty(currentMetadataNode, "keywords"));
                        respObj.put("description", getProperty(currentMetadataNode,"dc:description"));
                        respObj.put("resource-link", externalizer.publishLink(resourceResolver, uriBuilder));

                        data.put(respObj);
                        formsCount ++;
                    } else {
                        logger.warn("Ignoring hidden form {}", currentNode.getName());
                    }
                    logger.debug("Node found : {}", currentNode.getName());
                }
            }
            session.logout();
        } catch (Exception e) {
            logger.error(exceptionStacktraceToString(e));
        }
        JSONObject results = new JSONObject();
        results.put("count", formsCount);
        results.put("forms", data);

        logger.trace("Exiting generateJSONData");
        return results;
    }


    /**
     * Returns the value of a property stored in the given FP metadata node.
     */
    private String getProperty(Node metadataNode, String propertyName) throws RepositoryException {
        String result = "";
        if (metadataNode.hasProperty(propertyName)) {
            Property property = metadataNode.getProperty(propertyName);
            if (!property.isMultiple()) {
                result = property.getString();
            }
        }

        return result;
    }
}
