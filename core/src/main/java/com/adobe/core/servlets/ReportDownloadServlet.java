package com.adobe.core.servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.Servlet;

import com.adobe.core.service.FormsPortalConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.json.JSONArray;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.ReportDownloadService;


@SuppressWarnings("unused")
@Component(service = Servlet.class, property = { 
		ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/reports",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET','POST'"
})
public class ReportDownloadServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;
    
    @Reference
    private ReportDownloadService reportDownloadService;

	@Reference
	private FormsPortalConfigurationService reportManager;

    @Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
    	if (reportManager.getOfflineReportEnabled()) {
			JSONArray respJson = reportDownloadService.getReportData();

			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().write(respJson.toString());
		} else {
    		logger.warn("Report Download Servlet:processGet called when Offline Reports are disabled by OSGI configuration.");
		}
    }


    @Override
	protected void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {
    	if (reportManager.getOfflineReportEnabled()) {
			List<RequestParameter> requestParameters = request.getRequestParameterList();
			String id = "";
			String action = "";

			RequestParameter actionReq = request.getRequestParameter("action");
			if (actionReq != null) {
				action = actionReq.getString();
			}

			// This is searching for the file ID (singular) to delete. Suggest you use the logParameters above to
			// devise a way to delete multiple rows at once.
			if(requestParameters.size() > 1){
				String repParmName = requestParameters.get(1).getName();
				String[] subStrings = StringUtils.substringsBetween(repParmName, "[", "]");
				if(subStrings.length > 0){
					id = subStrings[0];
				}
			}

			if (action.equals("remove")) {
				reportDownloadService.deleteReport(id);
			}

			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().write("{}");

		} else {
			logger.warn("Report Download Servlet: processPost called when Offline Reports are disabled by OSGI configuration.");
		}
	}
}
