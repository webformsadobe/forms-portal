package com.adobe.core.servlets;

import java.io.IOException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.json.JSONArray;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.GetUnpublishedNodeDetailsService;


@Component(service = Servlet.class, property = { 
		ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/getUnpublishedNodeDetails",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"
})
public class GetUnpublishedNodeDetailsServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;
    
    @Reference
    private GetUnpublishedNodeDetailsService getUnpublishedNodeDetailsService;

	/**
	 * Executes this servlet. Collect and return metadata for the node type provided as a URI parameter.
	 *
	 * @param request The HTTP request as received.
	 * @param response The response to go back to the client.
	 * @throws IOException Writing to the response object may raise these exceptions.
	 */
	@Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
    	final String basePath = "/content/dodData/data/";
    	final String crxNode = request.getParameter("node");
    	final String nodePath = basePath + crxNode;

    	JSONArray respJson = new JSONArray();
    	
    	if("sponsorData".equals(crxNode)){
    		respJson = getUnpublishedNodeDetailsService.getUnpublishedSponsorCRXNodeData(nodePath);
    	} else if("supplierData".equals(crxNode)){
    		respJson = getUnpublishedNodeDetailsService.getUnpublishedSupplierCRXNodeData(nodePath);
    	} else if("newsData".equals(crxNode)){
    		respJson = getUnpublishedNodeDetailsService.getUnpublishedNewsCRXNodeData(nodePath);
    	} else {
    		logger.warn("getUnpublishedNodeDetailsService called with invalid parameter " +  crxNode);
		}

    	response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(respJson.toString());
    }
}
