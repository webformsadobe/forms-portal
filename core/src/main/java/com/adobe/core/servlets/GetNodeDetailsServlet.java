package com.adobe.core.servlets;

import java.io.IOException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;

import org.json.JSONArray;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.GetNodeDetailsService;


@Component(service = Servlet.class, property = { 
		ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/getNodeDetails",
        ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"
})
public class GetNodeDetailsServlet extends BaseServlet {
    private static final long serialVersionUID = 7726761041508170947L;

    @Reference
    private GetNodeDetailsService getNodeDetailsService;


	/**
	 * Executes the data request. This servlet is responsible for returning metadata for various different types
	 * of node, including categories, news, suppliers and sponsors.
	 *
	 * @param request The HTTP request as received.
	 * @param response The response to send back to the client.
	 * @throws IOException Writing to the response could potentially create IOExceptions
	 */
	@Override
    protected final void processGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
    	final String basePath = "/content/dodData/data/";
    	final String categoriesPath = "/etc/tags/defence/dplis/categories";
    	final String nodeType = request.getParameter("node");
    	final String nodePath = basePath + nodeType;

    	JSONArray respJson = new JSONArray();
    	
    	if("sponsorData".equals(nodeType)){
    		respJson = getNodeDetailsService.getSponsorCRXNodeData(nodePath);
    	} else if("supplierData".equals(nodeType)){
    		respJson = getNodeDetailsService.getSupplierCRXNodeData(nodePath);
    	} else if("newsData".equals(nodeType)){
    		respJson = getNodeDetailsService.getNewsCRXNodeData(nodePath);
    	} else if("defenceCategories".equals(nodeType)){
    		respJson = getNodeDetailsService.getCategoriesData(categoriesPath);
    	} else {
    		logger.warn("getNodeDetailsService called with parameter '" + nodeType + "'.");
		}
    	response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(respJson.toString());
    }
}
