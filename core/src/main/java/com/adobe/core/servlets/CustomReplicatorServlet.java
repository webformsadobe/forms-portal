package com.adobe.core.servlets;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.CustomReplicatorService;


/**
 * Custom replication servlet, which is used to activate news, supplier and sponsor nodes.
 */
@SuppressWarnings("unused")
@Component(
    immediate = true,
    configurationPolicy = ConfigurationPolicy.REQUIRE,
    service = Servlet.class,
    name = "com.adobe.core.servlets.CustomReplicatorServlet",
    property = {
        "sling.servlet.paths=/dod/replicate"
    })
public class CustomReplicatorServlet extends BaseServlet {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * AssetMetaDataService for populating asset meta data models
     */
    @Reference
    private transient CustomReplicatorService customReplicatorService;

    @Override
    protected final void processPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {
        final String dataNode = request.getParameter("dataNode");
        try {
            customReplicatorService.replicate(dataNode);
        } catch (Exception e) {
            logger.error("Custom Replicator Exception");
            logger.error(exceptionStacktraceToString(e));
        }
    }
}
