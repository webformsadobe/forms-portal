package com.adobe.core.servlets;

import com.day.commons.datasource.poolservice.DataSourcePool;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

@Component(service = Servlet.class,
        immediate = true,
        name = "com.adobe.core.servlets.SQLTerminalServlet",
        property = {
                ServletResolverConstants.SLING_SERVLET_PATHS + "=/dod/sqlTerminal",
                ServletResolverConstants.SLING_SERVLET_METHODS + "='POST'"
        })
public class SQLTerminalServlet extends BaseServlet {

    // A reference to the object which can provide a connection to the database.
    @Reference
    private DataSourcePool source;

    @Override
    protected void processPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws Exception {
        String errors = "";
        boolean success = false;
        JSONObject result = new JSONObject();

        // Get the required parameters.
        final RequestParameter dataSourceParam = request.getRequestParameter("dataSource");
        final RequestParameter queryToRunParam = request.getRequestParameter("query");

        final String dataSource = (dataSourceParam != null) ? dataSourceParam.getString() : "";
        final String queryToRun = (queryToRunParam != null) ? queryToRunParam.getString() : "";

        Connection connection = null;
        Statement  statement  = null;
        try {
            DataSource database = (DataSource) source.getDataSource(dataSource);
            connection = database.getConnection();

            if (!connection.isClosed()) {
                // Try to run the query.
                success = true;
                statement = connection.createStatement();
                ResultSet results = statement.executeQuery(queryToRun);

                ResultSetMetaData metaData = results.getMetaData();
                final int numColumns = metaData.getColumnCount();

                // The magic i + 1 is used because database columns are indexed from 1, not 0.
                String[] columnNames = new String[numColumns];
                for (int i = 0; i < numColumns; ++i) {
                    columnNames[i] = metaData.getColumnName(i + 1);
                }
                result.put("columns", columnNames);

                int rows = 0;
                JSONArray resultsArray = new JSONArray();
                while (results.next() && rows < 100) {
                    JSONObject resultRow = new JSONObject();
                    for (int i = 0; i < numColumns; ++i) {
                        resultRow.put(columnNames[i], results.getString(i + 1));
                    }
                    resultsArray.put(resultRow);
                    rows += 1;
                }
                result.put("queryData", resultsArray);
                result.put("rows", rows);
            }

        } catch (Exception e) {
            errors += e.getMessage() + "\n";
            success = false;
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        result.put("queryRan", queryToRun);
        result.put("dataSourceUsed", dataSource);
        result.put("success", success);
        result.put("error", errors);

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(result.toString());
    }
}
