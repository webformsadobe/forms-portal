package com.adobe.core.servlets;

import java.io.IOException;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.models.AssetMetaData;
import com.adobe.core.service.AssetMetaDataService;


/**
 * Servlet that gives the Asset meta-data, which is shown in formsportal view meta-data
 */
@Component(
    immediate = true,
    service = Servlet.class,
    name = "com.adobe.core.servlets.AssetMetaDataServlet",
    property = {
        "sling.servlet.resourceTypes=dam:Asset",
    	"sling.servlet.selectors=properties",
    	"sling.servlet.extensions=json"
    })
public class AssetMetaDataServlet extends BaseServlet {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * AssetMetaDataService for populating asset meta data models
     */
    @Reference
    private transient AssetMetaDataService assetMetaDataService;

    @Override
    protected final void processGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException, RepositoryException {
        final String assetPath = request.getResource().getPath();
        logger.debug("Path provided: " + assetPath);

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        final AssetMetaData respObj = assetMetaDataService.getAssetMetadata(assetPath, request);
        final JSONObject jsonObj    = new JSONObject(respObj);

        response.getWriter().write(jsonObj.toString());
    }
}
