package com.adobe.core.servlets;

import javax.servlet.Servlet;

import com.adobe.core.service.FormsPortalConfigurationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

import com.adobe.core.service.OfflineCDGenerationService;

import java.io.IOException;


@SuppressWarnings("unused")
@Component(
        immediate = true,
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        service = Servlet.class,
        property = {
                ServletResolverConstants.SLING_SERVLET_PATHS + "=/bin/forms-portal/admin/extractFormsOfflineReport",
                ServletResolverConstants.SLING_SERVLET_METHODS + "='GET'"}
)
public class ManualOfflineReportServlet extends BaseServlet {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @Reference
    private transient Scheduler scheduler;

    @Reference
    private transient FormsPortalConfigurationService reportManager;

    @Reference
    private transient OfflineCDGenerationService archiver;

    private final transient Runnable job = () -> archiver.extractSaveMetadata();

    /**
     * AssetMetadataExporterService for extracting and saving assetmetadata
     *
     * This servlets fires off an immediate job to the scheduler for background execution.
     */
    @Override
    protected final void processGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException {
        logger.debug("Request to schedule manual metadata extraction received.");

        boolean success = false;
        if (reportManager.getManualOfflineReportEnabled()) {
            ScheduleOptions options = scheduler.NOW();
            options.canRunConcurrently(false);
            options.name("Manual-Forms-Archive-Request");

            success = scheduler.schedule(job, options);
        } else {
            logger.warn("Attempted to trigger a manual offline CD export process but manual exports are disabled by configuration.");
        }

        JSONObject responseData = new JSONObject();
        responseData.put("success", success);

        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.getWriter().write(responseData.toString());
    }
}
