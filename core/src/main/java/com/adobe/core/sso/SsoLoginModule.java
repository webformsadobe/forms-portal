package com.adobe.core.sso;


import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.jackrabbit.oak.api.AuthInfo;
import org.apache.jackrabbit.oak.api.CommitFailedException;
import org.apache.jackrabbit.oak.api.Root;
import org.apache.jackrabbit.oak.commons.DebugTimer;
import org.apache.jackrabbit.oak.namepath.NamePathMapper;
import org.apache.jackrabbit.oak.plugins.value.ValueFactoryImpl;
import org.apache.jackrabbit.oak.spi.security.ConfigurationParameters;
import org.apache.jackrabbit.oak.spi.security.authentication.AbstractLoginModule;
import org.apache.jackrabbit.oak.spi.security.authentication.AuthInfoImpl;
import org.apache.jackrabbit.oak.spi.security.authentication.ImpersonationCredentials;
import org.apache.jackrabbit.oak.spi.security.authentication.PreAuthenticatedLogin;
import org.apache.jackrabbit.oak.spi.security.authentication.credentials.CredentialsSupport;
import org.apache.jackrabbit.oak.spi.security.authentication.credentials.SimpleCredentialsSupport;
import org.apache.jackrabbit.oak.spi.security.authentication.external.*;
import org.apache.jackrabbit.oak.spi.whiteboard.Whiteboard;
import org.apache.jackrabbit.oak.spi.whiteboard.WhiteboardUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Credentials;
import javax.jcr.RepositoryException;
import javax.jcr.SimpleCredentials;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

//
//public class SsoLoginModule extends AbstractLoginModule {
//    private static final Logger log = LoggerFactory.getLogger(SsoLoginModule.class);
//    private static final int MAX_SYNC_ATTEMPTS = 50;
////    public static final String PARAM_IDP_NAME = "idp.name";
////    public static final String PARAM_SYNC_HANDLER_NAME = "sync.handlerName";
//    private ExternalIdentityProviderManager idpManager;
//    private SyncManager syncManager;
//    private CredentialsSupport credentialsSupport = SimpleCredentialsSupport.getInstance();
//    private ConfigurationParameters osgiConfig;
//    private ExternalIdentityProvider idp;
//    private SyncHandler syncHandler;
//    private ExternalUser externalUser;
//    private Credentials credentials;
//
//    public SsoLoginModule() {
//    }
//
//    public SsoLoginModule(ConfigurationParameters osgiConfig) {
//        this.osgiConfig = osgiConfig;
//    }
//
//    @Override
//    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> opts) {
//        super.initialize(subject, callbackHandler, sharedState, opts);
//        if (this.osgiConfig != null) {
//            this.options = ConfigurationParameters.of(new ConfigurationParameters[]{this.osgiConfig, this.options});
//        }
//
//        Whiteboard whiteboard = this.getWhiteboard();
//        if (whiteboard == null) {
//            log.error("External login module needs whiteboard. Will not be used for login.");
//        } else {
////            String idpName = (String)this.options.getConfigValue("idp.name", "");
//            // override
//            String idpName = "ldap.drn.mil.au";
//            if (idpName.isEmpty()) {
//                log.error("External login module needs IPD name. Will not be used for login.");
//            } else {
//                if (this.idpManager == null) {
//                    this.idpManager = WhiteboardUtils.getService(whiteboard, ExternalIdentityProviderManager.class);
//                }
//
//                if (this.idpManager == null) {
//                    log.error("External login module needs IDPManager. Will not be used for login.");
//                } else {
//                    this.idp = this.idpManager.getProvider(idpName);
//                    if (this.idp == null) {
//                        log.error("No IDP found with name {}. Will not be used for login.", idpName);
//                    }
//                }
//            }
//
////            String syncHandlerName = (String)this.options.getConfigValue("sync.handlerName", "");
//            // override
//            String syncHandlerName = "sso-ad";
//
//            if (syncHandlerName.isEmpty()) {
//                log.error("External login module needs SyncHandler name. Will not be used for login.");
//            } else {
//                if (this.syncManager == null) {
//                    this.syncManager = WhiteboardUtils.getService(whiteboard, SyncManager.class);
//                }
//
//                if (this.syncManager == null) {
//                    log.error("External login module needs SyncManager. Will not be used for login.");
//                } else {
//                    this.syncHandler = this.syncManager.getSyncHandler(syncHandlerName);
//                    if (this.syncHandler == null) {
//                        log.error("No SyncHandler found with name {}. Will not be used for login.", syncHandlerName);
//                    }
//                }
//            }
//
//            if (this.idp instanceof CredentialsSupport) {
//                this.credentialsSupport = (CredentialsSupport)this.idp;
//            }
//
//        }
//    }
//
//    public boolean login() throws LoginException {
//        if (this.idp != null && this.syncHandler != null) {
//            debug("SSO Entering login attempt");
//            this.credentials = this.getCredentials();
//            PreAuthenticatedLogin preAuthLogin = this.getSharedPreAuthLogin();
//            String userId = this.getUserId(preAuthLogin, this.credentials);
//
//            if (userId != null) {
//                debug("SSO Known user ID : " + userId);
//            } else {
//                debug("SSO No known user ID located. Trying to extract.");
//
//                SimpleCredentials haveAGo = (SimpleCredentials) credentials;
//                if (haveAGo != null) {
//                    userId = haveAGo.getUserID();
//                    debug("SSO UserID is now found to be " + userId);
//                } else {
//                    debug("SSO Had a go, no username found");
//                }
//            }
//
//            if (userId == null && this.credentials == null) {
//                debug("No credentials | userId found for external login module. ignoring.");
//                return false;
//            } else {
//                Object logId = userId != null ? userId : this.credentials;
//
//                try {
//                    SyncedIdentity sId = this.getSyncedIdentity(userId);
//                    if (this.ignore(sId, preAuthLogin)) {
//                        return false;
//                    } else {
//                        if (userId != null) {
//                            this.externalUser = this.idp.getUser(userId);
//                        } else {
//                            this.externalUser = this.idp.authenticate(this.credentials);
//                        }
//
//                        if (this.externalUser != null) {
//                            debug("IDP {} returned valid user {}", this.idp.getName(), this.externalUser.toString());
//                            if (this.credentials != null) {
//                                this.sharedState.put("org.apache.jackrabbit.credentials", this.credentials);
//                            }
//
//                            this.sharedState.put("javax.security.auth.login.name", this.externalUser.getId());
//                            this.syncUser(this.externalUser);
//                            return true;
//                        } else {
//                            debug("IDP {} returned null for {}", this.idp.getName(), logId.toString());
//                            if (sId != null) {
//                                debug("local user exists for '{}'. re-validating.", sId.getId());
//                                this.validateUser(sId.getId());
//                            }
//
//                            return false;
//                        }
//                    }
//                } catch (ExternalIdentityException var6) {
//                    log.error("Error while authenticating '{}' with {}", new Object[]{logId, this.idp.getName(), var6});
//                    return false;
//                } catch (LoginException var7) {
//                    debug("IDP {} throws login exception for '{}': {}", this.idp.getName(), logId.toString(), var7.getMessage());
//                    throw var7;
//                } catch (Exception var8) {
//                    debug("SyncHandler {} throws sync exception for '{}'", this.syncHandler.getName(), logId.toString(), var8.toString());
//                    LoginException le = new LoginException("Error while syncing user.");
//                    le.initCause(var8);
//                    throw le;
//                }
//            }
//        } else {
//            return false;
//        }
//    }
//
//    public boolean commit() {
//        boolean result = false;
//        if (this.externalUser == null) {
//            this.clearState();
//            debug("SSO Commit externaluser was null, clearing");
//        } else {
//            Set<? extends Principal> principals = this.getPrincipals(this.externalUser.getId());
//            if (!principals.isEmpty()) {
//                if (!this.subject.isReadOnly()) {
//                    this.subject.getPrincipals().addAll(principals);
//                    if (this.credentials != null) {
//                        this.subject.getPublicCredentials().add(this.credentials);
//                    }
//
//                    setAuthInfo(this.createAuthInfo(this.externalUser.getId(), principals), this.subject);
//                    debug("SSO Commit Auth info set.");
//                } else {
//                    debug("Could not add information to read only subject {}", this.subject.toString());
//                }
//
//                result = true;
//            } else {
//                debug("SSO Commit principals were empty, clearing");
//                this.clearState();
//            }
//        }
//
//        return result;
//    }
//
//    @Override
//    public boolean abort() {
//        this.clearState();
//        return true;
//    }
//
//    private String getUserId(PreAuthenticatedLogin preAuthLogin, Credentials credentials) {
//        if (preAuthLogin != null) {
//            return preAuthLogin.getUserId();
//        } else {
//            return credentials != null ? this.credentialsSupport.getUserId(credentials) : null;
//        }
//    }
//
//    private SyncedIdentity getSyncedIdentity(String userId) throws RepositoryException {
//        UserManager userMgr = this.getUserManager();
//        return userId != null && userMgr != null ? this.syncHandler.findIdentity(userMgr, userId) : null;
//    }
//
//    private boolean ignore(SyncedIdentity syncedIdentity, PreAuthenticatedLogin preAuthLogin) {
//        if (syncedIdentity != null) {
//            ExternalIdentityRef externalIdRef = syncedIdentity.getExternalIdRef();
//            if (externalIdRef == null) {
//                debug("ignoring local user: {}", syncedIdentity.getId());
//                return true;
//            }
//
//            if (!this.idp.getName().equals(externalIdRef.getProviderName())) {
//                debug("ignoring foreign identity: {} (idp={})", externalIdRef.getProviderName(), this.idp.getName());
//                return true;
//            }
//
//            if (preAuthLogin != null && !this.syncHandler.requiresSync(syncedIdentity)) {
//                debug("pre-authenticated external user {} does not require syncing.", syncedIdentity.toString());
//                return true;
//            }
//        }
//
//        return false;
//    }
//
//    private void syncUser( ExternalUser user) throws SyncException {
//        Root root = this.getRoot();
//        if (root == null) {
//            throw new SyncException("Cannot synchronize user. root == null");
//        } else {
//            UserManager userManager = this.getUserManager();
//            if (userManager == null) {
//                throw new SyncException("Cannot synchronize user. userManager == null");
//            } else {
//                int numAttempt = 0;
//
//                while(true) {
//                    if (numAttempt++ < MAX_SYNC_ATTEMPTS) {
//                        SyncContext context = null;
//
//                        try {
//                            DebugTimer timer = new DebugTimer();
//                            context = this.syncHandler.createContext(this.idp, userManager, new ValueFactoryImpl(root, NamePathMapper.DEFAULT));
//                            SyncResult syncResult = context.sync(user);
//                            timer.mark("sync");
//                            if (root.hasPendingChanges()) {
//                                root.commit();
//                                timer.mark("commit");
//                            }
//
//                            debug("syncUser({}) {}, status: {}", user.getId(), timer.getString(), syncResult.getStatus().toString());
//                        } catch (CommitFailedException var11) {
//                            log.warn("User synchronization failed during commit: {}. (attempt {}/{})", new Object[]{var11.toString(), numAttempt, MAX_SYNC_ATTEMPTS});
//                            root.refresh();
//                            continue;
//                        } finally {
//                            if (context != null) {
//                                context.close();
//                            }
//
//                        }
//
//                        return;
//                    }
//
//                    throw new SyncException("User synchronization failed during commit after attempt " + MAX_SYNC_ATTEMPTS);
//                }
//            }
//        }
//    }
//
//    private void validateUser( String id) throws SyncException {
//        SyncContext context = null;
//
//        try {
//            Root root = this.getRoot();
//            if (root == null) {
//                throw new SyncException("Cannot synchronize user. root == null");
//            }
//
//            UserManager userManager = this.getUserManager();
//            if (userManager == null) {
//                throw new SyncException("Cannot synchronize user. userManager == null");
//            }
//
//            DebugTimer timer = new DebugTimer();
//            context = this.syncHandler.createContext(this.idp, userManager, new ValueFactoryImpl(root, NamePathMapper.DEFAULT));
//            context.sync(id);
//            timer.mark("sync");
//            root.commit();
//            timer.mark("commit");
//            debug("validateUser({}) {}", id, timer.getString());
//        } catch (CommitFailedException var9) {
//            throw new SyncException("User synchronization failed during commit.", var9);
//        } finally {
//            if (context != null) {
//                context.close();
//            }
//
//        }
//
//    }
//
//
//    private AuthInfo createAuthInfo( String userId,  Set<? extends Principal> principals) {
//        Credentials creds;
//        if (this.credentials instanceof ImpersonationCredentials) {
//            creds = ((ImpersonationCredentials)this.credentials).getBaseCredentials();
//        } else {
//            creds = this.credentials;
//        }
//
//        Map<String, Object> attributes = new HashMap<>();
//        Object shared = this.sharedState.get("javax.security.auth.login.attributes");
//        if (shared instanceof Map) {
//            Iterator iterator = ((Map)shared).entrySet().iterator();
//
//            while(iterator.hasNext()) {
//                Map.Entry entry = (Map.Entry)iterator.next();
//                attributes.put(entry.getKey().toString(), entry.getValue());
//            }
//        } else if (creds != null) {
//            attributes.putAll(this.credentialsSupport.getAttributes(creds));
//        }
//
//        return new AuthInfoImpl(userId, attributes, principals);
//    }
//
//    private static void debug( String msg, String... args) {
//        if (log.isDebugEnabled()) {
//            log.debug(msg, args);
//        }
//    }
//
//    @Override
//    protected void clearState() {
//        super.clearState();
//        this.externalUser = null;
//        this.credentials = null;
//    }
//
//
//    protected Set<Class> getSupportedCredentials() {
//        return this.credentialsSupport.getCredentialClasses();
//    }
//
//    public void setSyncManager( SyncManager syncManager) {
//        this.syncManager = syncManager;
//    }
//
//    public void setIdpManager( ExternalIdentityProviderManager idpManager) {
//        this.idpManager = idpManager;
//    }
//}
