package com.adobe.core.models;

public class SponsorData {
	private String DT_RowId;
	private String sponsor;
	private String sponsorId;
	private String heading1;
	private String heading2;
	private String contactOfficer;
	private String contactPosition;
	private String location;
	private String phone;
	private String fax;
	private String email;
	private String mobile;
	private String secondaryContactOfficer;
	private String secondaryContactPosition;
	private String secondaryLocation;
	private String secondaryPhone;
	private String secondaryFax;
	private String secondaryEmail;
	private String secondaryMobile;

	// Package-private - you have to go through the factory method ok?
	SponsorData() {}

	public String getSponsor() {
		return this.sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getSponsorId() {
		return this.sponsorId;
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getHeading1() {
		return this.heading1;
	}

	public void setHeading1(String heading1) {
		this.heading1 = heading1;
	}

	public String getHeading2() {
		return this.heading2;
	}

	public void setHeading2(String heading2) {
		this.heading2 = heading2;
	}

	public String getContactOfficer() {
		return this.contactOfficer;
	}

	public void setContactOfficer(String contactOfficer) {
		this.contactOfficer = contactOfficer;
	}

	public String getContactPosition() {
		return this.contactPosition;
	}

	public void setContactPosition(String contactPosition) {
		this.contactPosition = contactPosition;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecondaryContactOfficer() {
		return this.secondaryContactOfficer;
	}

	public void setSecondaryContactOfficer(String secondaryContactOfficer) {
		this.secondaryContactOfficer = secondaryContactOfficer;
	}

	public String getSecondaryContactPosition() {
		return this.secondaryContactPosition;
	}

	public void setSecondaryContactPosition(String secondaryContactPosition) {
		this.secondaryContactPosition = secondaryContactPosition;
	}

	public String getSecondaryLocation() {
		return this.secondaryLocation;
	}

	public void setSecondaryLocation(String secondaryLocation) {
		this.secondaryLocation = secondaryLocation;
	}

	public String getSecondaryPhone() {
		return this.secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getSecondaryFax() {
		return this.secondaryFax;
	}

	public void setSecondaryFax(String secondaryFax) {
		this.secondaryFax = secondaryFax;
	}

	public String getSecondaryEmail() {
		return this.secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSecondaryMobile() {
		return this.secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	public String getDT_RowId() { return this.DT_RowId; }

	public void setDT_RowId(String DT_RowId) { this.DT_RowId = DT_RowId; }
}
