package com.adobe.core.models;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.text.ParseException;

public class WfsFactory {
    private WfsFactory() {}

    public static Wfs create(Node assetNode) {
        Wfs result = null;

        try {
            AssetMetaData assetMetadataStore = AssetMetaDataFactory.create(assetNode);
            result = new Wfs(assetMetadataStore);
        } catch (Exception e) {
            // Actually an empty block, because this constructor returns null on failure.
        }

        return result;
    }
}
