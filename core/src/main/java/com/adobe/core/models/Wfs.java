package com.adobe.core.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sponsor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="arc-replace-by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="keywords" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="review-date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="arc-details" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="format" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="classification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stock-number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="controlled" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="old-number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="create-date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unit-of-issue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="supplier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="arc-date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="version-number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="version-date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="registry-number" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * <p>
 * <p>
 * This class provides a wrapper around an AssetMetaData instance.
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(name = "", propOrder = {"sponsor", "arcReplaceBy", "keywords", "reviewDate", "arcDetails", "format",
        "classification", "stockNumber", "controlled", "number", "oldNumber", "createDate", "unitOfIssue", "supplier",
        "name", "arcDate", "versionNumber", "group", "versionDate", "registryNumber"})
@XmlRootElement(name = "wfs")
public class Wfs {
    private AssetMetaData metaDataStore;

    private SimpleDateFormat OUT_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat SECOND_IN_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat IN_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private static String[] MARKERS = {"Unclassified".toUpperCase(), "Protected".toUpperCase(), "Confidential".toUpperCase(),
            "Secret".toUpperCase(), "Top Secret".toUpperCase(), "Restricted".toUpperCase()};

    private static List<String> List_MARKERS;

    static {
        List_MARKERS = Arrays.asList(MARKERS);
    }


    /** That feeling when you want to package-private your constructors but can't because Java **/
    public Wfs() {}

    /**
     * Package-private constructors, prevent other modules from instantiating these data models directly.
     */
    Wfs(AssetMetaData metaDataStore) {
        this.metaDataStore = metaDataStore;
    }

    // @formatter:off
    @Override
    public String toString() {

        return "\"/content/dam/dodassets/" + destFileName + "\"," + "\""
                + (getFormat().trim().equals("E") ? fileName : "download.pdf") + "\"," + "\"" + getNumber() + "\"," + "\""
                + getNumber() + "\"," + "\"" + getSponsor() + "\"" + ",\"" + getArcReplaceBy() + "\"" + ",\"" + getKeywords() + "\"" + ",\""
                + getReviewDate() + "\"" + ",\"" + getArcDetails() + "\"" + ",\"" + getFormat() + "\"" + ",\""
                + fixClassification(getClassification()) + "\"" + ",\"" + getStockNumber() + "\"" + ",\"" + getControlled() + "\""
                + ",\"" + getNumber() + "\"" + ",\"" + getOldNumber() + "\"" + ",\"" + getCreateDate() + "\"" + ",\""
                + getUnitOfIssue() + "\"" + ",\"" + getSupplier() + "\"" + ",\"" + getName() + "\"" + ",\"" + getArcDate() + "\""
                + ",\"" + getVersionNumber() + "\"" + ",\"" + getGroup() + "\"" + ",\"" + getVersionDate() + "\"" + ",\""
                + getRegistryNumber() + "\"" + ",\"" + getCoversheetString() + "\"" + ","
                + (getFormat().equals("P") ? "TRUE" : "FALSE") + ",";

    }

    private String fixClassification(String classification) {
        if (classification != null && List_MARKERS.contains(classification.toUpperCase())) {
            classification = classification.toUpperCase();
        }
        return classification;
    }

    public static void printHeader() {
        System.out.println("absTargetPath" + ",relSrcPath" + ",dc:title" + ",formNumber" + ",sponsor" + ",arcReplaceBy"
                + ",keywords" + ",reviewDate" + ",arcDetails" + ",format" + ",classification" + ",stockNumber"
                + ",controlled" + ",number" + ",oldNumber" + ",createDate" + ",unitOfIssue" + ",supplier" + ",formName"
                + ",arcDate" + ",versionNumber" + ",group" + ",versionDate" + ",registryNumber" + ",coverSheetContent1"
                + ",coverpage {{ Boolean }}");

    }
    // @formatter:on

    private String parseDate(String date) {
        if (date == null || "".equals(date)) {
            return "";
        }
        Date d = null;

        try {
            d = IN_FORMAT.parse(date);
        } catch (ParseException e) {
            // try again if cant be parsed
            try {
                d = SECOND_IN_FORMAT.parse(date);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }

        return OUT_FORMAT.format(d);
    }

    private String fileName = "";
    private String destFileName = "";

    public String getDestFileName() {
        return destFileName;
    }

    public void setDestFileName(String destFileName) {
        this.destFileName = destFileName;
    }

    /**
     * Get and set the coversheet string property (to be replaced)
     */
    public String getCoversheetString() {
        return metaDataStore.getCoverSheetContent();
    }

    public void setCoversheetString(String coverSheet) {
        metaDataStore.setCoverSheetContent(coverSheet);
    }

    /**
     * Gets the value of the sponsor property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true, defaultValue = "NO_SPONSOR")
    public String getSponsor() {
        return metaDataStore.getSponsor();
    }

    public void setSponsor(String value) {
        metaDataStore.setSponsor(value);
    }

    /**
     * Gets the value of the arcReplaceBy property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "arc-replace-by", required = true)
    public String getArcReplaceBy() {
        return metaDataStore.getArcReplaceBy();
    }

    public void setArcReplaceBy(String value) {
        metaDataStore.setArcReplaceBy(value);
    }

    /**
     * Gets the value of the keywords property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getKeywords() {
        return metaDataStore.getKeywords();
    }

    public void setKeywords(String value) {
        metaDataStore.setKeywords(value);
    }

    /**
     * Gets the value of the reviewDate property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "review-date", required = true)
    public String getReviewDate() {
        return parseDate(metaDataStore.getReviewDate());
    }

    public void setReviewDate(String value) {
        metaDataStore.setReviewDate(parseDate(value));
    }

    /**
     * Gets the value of the arcDetails property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "arc-details", required = true)
    public String getArcDetails() {
        return metaDataStore.getArcDetails();
    }

    public void setArcDetails(String value) {
        metaDataStore.setArcDetails(value);
    }

    /**
     * Gets the value of the format property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getFormat() {
        return metaDataStore.getFormat();
    }

    public void setFormat(String value) {
        metaDataStore.setFormat(value);
    }

    /**
     * Gets the value of the classification property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getClassification() {
        return metaDataStore.getClassification();
    }

    public void setClassification(String value) {
        metaDataStore.setClassification(value);
    }

    /**
     * Gets the value of the stockNumber property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "stock-number", required = true)
    public String getStockNumber() {
        return metaDataStore.getStockNumber();
    }

    public void setStockNumber(String value) {
        metaDataStore.setStockNumber(value);
    }

    /**
     * Gets the value of the controlled property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getControlled() {
        return metaDataStore.getControlled();
    }

    public void setControlled(String value) {
        metaDataStore.setControlled(value);
    }

    /**
     * Gets the value of the number property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getNumber() {
        return metaDataStore.getFormNumber();
    }

    public void setNumber(String value) {
        metaDataStore.setFormNumber(value);
    }

    /**
     * Gets the value of the oldNumber property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "old-number", required = true)
    public String getOldNumber() {
        return metaDataStore.getOldNumber();
    }

    public void setOldNumber(String value) {
        metaDataStore.setOldNumber(value);
    }

    /**
     * Gets the value of the createDate property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "create-date", required = true)
    public String getCreateDate() {
        return parseDate(metaDataStore.getCreateDate());
    }

    public void setCreateDate(String value) {
        metaDataStore.setCreateDate(parseDate(value));
    }

    /**
     * Gets the value of the unitOfIssue property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "unit-of-issue", required = true)
    public String getUnitOfIssue() {
        return metaDataStore.getUnitOfIssue();
    }

    public void setUnitOfIssue(String value) {
        metaDataStore.setUnitOfIssue(value);
    }

    /**
     * Gets the value of the supplier property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getSupplier() {
        return metaDataStore.getSupplier();
    }

    public void setSupplier(String value) {
        metaDataStore.setSupplier(value);
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getName() {
        return metaDataStore.getFormName();
    }

    public void setName(String value) {
        metaDataStore.setFormName(value);
    }

    /**
     * Gets the value of the arcDate property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "arc-date", required = true)
    public String getArcDate() {
        return metaDataStore.getArcDateVersion();
    }

    public void setArcDate(String value) {
        metaDataStore.setArcDateVersion(value);
    }

    /**
     * Gets the value of the versionNumber property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "version-number", required = true)
    public String getVersionNumber() {
        return metaDataStore.getVersionNumber();
    }

    public void setVersionNumber(String value) {
        metaDataStore.setVersionNumber(value);
    }

    /**
     * Gets the value of the group property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(required = true)
    public String getGroup() {
        return metaDataStore.getGroup();
    }

    public void setGroup(String value) {
        metaDataStore.setGroup(value);
    }

    /**
     * Gets the value of the versionDate property.
     *
     * @return possible object is {@link String }
     */
    @XmlElement(name = "version-date", required = true)
    public String getVersionDate() {
        return parseDate(metaDataStore.getVersionDate());
    }

    public void setVersionDate(String value) {
        metaDataStore.setVersionDate(parseDate(value));
    }

    /**
     * Gets the value of the registryNumber property.
     */
    @XmlElement(name = "registry-number")
    public String getRegistryNumber() {
        return metaDataStore.getRegistryNumber();
    }

    public void setRegistryNumber(String value) {
        metaDataStore.setRegistryNumber(value);
    }

    /**
     *
     */
    public Boolean getIsCoverpage() {
        return metaDataStore.getIsCoverPage();
    }

    public void setIsCoverpage(Boolean isCoverpage) {
        metaDataStore.setIsCoverPage(isCoverpage);
    }

    /**
     *
     */
    public Boolean getObsolete() {
        return Boolean.valueOf(metaDataStore.getObsolete());
    }

    public void setObsolete(Boolean obsolete) {
        metaDataStore.setObsolete(Boolean.toString(obsolete));
    }


    /**
     * Getter and setter for the cover page link and cover page link text metadata properties.
     */
    public String getCoversheetLinkName() { return metaDataStore.getCoversheetLinkName(); }
    public void setCoversheetLinkName(String value) { metaDataStore.setCoversheetLinkName(value);}

    public String getCoversheetLinkAddr() { return metaDataStore.getCoversheetLinkAddr(); }
    public void setCoversheetLinkAddr(String value) { metaDataStore.setCoversheetLinkAddr(value);}

    public SupplierData getSupplierData() { return metaDataStore.getSupplierData(); }
}
