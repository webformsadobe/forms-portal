package com.adobe.core.models;

import com.adobe.core.constants.DepartmentOfDefenceConstants;

import javax.jcr.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Factory class for AssetMetadata model objects.
 *
 * Methods are provided to instantiate a model class based on a node resource object. Note that this factory
 * does not populate the supplier and sponsor sub-models - callers wishing to use this information will need
 * to load it themselves.
 *
 * @todo try and find some way to use reflection to auto populate these models
 */
public class AssetMetaDataFactory {
    private static final SimpleDateFormat fromDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat toDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static AssetMetaData create(Node assetNode) throws RepositoryException, ParseException {
        AssetMetaData result = new AssetMetaData();

        // Set the simple string fields first.
        result.setArcDateVersion(getProperty(assetNode, DepartmentOfDefenceConstants.ARC_DATE_VERSISON_METADATA));
        result.setArcDetails(getProperty(assetNode,     DepartmentOfDefenceConstants.ARC_DETAILS_METADATA));
        result.setArcReplaceBy(getProperty(assetNode,   DepartmentOfDefenceConstants.ARC_REPLACE_BY_METADATA));
        result.setClassification(getProperty(assetNode, DepartmentOfDefenceConstants.CLASSIFICATION_METADATA));
        result.setControlled(getProperty(assetNode,     DepartmentOfDefenceConstants.CONTROLLED_METADATA));
        result.setDevelopedBy(getProperty(assetNode,    DepartmentOfDefenceConstants.DEVELOPED_BY));
        result.setFormat(getProperty(assetNode,         DepartmentOfDefenceConstants.FORMAT_METADATA));
        result.setFormName(getProperty(assetNode,       DepartmentOfDefenceConstants.FORM_NAME_METADATA));
        result.setFormNumber(getProperty(assetNode,     DepartmentOfDefenceConstants.FORM_NUM_METADATA));
        result.setGroup(getProperty(assetNode,          DepartmentOfDefenceConstants.GROUP_METADATA));
        result.setKeywords(getProperty(assetNode,       DepartmentOfDefenceConstants.KEYWORDS_METADATA));
        result.setOldNumber(getProperty(assetNode,      DepartmentOfDefenceConstants.OLD_NUMBER_METADATA));
        result.setRegistryNumber(getProperty(assetNode, DepartmentOfDefenceConstants.REGISTRY_NUMBER_METADATA));
        result.setReviewDate(getProperty(assetNode,     DepartmentOfDefenceConstants.REVIEW_DATE_METADATA));
        result.setSponsor(getProperty(assetNode,        DepartmentOfDefenceConstants.SPONSOR_METADATA));
        result.setStockNumber(getProperty(assetNode,    DepartmentOfDefenceConstants.STOCK_NUMBER_METADATA));
        result.setSupplier(getProperty(assetNode,       DepartmentOfDefenceConstants.SUPPLIER_METADATA));
        result.setUnitOfIssue(getProperty(assetNode,    DepartmentOfDefenceConstants.UNIT_OF_ISSUE_METADATA));
        result.setVersionDate(getProperty(assetNode,    DepartmentOfDefenceConstants.VERSION_DATE_METADATA));
        result.setVersionNumber(getProperty(assetNode,  DepartmentOfDefenceConstants.VERSION_NUMBER));

        // Set the cover page data
        boolean isCoverPage = Boolean.valueOf(getProperty(assetNode, DepartmentOfDefenceConstants.IS_COVERPAGE_METADATA));
        result.setIsCoverPage(isCoverPage);
        if (isCoverPage) {
            result.setCoverSheetContent(getProperty(assetNode,  DepartmentOfDefenceConstants.COVERSHEET_CONTENT_METADATA));
            result.setCoversheetLinkAddr(getProperty(assetNode, DepartmentOfDefenceConstants.COVERSHEET_LINK_ADDR_METADATA));
            result.setCoversheetLinkName(getProperty(assetNode, DepartmentOfDefenceConstants.COVERSHEET_LINK_NAME_METADATA));
            result.setObsolete(getProperty(assetNode,           DepartmentOfDefenceConstants.OBSOLETE_METADATA));
        }

        // Format and set the date objects.
        final String createDate  = getProperty(assetNode, DepartmentOfDefenceConstants.CREATED_DATE_METADATA);
        final String versionDate = getProperty(assetNode, DepartmentOfDefenceConstants.VERSION_DATE_METADATA);
        final String reviewDate  = getProperty(assetNode, DepartmentOfDefenceConstants.REVIEW_DATE_METADATA);

        final String createDateParsed  = (!createDate.isEmpty())  ? toDateFormat.format(fromDateFormat.parse(createDate))  : "";
        final String versionDateParsed = (!versionDate.isEmpty()) ? toDateFormat.format(fromDateFormat.parse(versionDate)) : "";
        final String reviewDateParsed  = (!reviewDate.isEmpty())  ? toDateFormat.format(fromDateFormat.parse(reviewDate))  : "";

        result.setCreateDate(createDateParsed);
        result.setVersionDate(versionDateParsed);
        result.setReviewDate(reviewDateParsed);


        return result;
    }

    public static AssetMetaData create() {
        return new AssetMetaData();
    }

    private static String getProperty(Node assetNode, String propertyName) throws RepositoryException {
        String result = "";
        if (assetNode.hasProperty(propertyName)) {
            Property property = assetNode.getProperty(propertyName);
            if (!property.isMultiple()) {
                result = property.getValue().getString();
            }
        }

        return result;
    }
}
