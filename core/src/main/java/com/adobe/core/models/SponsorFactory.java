package com.adobe.core.models;

import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Factory methods for the creation of the supplier data source objects.
 *
 */
public class SponsorFactory extends BaseFactory {
    public static final String SPONSOR_NODE_PATH = "/content/dodData/data/sponsorData";

    /**
     * Returns a properly initialised SupplierData object from the JCR Node provided.
     * If the node is not a supplier data node, the object returned will have null fields.
     *
     * @param node The JCR node to load from.
     * @return A SupplierData object.
     */
    public static SponsorData create(Node node, Logger log) {
        SponsorData res = new SponsorData();

        res.setSponsor(getProperty(node, "sponsor", log));
        res.setHeading1(getProperty(node, "heading1", log));
        res.setHeading2(getProperty(node, "heading2", log));
        res.setContactOfficer(getProperty(node, "contactOfficer", log));
        res.setContactPosition(getProperty(node, "contactPosition", log));
        res.setLocation(getProperty(node, "location", log));
        res.setPhone(getProperty(node, "phone", log));
        res.setFax(getProperty(node, "fax", log));
        res.setEmail(getProperty(node, "email", log));
        res.setMobile(getProperty(node, "mobile", log));
        res.setSecondaryContactOfficer(getProperty(node, "secondaryContactOfficer", log));
        res.setSecondaryContactPosition(getProperty(node, "secondaryContactPosition", log));
        res.setSecondaryLocation(getProperty(node, "secondaryLocation", log));
        res.setSecondaryPhone(getProperty(node, "secondaryPhone", log));
        res.setSecondaryMobile(getProperty(node, "secondaryFax", log));
        res.setSecondaryEmail(getProperty(node, "secondaryEmail", log));
        res.setSecondaryMobile(getProperty(node, "secondaryMobile", log));
        res.setSecondaryFax(getProperty(node, "secondaryFax", log));
        res.setSponsorId(getProperty(node, "id", log));
        res.setDT_RowId(getProperty(node, "DT_RowId", log));

        return res;
    }


    /**
     * Constructs and returns a SupplierData object for the named supplier. If the supplier cannot be found in the repo,
     * return an empty object.
     *
     * @return A SupplierData object, which may be empty if the supplier item does not exist.
     */
    public static SponsorData createIfExists(String sponsorId, SlingRepository slingRepo, Logger log) {
        SponsorData res = new SponsorData();
        Session localSession = null;
        try {
            localSession = slingRepo.loginService("datadownload", null);

            final String anticipatedNodeName = SPONSOR_NODE_PATH + "/" + sponsorId;
            if (localSession.itemExists(anticipatedNodeName)) {
                Node sponsorNode = localSession.getNode(anticipatedNodeName);
                res = create(sponsorNode, log);
            }
        } catch (Exception e) {
            log.warn("Exception in SupplierFactory.CreateIfExists() :  " + e);
        } finally {
            if (localSession != null && localSession.isLive()) {
                localSession.logout();
            }
        }

        return res;
    }


    /**
     * Load all suppliers from the JCR repository and return them as a lost of supplier objects.
     *
     * @return A list of valid supplier objects.
     */
    public static List<SponsorData> createAllAsList(SlingRepository slingRepo, Logger log) {
        List<SponsorData> res = new LinkedList<>();
        Session session = null;

        try {
            session = slingRepo.loginService("datadownload", null);

            Workspace workspace = session.getWorkspace();
            QueryManager queryManager = workspace.getQueryManager();
            Query query = queryManager
                    .createQuery("SELECT sponsor.* FROM [nt:unstructured] AS sponsor WHERE ISDESCENDANTNODE(sponsor, ["
                            + SPONSOR_NODE_PATH + "]) ORDER BY sponsor.[id]", Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeItr = result.getNodes();

            while (nodeItr.hasNext()) {
                Node node = nodeItr.nextNode();
                SponsorData newSupplier = create(node, log);

                res.add(newSupplier);
            }
        } catch (Exception e) {
            log.warn("Exception in SupplierFactory.CreateAll() :  " + e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
        }

        return res;
    }


    /**
     * Load all suppliers and return them as a map, keyed by the supplierId
     * @return A hashmap of supplierdata objects
     */
    public static Map<String, SponsorData> createAllAsMap(SlingRepository slingRepo, Logger log) {
        Map<String, SponsorData> res = new HashMap<>();

        for(SponsorData sponsor: createAllAsList(slingRepo, log)) {
            res.put(sponsor.getSponsorId(), sponsor);
        }

        return res;
    }
}
