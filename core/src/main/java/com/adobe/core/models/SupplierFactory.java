package com.adobe.core.models;

import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;

import javax.jcr.*;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Factory methods for the creation of the supplier data source objects.
 *
 */
public class SupplierFactory extends BaseFactory {
    public static final String SUPPLIER_NODE_PATH = "/content/dodData/data/supplierData";

    /**
     * Returns a properly initialised SupplierData object from the JCR Node provided.
     * If the node is not a supplier data node, the object returned will have null fields.
     *
     * @param node The JCR node to load from.
     * @return A SupplierData object.
     */
    public static SupplierData create(Node node, Logger log) {
        SupplierData res = new SupplierData();

        res.setSupplierId(getProperty(node, "id", log));
        res.setSupplier(getProperty(node, "supplier", log));
        res.setHeading1(getProperty(node, "heading1", log));
        res.setContent(getProperty(node, "text", log));
        res.setDT_RowId(getProperty(node, "DT_RowId", log));

        return res;
    }


    /**
     * Constructs and returns a SupplierData object for the named supplier. If the supplier cannot be found in the repo,
     * return an empty object.
     *
     * @return A SupplierData object, which may be empty if the supplier item does not exist.
     */
    public static SupplierData createIfExists(String supplierId, SlingRepository slingRepo, Logger log) {
        SupplierData res = new SupplierData();
        Session localSession = null;

        try {
            localSession = slingRepo.loginService("datadownload", null);

            final String anticipatedNodeName = SUPPLIER_NODE_PATH + "/" + supplierId;
            if (localSession.itemExists(anticipatedNodeName)) {
                Node supplierNode = localSession.getNode(anticipatedNodeName);
                res = create(supplierNode, log);
            }
        } catch (Exception e) {
            log.warn("Exception in SupplierFactory.CreateIfExists() :  " + e);
        } finally {
            if (localSession != null && localSession.isLive()) {
                localSession.logout();
            }
        }

        return res;
    }


    /**
     * Load all suppliers from the JCR repository and return them as a lost of supplier objects.
     *
     * @return A list of valid supplier objects.
     */
    public static List<SupplierData> createAllAsList(SlingRepository slingRepo, Logger log) {
        List<SupplierData> res = new LinkedList<>();
        Session session = null;

        try {
            session = slingRepo.loginService("datadownload", null);

            Workspace workspace = session.getWorkspace();
            QueryManager queryManager = workspace.getQueryManager();
            Query query = queryManager
                    .createQuery("SELECT supplier.* FROM [nt:unstructured] AS supplier WHERE ISDESCENDANTNODE(supplier, ["
                            + SUPPLIER_NODE_PATH + "])", Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeItr = result.getNodes();

            while (nodeItr.hasNext()) {
                Node node = nodeItr.nextNode();
                SupplierData newSupplier = create(node, log);

                res.add(newSupplier);
            }
        } catch (Exception e) {
            log.warn("Exception in SupplierFactory.CreateAll() :  " + e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }
        }

        return res;
    }


    /**
     * Load all suppliers and return them as a map, keyed by the supplierId
     * @return A hashmap of supplierdata objects
     */
    public static Map<String, SupplierData> createAllAsMap(SlingRepository slingRepo, Logger log) {
        Map<String, SupplierData> res = new HashMap<>();

        for(SupplierData supplier: createAllAsList(slingRepo, log)) {
            res.put(supplier.getSupplierId(), supplier);
        }

        return res;
    }
}
