package com.adobe.core.models;

import org.json.JSONObject;

public class SupplierData {
	private String supplier;
	private String supplierId;
	private String DT_RowId;
	private String heading1;
	private String content;

	/** Package-private constructor to restrict creation to the factory classes **/
	SupplierData() {}

	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getHeading1() {
		return this.heading1;
	}

	public void setHeading1(String heading1) {
		this.heading1 = heading1;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDT_RowId() {
		return this.DT_RowId;
	}

	public void setDT_RowId(String dtIt) {
		this.DT_RowId = dtIt;
	}

	/** Returns this supplier's data as a JSON object **/
	public JSONObject JSON() {
		JSONObject res = new JSONObject();

		res.put("DT_RowId", getDT_RowId());
		res.put("id", getSupplierId());
		res.put("content", getContent());
		res.put("heading1", getHeading1());
		res.put("supplier", getSupplier());

		return res;
	}
}
