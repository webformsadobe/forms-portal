package com.adobe.core.models;

public class AssetMetaData {
	private String formNumber;
	private String formName;
	private String sponsor;
	private String keywords;
	private String supplier;
	private String format;
	private String reviewDate;
	private String arcDetails;
	private String classification;
	private String stockNumber;
	private String controlled;
	private String oldNumber;
	private String createDate;
	private String unitOfIssue;
	private String arcReplaceBy;
	private String arcDateVersion;
	private String group;
	private String versionDate;
	private String registryNumber;
	private String coverSheetContent;
	private String obsolete;
	private String coversheetLinkName;
	private String coversheetLinkAddr;
	private String developedBy;
	private String versionNumber;

	private boolean isCoverPage;
	private SponsorData sponsorData;
	private SupplierData supplierData;

	/**
	 * Empty package-private constructor to force users to use the factory methods.
	 */
	AssetMetaData() {}

	public String getFormNumber() {
		return this.formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public String getFormName() {
		return this.formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getSponsor() {
		return this.sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getKeywords() {
		return this.keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getReviewDate() {
		return this.reviewDate;
	}

	public void setReviewDate(String reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getArcDetails() {
		return this.arcDetails;
	}

	public void setArcDetails(String arcDetails) {
		this.arcDetails = arcDetails;
	}

	public String getClassification() {
		return this.classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getStockNumber() {
		return this.stockNumber;
	}

	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}

	public String getControlled() {
		return this.controlled;
	}

	public void setControlled(String controlled) {
		this.controlled = controlled;
	}

	public String getOldNumber() {
		return this.oldNumber;
	}

	public void setOldNumber(String oldNumber) {
		this.oldNumber = oldNumber;
	}

	public String getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUnitOfIssue() {
		return this.unitOfIssue;
	}

	public void setUnitOfIssue(String unitOfIssue) {
		this.unitOfIssue = unitOfIssue;
	}

	public String getArcReplaceBy() {
		return this.arcReplaceBy;
	}

	public void setArcReplaceBy(String arcReplaceBy) {
		this.arcReplaceBy = arcReplaceBy;
	}

	public String getArcDateVersion() {
		return this.arcDateVersion;
	}

	public void setArcDateVersion(String arcDateVersion) {
		this.arcDateVersion = arcDateVersion;
	}

	public String getGroup() {
		return this.group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getVersionDate() {
		return this.versionDate;
	}

	public void setVersionDate(String versionDate) {
		this.versionDate = versionDate;
	}

	public String getRegistryNumber() {
		return this.registryNumber;
	}

	public void setRegistryNumber(String registryNumber) {
		this.registryNumber = registryNumber;
	}

	public String getCoverSheetContent() {
		return this.coverSheetContent;
	}

	public void setCoverSheetContent(String coverSheetContent) {
		this.coverSheetContent = coverSheetContent;
	}

	public String getObsolete() {
		return this.obsolete;
	}

	public void setObsolete(String obsolete) {
		this.obsolete = obsolete;
	}

	public String getCoversheetLinkName() {
		return this.coversheetLinkName;
	}

	public void setCoversheetLinkName(String coversheetLinkName) {
		this.coversheetLinkName = coversheetLinkName;
	}

	public String getCoversheetLinkAddr() {
		return this.coversheetLinkAddr;
	}

	public void setCoversheetLinkAddr(String coversheetLinkAddr) {
		this.coversheetLinkAddr = coversheetLinkAddr;
	}

	public String getDevelopedBy() {
		return this.developedBy;
	}

	public void setDevelopedBy(String developedBy) {
		this.developedBy = developedBy;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public SponsorData getSponsorData() {
		return this.sponsorData;
	}

	public void setSponsorData(SponsorData sponsorData) {
		this.sponsorData = sponsorData;
	}

	public SupplierData getSupplierData() {
		return this.supplierData;
	}

	public void setSupplierData(SupplierData supplierData) {
		this.supplierData = supplierData;
	}

	public boolean getIsCoverPage() { return this.isCoverPage; }
	public void setIsCoverPage(boolean coverPage) { this.isCoverPage = coverPage; }
}

