package com.adobe.core.models;

import org.slf4j.Logger;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class BaseFactory {
    /**
     * Returns a node's property as a string, if the node has that property. If it doesn't, return the empty string.
     *
     * @param node The node from which to extract the property.
     * @param propertyName The name of the property to extract.
     * @return The value of the property, or the empty string "" if the property does not exist.
     */
    protected static String getProperty(Node node, String propertyName, Logger log) {
        String res = "";
        try {
            if (node.hasProperty(propertyName)) {
                res = node.getProperty(propertyName).getString();
            }
        } catch (RepositoryException e) {
            log.warn("Unable to locate node {}. Stack trace follows on debug level.", node);
            log.debug(e.toString());
        }
        return res;
    }
}
