package com.adobe.core.models;

import javax.servlet.http.HttpServletRequest;

public class Browser {
    public enum BrowserType { IE, FIREFOX, CHROME, UNKNOWN }

    private BrowserType myType;
    private int ieVersion;

    public Browser(HttpServletRequest request) {
        final String rawAgent = request.getHeader("user-agent");

        if (rawAgent == null) {
            myType = BrowserType.UNKNOWN;
            ieVersion = -1;
        } else {
            final int msie = rawAgent.indexOf("MSIE ");
            final int trident = rawAgent.indexOf("Trident/");
            final int edge = rawAgent.indexOf("Edge/");
            final int chrome = rawAgent.indexOf("Chrome/");
            final int firefox = rawAgent.indexOf("Firefox/");

            if (msie > 0) {
                myType = BrowserType.IE;
                ieVersion = Integer.parseInt(rawAgent.substring(msie + 5, rawAgent.indexOf('.', msie)));
            } else if (trident > 0) {
                myType = BrowserType.IE;
                final int rv = rawAgent.indexOf("rv:");
                ieVersion = Integer.parseInt(rawAgent.substring(rv + 3, rawAgent.indexOf('.', rv)));
            } else if (edge > 0) {
                myType = BrowserType.IE;
                ieVersion = Integer.parseInt(rawAgent.substring(edge + 5, rawAgent.indexOf('.', edge)));
            } else if (chrome > 0) {
                myType = BrowserType.CHROME;
                ieVersion = -1;            // -1 means NOT IE
            } else if (firefox > 0) {
                myType = BrowserType.FIREFOX;
                ieVersion = -1;            // -1 means NOT IE
            }
        }
    }

    public BrowserType browserType() {
        return myType;
    }

    public int ieVersion() {
        return ieVersion;
    }

    @Override
    public String toString() {
        String res;
        switch (myType) {
            case IE:
                res = "Internet Explorer";
                break;
            case CHROME:
                res = "Chrome";
                break;
            case FIREFOX:
                res = "Firefox";
                break;
            default:
                res = "";
                break;
        }

        if (myType == BrowserType.IE) {
            res += " " + ieVersion;
        }

        return res;
    }

    /** Returns an appropriate descriptor for the X-UA-Compatibility header based on the browser type **/
    public String compatibilityHeader() {
        String res = "Edge";

        if (myType == BrowserType.IE) {
            if (ieVersion() >= 7 && ieVersion() <= 11) {
                res = Integer.toString(ieVersion());
            }
        }

        return "IE=" + res;
    }
}
