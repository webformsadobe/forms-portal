package com.adobe.core.constants;

import com.day.crx.JcrConstants;

public class DepartmentOfDefenceConstants {
	public static final String METADATA_NODE = "metadata";

	public static final String FORM_NUM_METADATA = "formNumber";
	public static final String FORM_NAME_METADATA = "formName";
	public static final String OBSOLETE_METADATA = "obsolete";
	public static final String SPONSOR_METADATA = "sponsor";
	public static final String ARC_REPLACE_BY_METADATA = "arcReplaceBy";
	public static final String KEYWORDS_METADATA = "keywords";
	public static final String REVIEW_DATE_METADATA = "reviewDate";
	public static final String ARC_DETAILS_METADATA = "arcDetails";
	public static final String FORMAT_METADATA = "format";
	public static final String CLASSIFICATION_METADATA = "classification";
	public static final String STOCK_NUMBER_METADATA = "stockNumber";
	public static final String CONTROLLED_METADATA = "controlled";
	public static final String OLD_NUMBER_METADATA = "oldNumber";
	public static final String CREATED_DATE_METADATA = "createDate";
	public static final String UNIT_OF_ISSUE_METADATA = "unitOfIssue";
	public static final String SUPPLIER_METADATA = "supplier";
	public static final String ARC_DATE_VERSISON_METADATA = "arcDateVersionNumber";
	public static final String GROUP_METADATA = "group";
	public static final String VERSION_DATE_METADATA = "versionDate";
	public static final String REGISTRY_NUMBER_METADATA = "registryNumber";
	public static final String IS_COVERPAGE_METADATA = "coverpage";
	public static final String COVERSHEET_CONTENT_METADATA = "coverSheetContent";
	public static final String COVERSHEET_LINK_NAME_METADATA = "coversheetLinkName";
	public static final String COVERSHEET_LINK_ADDR_METADATA = "coversheetLinkAddr";
	public static final String DEVELOPED_BY = "developedBy";
	public static final String VERSION_NUMBER = "versionNumber";
	public static final String HIDDEN = "hidden";


	public static class PATHS {
		public static final String ASSET_PATH = "/content/dam/dodassets";
		public static final String OFFLINE_REPORT_XML_TEMPLATE = "/apps/dod/data/reportExporter/coverPageTemplate.html";

		public static final String ASSET_METADATA_NODE = JcrConstants.JCR_CONTENT + "/" + METADATA_NODE;

		private PATHS() {}
	}

	/**
	 * Service accounts
	 */
	public static class SERVICES {
		public static final String ADMIN_READER = "datadownload";
		public static final String ADMIN_WRITER = "datawrite";

		private SERVICES() {}
	}


	private DepartmentOfDefenceConstants() {}
}
